# Annotator Management System #

## The System was delivered on 12 September 2020 with all the Updates ##

## Backend ##
* Run ```cd api/```
* Run ```npm install```
* Run ```npm run build``` to build the dist files
* Run ```npm run dev``` to start the server
* Database: MongoDB should run ```27017``` port for the development

## Frontend ##
* Run ```cd web/```
* Run ```npm install```
* Run ```ng build --prod ``` to build the dist files
* Run ```ng s -o``` to start the project

## Deployment ##
* Run ```cd / project root where contain docker-compose.yml```
* Run ```docker-compose up --build -d```