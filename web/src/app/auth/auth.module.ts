import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AuthComponent } from "./login/auth.component";
import { RegisterComponent } from "./register/register.component";
import { NoAuthGuard } from "./no-auth-guard.service";
import { SharedModule } from "../shared";
import { AuthRoutingModule } from "./auth-routing.module";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatCardModule } from "@angular/material/card";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { MatSelectModule } from "@angular/material/select";
import {
  RecaptchaModule,
  RECAPTCHA_SETTINGS,
  RecaptchaSettings,
  RecaptchaFormsModule,
} from "ng-recaptcha";
import { RecaptchaLoaderService } from "ng-recaptcha";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { ResetComponent } from "./reset/reset.component";
import { VerifyAccountComponent } from "./verify-account/verify-account.component";

@NgModule({
  imports: [
    SharedModule,
    BrowserAnimationsModule,
    AuthRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    RecaptchaModule,
    RecaptchaFormsModule,
  ],
  declarations: [
    AuthComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResetComponent,
    VerifyAccountComponent,
  ],
  providers: [
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: {
        siteKey: "6LfqurEZAAAAAG3vVyOH0IpTCGvnUNCsFX2oRDHP",
        // siteKey: "6LcCMr8ZAAAAAD-gUZrxVcW_czf-NvHxgYp8--R7",
      } as RecaptchaSettings,
    },
    NoAuthGuard,
  ],
})
export class AuthModule {}
