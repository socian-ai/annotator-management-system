import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService, ApiService } from '../../core';
import { JwtService } from '../../core/services';


@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {
  clicked: boolean = false;
  tokenValid: boolean = false;
  tokenInvalid: boolean = false;
  success: boolean = false;
  authType: String = '';
  hide = true;
  resetForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private jwtService: JwtService
  ) {

  }

  ngOnInit() {
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        if (authenticated) {
          let user = this.jwtService.getDecodedAccessToken();

          if (user.type == "admin") {
            this.router.navigateByUrl('/admin/users');
          } else if (user.type == "annotator") {
            this.router.navigateByUrl('/annotator/' + user.group.permission);
          }
        }
      }
    );

    this.apiService.patch("reset/" + this.route.snapshot.params['id']).subscribe(res => {
      console.log(res);
      
      if(res.status == "success") {
        this.tokenValid = true;
      }
    },
    err => {
      this.tokenInvalid = true;
    });

    let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    this.resetForm = this.formBuilder.group({
      'password': [null, [Validators.required, this.checkPassword]],
      'passwordConfirm': [null, [Validators.required]]
    });

    this.resetForm.valueChanges.subscribe(field => {
      if (field.password !== field.passwordConfirm) {
        this.resetForm.get('passwordConfirm').setErrors({ notSame: true });
      } else {
        this.resetForm.get('passwordConfirm').setErrors(null);
      }
    });
  }

  checkPassword(control) {
    let enteredPassword = control.value
    let passwordCheck = /^(?=.*[a-zA-Z\-0-9])(?=.{6,})/;
    return (!passwordCheck.test(enteredPassword) && enteredPassword) ? { 'requirements': true } : null;
  }

  reset(credentials) {
    let fields = {
      "reset_token": this.route.snapshot.params['id'],
      "password": credentials.password
    }

    this.clicked = true;

    this.apiService.post("reset", fields).subscribe(res => {
      if(res.status == "success") {
        this.success = true;
      }
    },
    err => {
      this.resetForm.get('password').setErrors({ noAccount: true });
    });
  }
}
