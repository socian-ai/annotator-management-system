import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService, ApiService } from '../../core';
import { JwtService } from '../../core/services';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  success: boolean = false;
  authType: String = '';
  hide = true;
  forgotForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private jwtService: JwtService
  ) {

  }

  ngOnInit() {
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        if (authenticated) {
          let user = this.jwtService.getDecodedAccessToken();

          if (user.type == "admin") {
            this.router.navigateByUrl('/admin/users');
          } else if (user.type == "annotator") {
            this.router.navigateByUrl('/annotator/' + user.group.permission);
          }
        }
      }
    );

    let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    this.forgotForm = this.formBuilder.group({
      'email': [null, [Validators.required, Validators.pattern(emailregex)]]
    });
  }

  forgot(credentials) {

    this.apiService.post("forget-password", credentials).subscribe(res => {
      if(res.status == "success") {
        this.success = true;
      }
    },
    err => {
      this.success = false;
      this.forgotForm.get('email').setErrors({ noAccount: true });
    });
  }

  getErrorEmail() {
    return this.forgotForm.get('email').hasError('required') ? 'Email is required' :
      this.forgotForm.get('email').hasError('pattern') ? 'Not a valid email address' :
        this.forgotForm.get('email').hasError('noAccount') ? 'There is no account with the email you provided.' : '';
  }
}
