import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RecaptchaLoaderService } from 'ng-recaptcha';
import { UserService } from '../../core';
import {JwtService} from '../../core/services';

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  error: String;
  authType: String = '';
  hide = true;
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private jwtService: JwtService
  ) {

  }

  ngOnInit() {
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        if(authenticated) {
          let user = this.jwtService.getDecodedAccessToken();

          if(user.type == "admin") {
            this.router.navigateByUrl('/admin/users?userType=all-users');
          } else if(user.type == "annotator") {
            this.router.navigateByUrl('/annotator/' + user.group.permission);
          }
        }
      }
    );

    let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    this.loginForm = this.formBuilder.group({
      'email': [null, [Validators.required, Validators.pattern(emailregex)]],
      'password': [null, [Validators.required, this.checkPassword]],
      'recaptcha': [null, [Validators.required]]
    });
  }

  login(credentials) {
    this.userService
      .attemptAuth(this.authType, credentials)
      .subscribe(data => {
          window.location.reload();
        },
        err => {
          this.loginForm.patchValue({
            'recaptcha': null
          });
          
          this.error = err.message;
        }
      );
  }

  getErrorEmail() {
    return this.loginForm.get('email').hasError('required') ? 'Email is required' :
      this.loginForm.get('email').hasError('pattern') ? 'Not a valid email address' :
        this.loginForm.get('email').hasError('alreadyInUse') ? 'This email address is already in use' : '';
  }
 
  checkPassword(control) {
    let enteredPassword = control.value
    let passwordCheck = /^(?=.*[a-zA-Z\-0-9])(?=.{6,})/;
    return (!passwordCheck.test(enteredPassword) && enteredPassword) ? { 'requirements': true } : null;
  }
}
