import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService, ApiService } from '../../core';
import { JwtService } from '../../core/services';

@Component({
  selector: 'app-verify-account',
  templateUrl: './verify-account.component.html',
  styleUrls: ['./verify-account.component.css']
})
export class VerifyAccountComponent implements OnInit {
  message: String;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private apiService: ApiService,
    private jwtService: JwtService
  ) {

  }

  ngOnInit() {
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        if (authenticated) {
          let user = this.jwtService.getDecodedAccessToken();

          if (user.type == "admin") {
            this.router.navigateByUrl('/admin/users');
          } else if (user.type == "annotator") {
            this.router.navigateByUrl('/annotator/' + user.group.permission);
          }
        }
      }
    );

    this.apiService.patch("verify/" + this.route.snapshot.params['id']).subscribe(res => {
      if(res.status == "success") {
        this.message = res.message;
      }
    },
    err => {
      this.message = err.message;
    });
  }
}
