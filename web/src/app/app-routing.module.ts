import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserProfileComponent } from './profile/user-profile.component';
import { UserAuthResolverService } from './user-auth-resolver.service';

const routes: Routes = [
  {
    path: 'profile',
    component: UserProfileComponent,
    resolve: {
      isAuthenticated: UserAuthResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
