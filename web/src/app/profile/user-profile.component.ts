import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../core';

interface gender {
  value: string;
  viewValue: string;
}

interface type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  formGroup: FormGroup;
  fields: any;
  user: any;
  success: boolean = false;

  genders: gender[] = [
    {value: 'male', viewValue: 'Male'},
    {value: 'female', viewValue: 'Female'}
  ];

  types: type[] = [
    {value: 'annotator', viewValue: 'Annotator'},
    {value: 'linguistic', viewValue: 'Linguistic'}
  ];

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService
  ) { }


  ngOnInit(): void {
    let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    this.formGroup = this.formBuilder.group({
      'email': [null, [Validators.required, Validators.pattern(emailregex)]],
      'name': [null, Validators.required],
      'password': [null, [this.checkPassword]],
      'phone': [null, [Validators.required, this.getErrorPhone]],
      'age': [null, Validators.required],
      'gender': [null, Validators.required],
      'profession': [null, Validators.required],
      'type': [null, Validators.required]
    });

    this.apiService.get("me").subscribe( res => {
      if(res.status == 'success'){
        this.user = res.user;
        this.updateFormValue(this.user);
      }
    });
  }

  updateFormValue(user) {
    this.formGroup.patchValue({
      name: user.name,
      email: user.email,
      phone: user.phone,
      age: user.age,
      gender: user.gender,
      profession: user.profession,
      type: user.type
    });

    this.formGroup.get('email').disable();
    this.formGroup.get('type').disable();
  }

  checkPassword(control) {
    let enteredPassword = control.value;
    let passwordCheck = /^(?=.*[a-zA-Z\-0-9])(?=.{6,})/;
    return (!passwordCheck.test(enteredPassword) && enteredPassword) ? { 'requirements': true } : null;
  }

  getErrorEmail() {
    return this.formGroup.get('email').hasError('required') ? 'Field is required' :
      this.formGroup.get('email').hasError('pattern') ? 'Not a valid email address' :
        this.formGroup.get('email').hasError('alreadyInUse') ? 'This email address is already in use' : '';
  }

  getErrorPhone(control) {
    let enteredPhone = control.value;
    let phoneCheck = /^(?=.*[0-9])(?=.{10,})/;
    return (!phoneCheck.test(enteredPhone) && enteredPhone) ? { 'requirements': true } : null;
  }

  update(formData) {
    this.fields = formData;

    this.apiService.put('me', this.fields).subscribe( res => {
      if(res.status == 'success'){
        this.success = true;
      }
    }, err => {
      console.log(err);
    });
  }
}
