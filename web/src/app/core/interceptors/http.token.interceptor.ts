import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

import { JwtService } from '../services';

@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {
  constructor(private jwtService: JwtService, private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headersConfig = {};

    if (this.router.url != '/admin/data-upload') {
      headersConfig['Content-Type'] = 'application/json';
      headersConfig['Accept'] = 'application/json';
    }

    const token = this.jwtService.getToken();

    if (token) {
      headersConfig['Authorization'] = `Bearer ${token}`;
    } else {
      if(this.router.url == "/register" || this.router.url 
      == "/forget-password" || this.router.url.split("/")[1] 
      == "reset" || this.router.url.split("/")[1] == "verify") {
            
      } else {
        this.router.navigateByUrl('/');
      }
    }

    const request = req.clone({ setHeaders: headersConfig });
    return next.handle(request);
  }
}
