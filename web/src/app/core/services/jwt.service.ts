import { Injectable } from '@angular/core';
import * as jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  getToken(): String {
    return window.localStorage['token'];
  }

  saveToken(token: String) {
    window.localStorage['token'] = token;
  }

  destroyToken() {
    window.localStorage.removeItem('token');
  }

  getDecodedAccessToken () {
    try {
      return jwt_decode(localStorage.getItem('token'));
    }
    catch (Error) {
      return null;
    }
  }

  getPermission () {
    let user = this.getDecodedAccessToken();
    if (user.type == 'admin' && user.sub == 1) {
      return true;
    }

    return false;
  }

  isExpired() {
    let user = this.getDecodedAccessToken();

    if (Date.now() >= user.exp * 1000) {
      return false;
    } else {
      return true;
    }
  }
}
