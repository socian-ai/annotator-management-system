import {Injectable} from '@angular/core';
import {Subject} from "rxjs/internal/Subject";

@Injectable({
    providedIn: 'root'
  })
  export class HeaderService {
    public leftSidebar= new Subject<any>();

    expandCollapse(data) {
        this.leftSidebar.next(data);
    }
  }