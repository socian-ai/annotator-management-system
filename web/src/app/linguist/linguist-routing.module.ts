import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LinguistAuthResolverService } from './linguist-auth-resolver.service';
import { SentenceSentimentComponent } from './sentiment/sentence/sentence-sentiment.component';
import { ParagraphSentimentComponent } from './sentiment/paragraph/paragraph-sentiment.component';
import { DocumentSentimentComponent } from './sentiment/document/document-sentiment.component';
import { SentenceEmotionComponent } from './emotion/sentence/sentence-emotion.component';
import { ParagraphEmotionComponent } from './emotion/paragraph/paragraph-emotion.component';
import { SentenceReviewComponent } from './review/sentiment/sentence-review/sentence-review.component';
import { EditSentenceReviweComponent } from './review/sentiment/sentence-review/edit-sentence-review.component';
import { ParagraphReviewComponent } from './review/sentiment/paragraph-review/paragraph-review.component';
import { EditParagraphReviewComponent } from './review/sentiment/paragraph-review/edit-paragraph-review.component';
import { DocumentReviewComponent } from './review/sentiment/document-review/document-review.component';
import { EditDocumentReviewComponent } from './review/sentiment/document-review/edit-document-review.component';
import { SentenceEmotionReviewComponent } from './review/emotion/sentence-emotion-review/sentence-emotion-review.component';
import { EditSentenceEmotionReviewComponent } from './review/emotion/sentence-emotion-review/edit-sentence-emotion-review.component';
import { ParagraphEmotionReviewComponent } from './review/emotion/paragraph-emotion-review/paragraph-emotion-review.component';
import { EditParagraphEmotionReviewComponent } from './review/emotion/paragraph-emotion-review/edit-paragraph-emotion-review.component';
import { EditSentenceSentimentComponent } from './sentiment/sentence/edit-sentence-sentiment.component';
import { EditParagraphSentimentComponent } from './sentiment/paragraph/edit-paragraph-sentiment.component';
import { EditDocumentSentimentComponent } from './sentiment/document/edit-document-sentiment.component';
import { EditSentenceEmotionComponent } from './emotion/sentence/edit-sentence-emotion.component';
import { EditParagraphEmotionComponent } from './emotion/paragraph/edit-paragraph-emotion.component';

import { HistoryModifyComponent } from './history/history-modify/history-modify.component';
import { ViewAllComponent } from './history/view-all/view-all.component'; 

const routes: Routes = [
  {
    path: 'linguist/sentence-sentiment',
    component: SentenceSentimentComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/paragraph-sentiment',
    component: ParagraphSentimentComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/document-sentiment',
    component: DocumentSentimentComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/sentence-emotion',
    component: SentenceEmotionComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/paragraph-emotion',
    component: ParagraphEmotionComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/sentence-sentiment-review/:id',
    component: SentenceReviewComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/sentence-sentiment-review/edit/:id',
    component: EditSentenceReviweComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/paragraph-sentiment-review/:id',
    component: ParagraphReviewComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/paragraph-sentiment-review/edit/:id',
    component: EditParagraphReviewComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/document-sentiment-review/:id',
    component: DocumentReviewComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/document-sentiment-review/edit/:id',
    component: EditDocumentReviewComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/sentence-emotion-review/:id',
    component: SentenceEmotionReviewComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/sentence-emotion-review/edit/:id',
    component: EditSentenceEmotionReviewComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/paragraph-emotion-review/:id',
    component: ParagraphEmotionReviewComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/paragraph-emotion-review/edit/:id',
    component: EditParagraphEmotionReviewComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/history-sentence-sentiment/:id',
    component: EditSentenceSentimentComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/history-paragraph-sentiment/:id',
    component: EditParagraphSentimentComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/history-document-sentiment/:id',
    component: EditDocumentSentimentComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/history-sentence-emotion/:id',
    component: EditSentenceEmotionComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/history-paragraph-emotion/:id',
    component: EditParagraphEmotionComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/history-modify',
    component: HistoryModifyComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  },
  {
    path: 'linguist/group-history/:id',
    component: ViewAllComponent,
    resolve: {
      isAuthenticated: LinguistAuthResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LinguistRoutingModule {}
 