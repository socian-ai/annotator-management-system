import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared';
import { LinguistRoutingModule } from './linguist-routing.module';
import { LinguistAuthResolverService } from './linguist-auth-resolver.service';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatExpansionModule } from '@angular/material/expansion';

import { SentenceSentimentComponent } from './sentiment/sentence/sentence-sentiment.component';
import { EditSentenceSentimentComponent } from './sentiment/sentence/edit-sentence-sentiment.component';
import { ParagraphSentimentComponent } from './sentiment/paragraph/paragraph-sentiment.component';
import { EditParagraphSentimentComponent } from './sentiment/paragraph/edit-paragraph-sentiment.component';
import { DocumentSentimentComponent } from './sentiment/document/document-sentiment.component';
import { EditDocumentSentimentComponent } from './sentiment/document/edit-document-sentiment.component';
import { SentenceEmotionComponent } from './emotion/sentence/sentence-emotion.component';
import { EditSentenceEmotionComponent } from './emotion/sentence/edit-sentence-emotion.component';
import { ParagraphEmotionComponent } from './emotion/paragraph/paragraph-emotion.component';
import { EditParagraphEmotionComponent } from './emotion/paragraph/edit-paragraph-emotion.component';

import { SentenceReviewComponent } from './review/sentiment/sentence-review/sentence-review.component';
import { EditSentenceReviweComponent } from './review/sentiment/sentence-review/edit-sentence-review.component';
import { ParagraphReviewComponent } from './review/sentiment/paragraph-review/paragraph-review.component';
import { EditParagraphReviewComponent } from './review/sentiment/paragraph-review/edit-paragraph-review.component';
import { DocumentReviewComponent } from './review/sentiment/document-review/document-review.component';
import { EditDocumentReviewComponent } from './review/sentiment/document-review/edit-document-review.component';
import { SentenceEmotionReviewComponent } from './review/emotion/sentence-emotion-review/sentence-emotion-review.component';
import { EditSentenceEmotionReviewComponent } from './review/emotion/sentence-emotion-review/edit-sentence-emotion-review.component';
import { ParagraphEmotionReviewComponent } from './review/emotion/paragraph-emotion-review/paragraph-emotion-review.component';
import { EditParagraphEmotionReviewComponent } from './review/emotion/paragraph-emotion-review/edit-paragraph-emotion-review.component';
import { HistoryModifyComponent } from './history/history-modify/history-modify.component';
import { ViewAllComponent } from './history/view-all/view-all.component';

@NgModule({
  imports: [
    SharedModule,
    BrowserAnimationsModule,
    LinguistRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatTableModule,
    MatExpansionModule
  ],
  declarations: [
    SentenceSentimentComponent,
    ParagraphSentimentComponent,
    DocumentSentimentComponent,
    SentenceEmotionComponent,
    ParagraphEmotionComponent,
    SentenceReviewComponent,
    EditSentenceReviweComponent,
    ParagraphReviewComponent,
    EditParagraphReviewComponent,
    DocumentReviewComponent,
    EditDocumentReviewComponent,
    SentenceEmotionReviewComponent,
    EditSentenceEmotionReviewComponent,
    ParagraphEmotionReviewComponent,
    EditParagraphEmotionReviewComponent,
    EditSentenceSentimentComponent,
    EditParagraphSentimentComponent,
    EditDocumentSentimentComponent,
    EditSentenceEmotionComponent,
    EditParagraphEmotionComponent,
    HistoryModifyComponent,
    ViewAllComponent
  ],
  providers: [
    LinguistAuthResolverService
  ]
})
export class LinguistModule { }
