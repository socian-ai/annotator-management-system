import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService, JwtService } from '../../../core';

@Component({
  selector: 'submitted-sentence-sentiment',
  templateUrl: '../submitted-sentiment.component.html',
  styleUrls: ['../submitted-sentiment.component.css']
})
export class SubmittedSentenceSentimentComponent implements OnInit {
  title: string = "Sentence Sentiment Submitted History";
  displayedColumns: string[] = ['serial', 'text', 'answer', 'action'];
  dataSource: any;
  user: any;
  sentence: boolean = true;
  paragraph: boolean = false;
  document: boolean = false;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private apiService: ApiService, private jwtService: JwtService) { }

  ngOnInit(): void {
    this.user = this.jwtService.getDecodedAccessToken();

    this.apiService.get("annotator/sentence-sentiment").subscribe( res => {
      if(res.status == 'success'){
        this.dataSource = new MatTableDataSource(res.submitted_list);
      }
    });
  }

}
