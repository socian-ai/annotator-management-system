import { Component, OnInit } from '@angular/core';
import { ApiService, JwtService } from '../../../core';
import { Router, ActivatedRoute } from '@angular/router';

interface Type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'edit-document-sentiment',
  templateUrl: './edit-document-sentiment.component.html',
  styleUrls: ['../sentiment.component.css']
})
export class EditDocumentSentimentComponent implements OnInit {
  user: any;
  document: any;
  topics = [
    "বাংলাদেশ", "অর্থনীতি", "রাজনীতি", "নির্বচন", "বিজনেস", "শিল্প ও সংস্কৃতি", "আন্তর্জাতিক",
    "শিক্ষা", "খালাধুলা", "বিনোদন", "স্বাস্থ্য ও চিকিত্সা", "বিজ্ঞান ও প্রযুক্তি"
  ];
  topicInput: string;
  selectedTopics = [];
  selectedTopicsModel: any = {
    "overall": null,
    "skip": false
  };
  addedTopics = [];

  polarities: Type[] = [
    { value: 'SP', viewValue: 'Strongly Positive' },
    { value: 'WP', viewValue: 'Weekly Positive' },
    { value: 'SN', viewValue: 'Strongly Negative' },
    { value: 'WN', viewValue: 'Weekly Negative' },
    { value: 'NU', viewValue: 'Neutral' }
  ];

  constructor(private router: Router, private apiService: ApiService, private route: ActivatedRoute, private jwtService: JwtService) { }

  ngOnInit(): void {
    this.user = this.jwtService.getDecodedAccessToken();

    this.apiService.patch("linguist/document-sentiment/" + this.route.snapshot.params['id']).subscribe(res => {
      if (res.status == 'success') {
        this.document = res.document;

        this.document.linguistics.forEach(linguist => {
          if(linguist.user_id == this.user._id) {
            if(!linguist.answer.skip) {
              this.selectedTopicsModel.overall = linguist.answer.sentiment_overall;
              linguist.answer.topics.forEach( topic => {
                if(this.topics.indexOf(topic.name) > -1) {
                  this.selectedTopics.push(topic.name);
                } else {
                  this.addedTopics.push(topic.name);
                }
                
                this.selectedTopicsModel[topic.name] = topic.polarity;
              });
            }
          }
        });

      } else {
        alert(res.message);
      }
    });
  }

  selectTopic(index) {
    if (this.selectedTopics.includes(this.topics[index])) {
      const sel_index = this.selectedTopics.indexOf(this.topics[index]);
      if (sel_index > -1) {
        this.selectedTopics.splice(sel_index, 1);
        delete this.selectedTopicsModel[this.topics[index]];
      }
    } else {
      this.selectedTopics.push(this.topics[index]);
      this.selectedTopicsModel[this.topics[index]] = null;
    }
  }

  addTopic() {
    this.addedTopics.push(this.topicInput);
    this.selectedTopicsModel[this.topicInput] = null;
    this.topicInput = null;
  }

  removeTopic(index) {
    delete this.selectedTopicsModel[this.addedTopics[index]];
    this.addedTopics.splice(index, 1);
  }

  submit() {
    let fields = {
      "id": this.document._id,
      "update": true,
      "answer": {}
    };

    if (this.selectedTopicsModel['skip']) {
      fields.answer['skip'] = true;
    } else {
      fields.answer['topics'] = [];

      Object.keys(this.selectedTopicsModel).forEach(key => {
        if (key == 'skip') return;

        if (key == 'overall') {
          fields.answer['sentiment_overall'] = this.selectedTopicsModel[key]
        } else {
          if (this.selectedTopicsModel[key] != null) {
            fields.answer['topics'].push({
              "name": key,
              "polarity": this.selectedTopicsModel[key]
            });
          }
        }
      });
    }

    this.apiService.put("linguist/document-sentiment", fields).subscribe(res => {
      if (res.status == 'success') {
        this.router.navigate(['/linguist/history-modify'], {queryParams:{dataType: "document-sentiment"}});
      }
    });
  }
}
