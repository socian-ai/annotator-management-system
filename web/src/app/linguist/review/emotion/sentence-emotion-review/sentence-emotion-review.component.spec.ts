import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SentenceEmotionReviewComponent } from './sentence-emotion-review.component';

describe('SentenceEmotionReviewComponent', () => {
  let component: SentenceEmotionReviewComponent;
  let fixture: ComponentFixture<SentenceEmotionReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SentenceEmotionReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SentenceEmotionReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
