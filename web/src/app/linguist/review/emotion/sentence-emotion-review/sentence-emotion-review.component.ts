import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from '../../../../core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: "app-sentence-emotion-review",
  templateUrl: "./sentence-emotion-review.component.html",
  styleUrls: ["./sentence-emotion-review.component.css"],
})
export class SentenceEmotionReviewComponent implements OnInit {
  group: any;
  validation_complete: Number;
  number_of_data: Number;
  displayedColumns: string[] = [];
  dataSource: any;
  data: any;
  annotators: any;
  dataType: string = 'linguistic-data';
  filterType: string = 'all-data';

  filter_types = [
    { value: 'all-data', viewValue: 'All Data' },
    { value: 'validation-complete', viewValue: 'Validation Complete' },
    { value: 'validation-incomplete', viewValue: 'Validation Incomplete' }
  ];

  data_types = [
    { value: 'linguistic-data', viewValue: 'Linguistic Data' },
    { value: 'final-data', viewValue: 'Final Data' }
  ];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      if (params['dataType'] && params['filterType']) {
        this.filterType = params['filterType'];
        this.dataType = params['dataType'];
      }
    });

    this.getData();
  }

  getData() {
    this.apiService.get(
      "linguist/review/group?groupID=" + this.route.snapshot.params['id']
      + "&instanceType=sentence-emotion" + "&dataType=" + this.dataType + "&filterType=" + this.filterType).subscribe(res => {
        console.log(res);

        if (res.status == 'success') {
          this.group = res.group;
          this.annotators = res.annotators;
          this.validation_complete = res.validation_complete;
          this.number_of_data = res.number_of_data;
          this.data = res.data;

          this.displayedColumns = ['serial', 'data'];

          for (let user of this.annotators) {
            this.displayedColumns.push(user.name);
          }

          this.displayedColumns.push('linguistic');

          if (this.dataType == 'final-data') {
            this.displayedColumns.push('final');
          }

          this.displayedColumns.push('validation');

          this.dataSource = new MatTableDataSource(this.data);
        }
      });
  }

  changeDataType() {
    this.getData();
  }

  toggleValidation(id, index) {
    if (this.data[index].validation) {
      this.data[index].validation = false;
    } else {
      this.data[index].validation = true;
    }

    this.apiService.put("linguist/review/final-data-validation", {id: id}).subscribe(res => {
      // console.log(res);
    });
  }
}
