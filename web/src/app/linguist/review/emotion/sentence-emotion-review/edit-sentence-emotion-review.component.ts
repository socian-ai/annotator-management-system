import {Component, OnInit} from '@angular/core';
import { ApiService, JwtService } from '../../../../core';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'edit-sentence-emotion-review',
  templateUrl: './edit-sentence-emotion-review.component.html',
  styleUrls: ['../emotion.component.css']
})
export class EditSentenceEmotionReviewComponent implements OnInit {
  user: any;
  group_id: string;
  dataType: string;
  filterType: string;
  sentence_emotion: any;
  emotions = [];
  labels = ['Happy', 'Fear', 'Anger/Disgust', 'Sadness', 'Surprise', 'Neutral'];
  instanceType: string;

  constructor(private router: Router,private apiService: ApiService, private route: ActivatedRoute, private jwtService: JwtService) { }

  ngOnInit(): void {
    this.user = this.jwtService.getDecodedAccessToken();

    this.route.queryParams.subscribe(params => {
      if ((params['dataType'] == 'linguistic-data' || params['dataType'] == 'final-data') && params['groupID'] && params['filterType']) {
        this.group_id = params['groupID'];
        this.dataType = params['dataType'];
        this.filterType = params['filterType'];
        this.instanceType = params['instanceType'];
        this.getAInstance();
      }
    });
  }

  getAInstance() {
    this.apiService.get(
      "linguist/review/get-a-instance?instanceID=" + this.route.snapshot.params['id'] 
      + "&dataType=" + this.dataType).subscribe(res => {
      if (res.status == 'success') {
        this.sentence_emotion = res.data;
        let labels = ['Happy', 'Fear', 'Anger/Disgust', 'Sadness', 'Surprise', 'Neutral'];
        
        this.sentence_emotion.linguistics.forEach( linguistic => {
          if(linguistic.user_id == this.user._id) {
            if(linguistic.hasOwnProperty('answer') && !linguistic.answer.skip) {
              this.emotions = linguistic.answer.emotions;
            }
          }
        });

      } else {
        alert(res.message);
      }
    });
  }

  selectEmotion(emotion) {
    const sel_index = this.emotions.indexOf(emotion);
    if (sel_index > -1) {
      this.emotions.splice(sel_index, 1);
    } else {
      this.emotions.push(emotion)
    }
  }

  submit(skip){
    let fields = {
      "id": this.sentence_emotion._id,
      "dataType": this.dataType,
      "answer": {}
    }

    if (skip) {
      fields.answer['skip'] = true
    } else {
      fields.answer['emotions'] = this.emotions
    }

    this.apiService.put("linguist/review/update-a-instance", fields).subscribe(res => {
      if (res.status == 'success') {
        if (this.instanceType == 'sentence-emotion') {
          this.router.navigate(['/linguist/group-history/' + this.group_id], {
            queryParams: {
              dataType: this.dataType,
              filterType: this.filterType,
              instanceType: this.instanceType
            }
          });
        } else {
          this.router.navigate(['/linguist/sentence-emotion-review/' + this.group_id], { queryParams: { dataType:  this.dataType, filterType: this.filterType} });
        }
      }
    });
  }

}
