import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParagraphEmotionReviewComponent } from './paragraph-emotion-review.component';

describe('ParagraphEmotionReviewComponent', () => {
  let component: ParagraphEmotionReviewComponent;
  let fixture: ComponentFixture<ParagraphEmotionReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParagraphEmotionReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParagraphEmotionReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
