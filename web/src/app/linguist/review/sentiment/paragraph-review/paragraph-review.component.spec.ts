import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParagraphReviewComponent } from './paragraph-review.component';

describe('ParagraphReviewComponent', () => {
  let component: ParagraphReviewComponent;
  let fixture: ComponentFixture<ParagraphReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParagraphReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParagraphReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
