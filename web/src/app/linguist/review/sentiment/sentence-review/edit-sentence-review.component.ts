import { Component, OnInit } from '@angular/core';
import { ApiService, JwtService } from '../../../../core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

interface Type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'edit-sentence-sentiment',
  templateUrl: './edit-sentence-review.component.html',
  styleUrls: ['../sentiment.component.css']
})
export class EditSentenceReviweComponent implements OnInit {
  formGroup: FormGroup;
  sentence: any;
  user: any;
  group_id: string;
  dataType: string;
  filterType: string;
  instanceType: string;

  subjectivities: Type[] = [
    { value: 'subjective', viewValue: 'Subjective' },
    { value: 'objective', viewValue: 'Objective' }
  ];

  polarities: Type[] = [
    { value: 'SP', viewValue: 'Strongly Positive' },
    { value: 'WP', viewValue: 'Weekly Positive' },
    { value: 'SN', viewValue: 'Strongly Negative' },
    { value: 'WN', viewValue: 'Weekly Negative' },
    { value: 'NU', viewValue: 'Neutral' }
  ];

  constructor(
    private router: Router,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private jwtService: JwtService
  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      'id': [null, Validators.required],
      'subjectivity': [null, Validators.required],
      'polarity': [null, Validators.required],
      'skip': [null],
      'dataType': [null, Validators.required]
    });

    this.user = this.jwtService.getDecodedAccessToken();

    this.route.queryParams.subscribe(params => {
      if ((params['dataType'] == 'linguistic-data' || params['dataType'] == 'final-data') && params['groupID'] && params['filterType']) {
        this.group_id = params['groupID'];
        this.dataType = params['dataType'];
        this.filterType = params['filterType'];
        this.instanceType = params['instanceType'];
        this.getAInstance();
        this.formGroup.get('dataType').setValue(this.dataType);
      }
    });
  }

  getAInstance() {
    this.apiService.get(
      "linguist/review/get-a-instance?instanceID=" + this.route.snapshot.params['id']
      + "&dataType=" + this.dataType).subscribe(res => {
        if (res.status == 'success') {
          this.sentence = res.data;

          this.formGroup.get('id').setValue(this.sentence._id);


          this.sentence.linguistics.forEach(linguistic => {

            if (linguistic.user_id == this.user._id) {
              this.formGroup.get('subjectivity').setValue(linguistic.answer.subjectivity);
              this.formGroup.get('polarity').setValue(linguistic.answer.polarity);
            }
          });

        } else {
          alert(res.message);
        }
      });
  }

  changeSubjectivity(value) {
    if (value == 'objective') {
      this.formGroup.get('polarity').setValue("NU");
    } else {
      this.formGroup.get('polarity').setValue("");
    }
  }

  submit(formData) {
    this.apiService.put("linguist/review/update-a-instance", formData).subscribe(res => {
      if (res.status == 'success') {
        if (this.instanceType == 'sentence-sentiment') {
          this.router.navigate(['/linguist/group-history/' + this.group_id], {
            queryParams: {
              dataType: this.dataType,
              filterType: this.filterType,
              instanceType: this.instanceType
            }
          });
        } else {
          this.router.navigate(['/linguist/sentence-sentiment-review/' + this.group_id], { queryParams: { dataType: this.dataType, filterType: this.filterType } });
        }
      }
    });
  }
}
