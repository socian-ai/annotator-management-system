import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryModifyComponent } from './history-modify.component';

describe('HistoryModifyComponent', () => {
  let component: HistoryModifyComponent;
  let fixture: ComponentFixture<HistoryModifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryModifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
