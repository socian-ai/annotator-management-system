import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService, JwtService } from '../../../core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-history-modify',
  templateUrl: './history-modify.component.html',
  styleUrls: ['./history-modify.component.css']
})
export class HistoryModifyComponent implements OnInit {
  displayedColumns: string[] = ['serial', 'text', 'label', 'action'];
  dataSource: any;
  user: any;
  selectedValue: string;

  sentiments = [
    { value: 'sentence-sentiment', viewValue: 'sentence level sentiment' },
    { value: 'paragraph-sentiment', viewValue: 'paragraph level sentiment' },
    { value: 'document-sentiment', viewValue: 'document level sentiment' },
    { value: 'sentence-emotion', viewValue: 'sentence level emotion' },
    { value: 'paragraph-emotion', viewValue: 'paragraph level emotion' }
  ];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private router: Router,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private jwtService: JwtService
  ) { }

  ngOnInit(): void {
    this.user = this.jwtService.getDecodedAccessToken();

    this.selectedValue = this.route.snapshot.queryParamMap.get('dataType') == null ?
      'sentence-sentiment' : this.route.snapshot.queryParamMap.get('dataType');

    this.changeDataType(this.selectedValue);
  }

  changeDataType(event) {
    if (event == 'sentence-sentiment') {
      this.apiService.get("linguist/sentence-sentiment").subscribe(res => {
        if (res.status == 'success') {
          this.dataSource = new MatTableDataSource(res.submitted_list);
        }
      });
    } else if (event == 'paragraph-sentiment') {
      this.apiService.get("linguist/paragraph-sentiment").subscribe(res => {
        if (res.status == 'success') {
          this.dataSource = new MatTableDataSource(res.submitted_list);
        }
      });
    } else if (event == 'document-sentiment') {
      this.apiService.get("linguist/document-sentiment").subscribe(res => {
        if (res.status == 'success') {
          this.dataSource = new MatTableDataSource(res.submitted_list);
        }
      });
    } else if (event == 'sentence-emotion') {
      this.apiService.get("linguist/sentence-emotion").subscribe(res => {
        if (res.status == 'success') {
          this.dataSource = new MatTableDataSource(res.submitted_list);
        }
      });
    } else if (event == 'paragraph-emotion') {
      this.apiService.get("linguist/paragraph-emotion").subscribe(res => {
        if (res.status == 'success') {
          this.dataSource = new MatTableDataSource(res.submitted_list);
        }
      });
    }
  }
}
