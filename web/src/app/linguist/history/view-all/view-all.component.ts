import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from '../../../core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-all',
  templateUrl: './view-all.component.html',
  styleUrls: ['./view-all.component.css']
})
export class ViewAllComponent implements OnInit {
  group: any;
  validation_complete: Number;
  number_of_data: Number;
  displayedColumns: string[] = [];
  dataSource: any;
  data: any;
  annotators: any;
  instanceType: string = 'sentence-sentiment';
  dataType: string = 'final-data';
  filterType: string = 'all-data';

  filter_types = [
    { value: 'all-data', viewValue: 'All Data' },
    { value: 'validation-complete', viewValue: 'Validation Complete' },
    { value: 'validation-incomplete', viewValue: 'Validation Incomplete' }
  ];

  sentiments = [
    { value: 'sentence-sentiment', viewValue: 'sentence level sentiment' },
    { value: 'paragraph-sentiment', viewValue: 'paragraph level sentiment' },
    { value: 'document-sentiment', viewValue: 'document level sentiment' },
    { value: 'sentence-emotion', viewValue: 'sentence level emotion' },
    { value: 'paragraph-emotion', viewValue: 'paragraph level emotion' }
  ];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      if (params['dataType'] && params['filterType'] && params['instanceType']) {
        this.filterType = params['filterType'];
        this.dataType = params['dataType'];
        this.instanceType = params['instanceType'];
      }
    });

    this.getData();
  }

  getData() {
    this.apiService.get(
      "linguist/review/group?groupID=" + this.route.snapshot.params['id']
      + "&instanceType=" + this.instanceType + "&dataType=final-data" + "&filterType=" + this.filterType).subscribe(res => {
        console.log(res);

        if (res.status == 'success') {
          this.group = res.group;
          this.annotators = res.annotators;
          this.validation_complete = res.validation_complete;
          this.number_of_data = res.number_of_data;
          this.data = res.data;

          this.displayedColumns = ['serial', 'data'];

          for (let user of this.annotators) {
            this.displayedColumns.push(user.name);
          }

          this.displayedColumns.push('linguistic');

          if (this.dataType == 'final-data') {
            this.displayedColumns.push('final');
          }

          this.displayedColumns.push('validation');

          this.dataSource = new MatTableDataSource(this.data);
        }
      });
  }

  toggleValidation(id, index) {
    if (this.data[index].validation) {
      this.data[index].validation = false;
    } else {
      this.data[index].validation = true;
    }

    this.apiService.put("linguist/review/final-data-validation", {id: id}).subscribe(res => {
      // console.log(res);
    });
  }
}
