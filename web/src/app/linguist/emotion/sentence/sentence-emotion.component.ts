import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../core';

@Component({
  selector: 'sentence-emotion',
  templateUrl: './sentence-emotion.component.html',
  styleUrls: ['../emotion.component.css']
})
export class SentenceEmotionComponent implements OnInit {
  sentence_emotion: any;
  emotions = [];
  labels = ['Happy', 'Fear', 'Anger/Disgust', 'Sadness', 'Surprise', 'Neutral'];

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.patch("linguist/sentence-emotion").subscribe(res => {
      if (res.status == 'success') {
        this.sentence_emotion = res.sentence_emotion;
      }
    });
  }

  selectEmotion(emotion) {
    const sel_index = this.emotions.indexOf(emotion);
    if (sel_index > -1) {
      this.emotions.splice(sel_index, 1);
    } else {
      this.emotions.push(emotion)
    }
  }

  submit(skip) {
    let fields = {
      "id": this.sentence_emotion._id,
      "answer": {}
    }

    if (skip) {
      fields.answer['skip'] = true
    } else {
      fields.answer['emotions'] = this.emotions
    }
    console.log(fields);

    this.apiService.put("linguist/sentence-emotion", fields).subscribe(res => {
      if (res.status == 'success') {
        this.sentence_emotion = res.sentence_emotion;
        this.emotions = [];

        this.uncheckElements();
      }
    });
  }

  uncheckElements() {
    var uncheck = document.getElementsByTagName('input');

    for (var i = 0; i < uncheck.length; i++) {
      if (uncheck[i].type == 'checkbox') {
        uncheck[i].checked = false;
      }
    }
  }

}
