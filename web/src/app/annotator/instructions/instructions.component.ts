import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-instructions',
  templateUrl: './instructions.component.html',
  styleUrls: ['./instructions.component.css']
})
export class InstructionsComponent implements OnInit {
  title: string = "Instructions";

  panelOpenState = false;

  constructor() { }

  ngOnInit(): void {
  }

}
