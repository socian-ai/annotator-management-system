import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService, JwtService } from '../../../core';

@Component({
  selector: 'app-rejected-data',
  templateUrl: './rejected-data.component.html',
  styleUrls: ['./rejected-data.component.css']
})
export class RejectedDataComponent implements OnInit {
  displayedColumns: string[] = ['serial', 'text', 'label'];
  dataSource: any;
  user: any;
  selectedValue: string;

  sentiments = [
    { value: 'sentence-sentiment', viewValue: 'sentence level sentiment' },
    { value: 'paragraph-sentiment', viewValue: 'paragraph level sentiment' },
    { value: 'document-sentiment', viewValue: 'document level sentiment' },
    { value: 'sentence-emotion', viewValue: 'sentence level emotion' },
    { value: 'paragraph-emotion', viewValue: 'paragraph level emotion' }
  ];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private apiService: ApiService,
    private jwtService: JwtService
  ) { }

  ngOnInit(): void {
    this.user = this.jwtService.getDecodedAccessToken();
    this.changeDataType('sentence-sentiment');
  }

  changeDataType(dataType) {
    this.apiService.patch("annotator/rejected-data/" + dataType).subscribe(res => {
      if (res.status == 'success') {
        this.dataSource = new MatTableDataSource(res.rejected_data);
      }
    });
  }
}
