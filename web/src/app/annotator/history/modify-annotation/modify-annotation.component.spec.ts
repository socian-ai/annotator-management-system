import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyAnnotationComponent } from './modify-annotation.component';

describe('ModifyAnnotationComponent', () => {
  let component: ModifyAnnotationComponent;
  let fixture: ComponentFixture<ModifyAnnotationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifyAnnotationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyAnnotationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
