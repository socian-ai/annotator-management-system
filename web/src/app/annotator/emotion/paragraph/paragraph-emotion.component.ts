import {Component, OnInit} from '@angular/core';
import { ApiService } from '../../../core';

interface Type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'paragraph-emotion',
  templateUrl: './paragraph-emotion.component.html',
  styleUrls: ['../emotion.component.css']
})
export class ParagraphEmotionComponent implements OnInit {
  paragraph_emotion: any;
  topics = [
    "বাংলাদেশ", "অর্থনীতি", "রাজনীতি", "নির্বচন", "বিজনেস", "শিল্প ও সংস্কৃতি", "আন্তর্জাতিক",
    "শিক্ষা", "খালাধুলা", "বিনোদন", "স্বাস্থ্য ও চিকিত্সা", "বিজ্ঞান ও প্রযুক্তি"
  ];
  labels = ['Happy', 'Fear', 'Anger/Disgust', 'Sadness', 'Surprise', 'Neutral'];
  topicInput: string;
  selectedTopics = [];
  selectedTopicsModel: any = {
    "overall": [],
    "skip": false
  };
  addedTopics = [];

  polarities: Type[] = [
    { value: 'SP', viewValue: 'Strongly Positive' },
    { value: 'WP', viewValue: 'Weekly Positive' },
    { value: 'SN', viewValue: 'Strongly Negative' },
    { value: 'WN', viewValue: 'Weekly Negative' },
    { value: 'NU', viewValue: 'Neutral' }
  ];

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.patch("annotator/paragraph-emotion").subscribe(res => {
      if (res.status == 'success') {
        this.paragraph_emotion = res.paragraph_emotion;
      }
    });
  }

  selectTopic(index) {
    if (this.selectedTopics.includes(this.topics[index])) {
      const sel_index = this.selectedTopics.indexOf(this.topics[index]);
      if (sel_index > -1) {
        this.selectedTopics.splice(sel_index, 1);
        delete this.selectedTopicsModel[this.topics[index]];
      }
    } else {
      this.selectedTopics.push(this.topics[index]);
      this.selectedTopicsModel[this.topics[index]] = [];
    }
  }

  selectEmotion(topic, emotion) {
    const sel_index = this.selectedTopicsModel[topic].indexOf(emotion);
    if (sel_index > -1) {
      this.selectedTopicsModel[topic].splice(sel_index, 1);
    } else {
      this.selectedTopicsModel[topic].push(emotion);
    }
  }

  addTopic() {
    this.addedTopics.push(this.topicInput);
    this.selectedTopicsModel[this.topicInput] = [];
    this.topicInput = null;
  }

  removeTopic(index) {
    delete this.selectedTopicsModel[this.addedTopics[index]];
    this.addedTopics.splice(index, 1);
  }

  submit() {
    let fields = {
      "id": this.paragraph_emotion._id,
      "answer": {}
    };

    if (this.selectedTopicsModel['skip']) {
      fields.answer['skip'] = true;
    } else {
      fields.answer['topics'] = [];

      Object.keys(this.selectedTopicsModel).forEach(key => {
        if(key == 'skip') return;

        if (key == 'overall') {
          fields.answer['sentiment_overall'] = this.selectedTopicsModel[key]
        } else {
          if (this.selectedTopicsModel[key].length > 0) {
            fields.answer['topics'].push({
              "name": key,
              "emotions": this.selectedTopicsModel[key]
            });
          }
        }
      });
    }

    this.apiService.put("annotator/paragraph-emotion", fields).subscribe(res => {
      if (res.status == 'success') {
        this.paragraph_emotion = res.paragraph_emotion;
        this.selectedTopics = [];
        this.selectedTopicsModel = {
          "overall": [],
          "skip": false
        };
        this.addedTopics = [];
        this.uncheckElements();
      }
    });
  }

  uncheckElements() {
    var uncheck = document.getElementsByTagName('input');
    for (var i = 0; i < uncheck.length; i++) {
      if (uncheck[i].type == 'checkbox') {
        uncheck[i].checked = false;
      }
    }
  }

}
