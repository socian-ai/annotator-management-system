import {Component, OnInit} from '@angular/core';
import { ApiService, JwtService } from '../../../core';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'edit-sentence-emotion',
  templateUrl: './edit-sentence-emotion.component.html',
  styleUrls: ['../emotion.component.css']
})
export class EditSentenceEmotionComponent implements OnInit {
  user: any;
  sentence_emotion: any;
  emotions = [];
  labels = ['Happy', 'Fear', 'Anger/Disgust', 'Sadness', 'Surprise', 'Neutral'];

  constructor(private router: Router,private apiService: ApiService, private route: ActivatedRoute, private jwtService: JwtService) { }

  ngOnInit(): void {
    this.user = this.jwtService.getDecodedAccessToken();

    this.apiService.patch("annotator/sentence-emotion/" + this.route.snapshot.params['id']).subscribe( res => {
      if(res.status == 'success'){
        this.sentence_emotion = res.sentence_emotion;
        let labels = ['Happy', 'Fear', 'Anger/Disgust', 'Sadness', 'Surprise', 'Neutral'];
        this.sentence_emotion.annotators.forEach( annotator => {
          if(annotator.user_id == this.user._id) {
            if(!annotator.answer.skip) {
              this.emotions = annotator.answer.emotions;
            }
          }
        });
      } else {
        alert(res.message);
      }
    });
  }

  selectEmotion(emotion) {
    const sel_index = this.emotions.indexOf(emotion);
    if (sel_index > -1) {
      this.emotions.splice(sel_index, 1);
    } else {
      this.emotions.push(emotion)
    }
  }

  submit(skip){
    let fields = {
      "id": this.sentence_emotion._id,
      "update": true,
      "answer": {}
    }

    if (skip) {
      fields.answer['skip'] = true
    } else {
      fields.answer['emotions'] = this.emotions
    }

    this.apiService.put("annotator/sentence-emotion", fields).subscribe( res => {
      if(res.status == 'success'){
        this.router.navigate(['/annotator/history-modify'], {queryParams:{dataType: "sentence-emotion"}});
      }
    });
  }

}
