import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

interface Type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'sentence-sentiment',
  templateUrl: './sentence-sentiment.component.html',
  styleUrls: ['../sentiment.component.css']
})
export class SentenceSentimentComponent implements OnInit {
  formGroup: FormGroup;
  sentence: any;

  subjectivities: Type[] = [
    { value: 'subjective', viewValue: 'Subjective' },
    { value: 'objective', viewValue: 'Objective' }
  ];

  polarities: Type[] = [
    { value: 'SP', viewValue: 'Strongly Positive' },
    { value: 'WP', viewValue: 'Weekly Positive' },
    { value: 'SN', viewValue: 'Strongly Negative' },
    { value: 'WN', viewValue: 'Weekly Negative' },
    { value: 'NU', viewValue: 'Neutral' }
  ];

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      'id': [null, Validators.required],
      'subjectivity': [null, Validators.required],
      'polarity': [null, Validators.required],
      'skip': [null]
    });


    this.apiService.patch("annotator/sentence-sentiment").subscribe(res => {
      if (res.status == 'success') {
        this.sentence = res.sentence;
        this.formGroup.get('id').setValue(this.sentence._id);
      }
    });
  }

  changeSubjectivity(value) {
    if (value == 'objective') {
      this.formGroup.get('polarity').setValue("NU");
    } else {
      this.formGroup.get('polarity').setValue("");
    }
  }

  submit(formData) {
      this.apiService.put("annotator/sentence-sentiment", formData).subscribe(res => {
        if (res.status == 'success') {
          this.sentence = res.sentence;
          this.formGroup.reset();
          setTimeout( ()=>{
            Object.keys(this.formGroup.controls).forEach(key => {
              this.formGroup.get(key).setErrors(null) ;
            });
          }, 100);

          this.formGroup.get('id').setValue(this.sentence._id);
        }
      });
  }
}
