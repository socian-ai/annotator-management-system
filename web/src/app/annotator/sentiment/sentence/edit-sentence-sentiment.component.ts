import { Component, OnInit } from '@angular/core';
import { ApiService, JwtService } from '../../../core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

interface Type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'edit-sentence-sentiment',
  templateUrl: './edit-sentence-sentiment.component.html',
  styleUrls: ['../sentiment.component.css']
})
export class EditSentenceSentimentComponent implements OnInit {
  formGroup: FormGroup;
  sentence: any;
  user: any;

  subjectivities: Type[] = [
    { value: 'subjective', viewValue: 'Subjective' },
    { value: 'objective', viewValue: 'Objective' }
  ];

  polarities: Type[] = [
    { value: 'SP', viewValue: 'Strongly Positive' },
    { value: 'WP', viewValue: 'Weekly Positive' },
    { value: 'SN', viewValue: 'Strongly Negative' },
    { value: 'WN', viewValue: 'Weekly Negative' },
    { value: 'NU', viewValue: 'Neutral' }
  ];

  constructor(
    private router: Router,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private jwtService: JwtService
  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      'id': [null, Validators.required],
      'subjectivity': [null, Validators.required],
      'polarity': [null, Validators.required],
      'skip': [null],
      'update': [true]
    });

    this.user = this.jwtService.getDecodedAccessToken();

    this.apiService.patch("annotator/sentence-sentiment/" + this.route.snapshot.params['id']).subscribe(res => {
      if (res.status == 'success') {
        this.sentence = res.sentence;

        this.formGroup.get('id').setValue(this.sentence._id);


        this.sentence.annotators.forEach(annotator => {
          
          if (annotator.user_id == this.user._id) {
            this.formGroup.get('subjectivity').setValue(annotator.answer.subjectivity);
            this.formGroup.get('polarity').setValue(annotator.answer.polarity);
          }
        });

      } else {
        alert(res.message);
      }
    });
  }

  changeSubjectivity(value) {
    if (value == 'objective') {
      this.formGroup.get('polarity').setValue("NU");
    } else {
      this.formGroup.get('polarity').setValue("");
    }
  }

  submit(formData) {
    this.apiService.put("annotator/sentence-sentiment", formData).subscribe(res => {
      if (res.status == 'success') {
        this.router.navigate(['/annotator/history-modify'], {queryParams:{dataType: "sentence-sentiment"}});
      }
    });
  }
}
