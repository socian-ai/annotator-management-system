import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../core';

@Component({
  selector: 'annotator-group',
  templateUrl: './annotator-group.component.html',
  styleUrls: ['./annotator-group.component.css']
})
export class AnnotatorGroupComponent implements OnInit {
  group: any;
  constructor(
    private apiService: ApiService
  ) { }


  ngOnInit(): void {

    this.apiService.get("annotator/group/").subscribe( res => {
      if(res.status == 'success'){
        this.group = res.group;
      }
    });
  }
}
