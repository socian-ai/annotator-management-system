import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnnotatorAuthResolverService } from './annotator-auth-resolver.service';

import { SentenceSentimentComponent } from './sentiment/sentence/sentence-sentiment.component';
import { EditSentenceSentimentComponent } from './sentiment/sentence/edit-sentence-sentiment.component';
import { ParagraphSentimentComponent } from './sentiment/paragraph/paragraph-sentiment.component';
import { EditParagraphSentimentComponent } from './sentiment/paragraph/edit-paragraph-sentiment.component';
import { DocumentSentimentComponent } from './sentiment/document/document-sentiment.component';
import { EditDocumentSentimentComponent } from './sentiment/document/edit-document-sentiment.component';
import { SentenceEmotionComponent } from './emotion/sentence/sentence-emotion.component';
import { EditSentenceEmotionComponent } from './emotion/sentence/edit-sentence-emotion.component';
import { ParagraphEmotionComponent } from './emotion/paragraph/paragraph-emotion.component';
import { EditParagraphEmotionComponent } from './emotion/paragraph/edit-paragraph-emotion.component';
import { AnnotatorGroupComponent } from './group/annotator-group.component';
import { InstructionsComponent } from './instructions/instructions.component';
import { ModifyAnnotationComponent } from './history/modify-annotation/modify-annotation.component';
import { RejectedDataComponent } from './history/rejected-data/rejected-data.component';
import { ViewAllComponent } from './history/view-all/view-all.component';

const routes: Routes = [
  {
    path: 'annotator/sentence-sentiment',
    component: SentenceSentimentComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  },
  {
    path: 'annotator/history-sentence-sentiment/:id',
    component: EditSentenceSentimentComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  },
  {
    path: 'annotator/paragraph-sentiment',
    component: ParagraphSentimentComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  },
  {
    path: 'annotator/history-paragraph-sentiment/:id',
    component: EditParagraphSentimentComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  },
  {
    path: 'annotator/document-sentiment',
    component: DocumentSentimentComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  },
  {
    path: 'annotator/history-document-sentiment/:id',
    component: EditDocumentSentimentComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  },
  {
    path: 'annotator/sentence-emotion',
    component: SentenceEmotionComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  },
  {
    path: 'annotator/history-sentence-emotion/:id',
    component: EditSentenceEmotionComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  },
  {
    path: 'annotator/paragraph-emotion',
    component: ParagraphEmotionComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  },
  {
    path: 'annotator/history-paragraph-emotion/:id',
    component: EditParagraphEmotionComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  },
  {
    path: 'annotator/history-modify',
    component: ModifyAnnotationComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  },
  {
    path: 'annotator/history-rejected',
    component: RejectedDataComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  },
  {
    path: 'annotator/history-view-all',
    component: ViewAllComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  },
  {
    path: 'annotator/group',
    component: AnnotatorGroupComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  },
  {
    path: 'annotator/instructions',
    component: InstructionsComponent,
    resolve: {
      isAuthenticated: AnnotatorAuthResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnnotatorRoutingModule { }
