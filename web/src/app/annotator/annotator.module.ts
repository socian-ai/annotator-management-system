import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AnnotatorAuthResolverService } from './annotator-auth-resolver.service';
import { SharedModule } from '../shared';
import { AnnotatorRoutingModule } from './annotator-routing.module';
import { SentenceSentimentComponent } from './sentiment/sentence/sentence-sentiment.component';
import { EditSentenceSentimentComponent } from './sentiment/sentence/edit-sentence-sentiment.component';
import { ParagraphSentimentComponent } from './sentiment/paragraph/paragraph-sentiment.component';
import { EditParagraphSentimentComponent } from './sentiment/paragraph/edit-paragraph-sentiment.component';
import { DocumentSentimentComponent } from './sentiment/document/document-sentiment.component';
import { EditDocumentSentimentComponent } from './sentiment/document/edit-document-sentiment.component';
import { SentenceEmotionComponent } from './emotion/sentence/sentence-emotion.component';
import { EditSentenceEmotionComponent } from './emotion/sentence/edit-sentence-emotion.component';
import { ParagraphEmotionComponent } from './emotion/paragraph/paragraph-emotion.component';
import { EditParagraphEmotionComponent } from './emotion/paragraph/edit-paragraph-emotion.component';
import { AnnotatorGroupComponent } from './group/annotator-group.component';
import { InstructionsComponent } from './instructions/instructions.component';

import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import {MatExpansionModule} from '@angular/material/expansion';
import { ModifyAnnotationComponent } from './history/modify-annotation/modify-annotation.component';
import { RejectedDataComponent } from './history/rejected-data/rejected-data.component';
import { ViewAllComponent } from './history/view-all/view-all.component';


@NgModule({
  imports: [
    SharedModule,
    BrowserAnimationsModule,
    AnnotatorRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatTableModule,
    MatExpansionModule
  ],
  declarations: [
    SentenceSentimentComponent,
    EditSentenceSentimentComponent,
    ParagraphSentimentComponent,
    EditParagraphSentimentComponent,
    DocumentSentimentComponent,
    EditDocumentSentimentComponent,
    SentenceEmotionComponent,
    EditSentenceEmotionComponent,
    ParagraphEmotionComponent,
    EditParagraphEmotionComponent,
    AnnotatorGroupComponent,
    InstructionsComponent,
    ModifyAnnotationComponent,
    RejectedDataComponent,
    ViewAllComponent
  ],
  providers: [
    AnnotatorAuthResolverService
  ]
})
export class AnnotatorModule {}
