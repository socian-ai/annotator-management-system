import { Component, OnInit } from '@angular/core';
import { ApiService, ExcelService } from '../../../core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.css']
})
export class DownloadComponent implements OnInit {
  title: string = "Data Download";
  success: string;
  fileToUpload: File = null;
  formGroup: FormGroup;

  analysisType = [
    { value: 'sentiment', viewValue: 'Sentiment' },
    { value: 'emotion', viewValue: 'Emotion' }
  ];

  instanceTypes = [

  ];
  constructor(
    private apiService: ApiService,
    private formBuilder: FormBuilder,
    private excelService: ExcelService
    ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      'analysisType': [null, Validators.required],
      'instanceType': [null, Validators.required],
      'downloadFormat': [null, Validators.required]
    });

    this.formGroup.patchValue({
      downloadFormat: '.csv'
    });
  }

  changeType(value) {
    this.formGroup.get('instanceType').setValue("");
    this.success = undefined;

    if (value == 'sentiment') {
      this.instanceTypes = [
        { value: 'sentence-sentiment', viewValue: 'Sentence' },
        { value: 'paragraph-sentiment', viewValue: 'Paragraph' },
        { value: 'document-sentiment', viewValue: 'Document' }
      ]
    } else if (value == 'emotion') {
      this.instanceTypes = [
        { value: 'sentence-emotion', viewValue: 'Sentence' },
        { value: 'paragraph-emotion', viewValue: 'Paragraph' },
      ]
    }
  }

  onDownload(formData) {
    console.log(formData.instanceType);
    this.apiService.patch('admin/data-download/' + formData.instanceType).subscribe( res => {
      if(res.status == 'success'){
        if(formData.downloadFormat == '.xlsx') {
          this.excelService.exportAsExcelFile(res.data, 'download-' + formData.instanceType);
        } else {
          this.excelService.exportAsCsvFile(res.data, 'download-' + formData.instanceType);
        }
      }
    });
  }

}
