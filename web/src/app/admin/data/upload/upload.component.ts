import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { ExcelService } from '../../../core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../../../shared/dialog/dialog.component';
import { Papa } from 'ngx-papaparse';
import * as XLSX from 'xlsx';

interface Upload {
  status: string;
  message: string;
  data: any;
}

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  title: string = "Data Upload";
  success: string;
  formGroup: FormGroup;

  analysisType = [
    { value: 'sentiment', viewValue: 'Sentiment' },
    { value: 'emotion', viewValue: 'Emotion' }
  ];

  instanceTypes = [

  ];

  constructor(
    private http: HttpClient,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private excelService: ExcelService,
    private papa: Papa
  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      'analysisType': [null, Validators.required],
      'instanceType': [null, Validators.required],
      'downloadFormat': [null, Validators.required],
      'data': [null, Validators.required]
    });

    this.formGroup.patchValue({
      downloadFormat: '.csv'
    });
  }

  changeType(value) {
    this.formGroup.get('instanceType').setValue("");
    this.success = undefined;

    if (value == 'sentiment') {
      this.instanceTypes = [
        { value: 'sentence-sentiment', viewValue: 'Sentence' },
        { value: 'paragraph-sentiment', viewValue: 'Paragraph' },
        { value: 'document-sentiment', viewValue: 'Document' }
      ]
    } else if (value == 'emotion') {
      this.instanceTypes = [
        { value: 'sentence-emotion', viewValue: 'Sentence' },
        { value: 'paragraph-emotion', viewValue: 'Paragraph' },
      ]
    }
  }

  onFileChange(event) {
    const reader = new FileReader();

    let validExts = new Array(".csv", ".xlsx");
    let fileExt = event.target.value;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));

    if (validExts.indexOf(fileExt) < 0) {
      alert("Invalid file selected, valid files are of " + validExts.toString() + " types.");
      event.target.value = ''
      this.formGroup.patchValue({
        data: null
      });
      return false;
    }

    if (event.target.files && event.target.files.length) {
      let fileReaded = event.target.files[0];

      if (fileExt == '.csv') {
        this.papa.parse(fileReaded, {
          complete: (result) => {
            let data = [];

            result.data.forEach(resData => {
              data.push({
                "Unannotated Id": resData[0],
                "Text": resData[1],
                "Type": resData[2],
                "Domain": resData[3],
                "Root Url": resData[4],
                "Category": resData[5],
                "Published Date": resData[6],
                "Parse Time": resData[7]
              })
            })

            data.shift()

            this.formGroup.patchValue({
              data: data
            });
          }
        });
      } else if (fileExt == '.xlsx') {
        reader.onload = (event) => {
          const data = reader.result;
          let workBook = XLSX.read(data, { type: 'binary' });

          let jsonData = workBook.SheetNames.reduce((initial, name) => {
            const sheet = workBook.Sheets[name];

            return XLSX.utils.sheet_to_json(sheet);
          }, {});

          this.formGroup.patchValue({
            data: jsonData
          });
        }

        reader.readAsBinaryString(fileReaded);
      }
    }
  }

  onUpload(formData) {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      data: {
        title: 'Confirmation',
        message: "Are you sure you want to upload " + formData.instanceType + " data?",
        result: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.http.post<Upload>(environment.api_url + 'admin/data-upload', formData).subscribe(res => {
          if (res.status == 'success') {
            (<HTMLInputElement>document.getElementById('uploadFile')).value = "";
            this.formGroup.patchValue({
              data: []
            });
            
            if (formData.downloadFormat == '.xlsx') {
              this.excelService.exportAsExcelFile(res.data, 'similar');
            } else {
              this.excelService.exportAsCsvFile(res.data, 'similar');
            }

            this.success = res.message;
          }
        });
      }
    });
  }

}
