import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from '../../../core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../../../shared/dialog/dialog.component';

@Component({
  selector: 'app-upload-history',
  templateUrl: './upload-history.component.html',
  styleUrls: ['./upload-history.component.css']
})
export class UploadHistoryComponent implements OnInit {
  displayedColumns: string[] = ['class_type', 'data_type', 'date', 'uploaded_data', 'similar_data', 'stored_data'];
  dataSource: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private apiService: ApiService, 
    public dialog: MatDialog,
    breakpointObserver: BreakpointObserver
    ) {

    breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = result.matches ? 
          ['class_type', 'data_type', 'date', 'uploaded_data', 'similar_data', 'stored_data'] : 
          ['class_type', 'data_type', 'date', 'uploaded_data', 'similar_data', 'stored_data'];
    });
   }

  ngOnInit(): void {

    this.apiService.get('admin/upload-history').subscribe( res => {
      if(res.status == 'success'){
        this.dataSource = new MatTableDataSource(res.upload_histories);
      }
    });
  }
}
