import { Component, OnInit } from '@angular/core';
import * as echarts from 'echarts';
import { ApiService } from '../../../core';

@Component({
  selector: 'app-class-distribution',
  templateUrl: './class-distribution.component.html',
  styleUrls: ['./class-distribution.component.css']
})
export class ClassDistributionComponent implements OnInit {
  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.get('admin/class-distributions').subscribe(res => {
      if (res.status == 'success') {
        
        this.barChart(
          ['Subjective', 'Objective'],
          [res.subjectivity.subjective, res.subjectivity.objective],
          'sentence_subjectivity',
          'Sentence (Subjectivity)'
        );

        this.barChart([
          'Strongly Positive',
          'Positive',
          'Neutral',
          'Negative',
          'Strongly Negative'
        ],[
          res.sentence_polarity.strongly_positive, res.sentence_polarity.weekly_positive, 
          res.sentence_polarity.neutral, res.sentence_polarity.weekly_negative, res.sentence_polarity.strongly_negative
        ], 'sentence_polarity', 'Sentence (Polarity)');

        this.barChart([
          'Strongly Positive',
          'Positive',
          'Neutral',
          'Negative',
          'Strongly Negative'
        ],[
          res.paragraph_polarity.strongly_positive, res.paragraph_polarity.weekly_positive, 
          res.paragraph_polarity.neutral, res.paragraph_polarity.weekly_negative, res.paragraph_polarity.strongly_negative
        ], 'paragraph_polarity', 'Paragraph (Polarity)');

        this.barChart([
          'Strongly Positive',
          'Positive',
          'Neutral',
          'Negative',
          'Strongly Negative'
        ],[
          res.document_polarity.strongly_positive, res.document_polarity.weekly_positive, 
          res.document_polarity.neutral, res.document_polarity.weekly_negative, res.document_polarity.strongly_negative
        ], 'document_polarity', 'Document (Polarity)');

        this.barChart([
          'Happy',
          'Sadness',
          'Neutral',
          'Angry',
          'Fear',
          'Surprise'
        ],[
          res.sentence_emotion.happy, res.sentence_emotion.sadness, 
          res.sentence_emotion.neutral, res.sentence_emotion.anger, 
          res.sentence_emotion.fear, res.sentence_emotion.surprise
        ], 'sentence_emotion', 'Sentence');

        this.barChart([
          'Happy',
          'Sadness',
          'Neutral',
          'Angry',
          'Fear',
          'Surprise'
        ],[
          res.paragraph_emotion.happy, res.paragraph_emotion.sadness, 
          res.paragraph_emotion.neutral, res.paragraph_emotion.anger, 
          res.paragraph_emotion.fear, res.paragraph_emotion.surprise
        ], 'paragraph_emotion', 'Paragraph');
      }
    }, err => {
      console.log(err);
    });
  }

  barChart(xAxisData, yAxisData, container, title=null) {

    let option = {
      title: {
        text: title
      },
      color: ['#3398DB'],
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: [
        {
          type: 'category',
          data: xAxisData,
          axisTick: {
            alignWithLabel: true
          }
        }
      ],
      yAxis: [
        {
          type: 'value'
        }
      ],
      series: [
        {
          name: 'Count',
          type: 'bar',
          barWidth: '60%',
          data: yAxisData
        }
      ]
    };

    echarts.init(document.getElementById(container)).setOption(option);
  }

}
