import { Component, OnInit } from '@angular/core';
import * as echarts from 'echarts';
import { ApiService } from '../../../core';

@Component({
  selector: 'app-overall-statistics',
  templateUrl: './overall-statistics.component.html',
  styleUrls: ['./overall-statistics.component.css']
})
export class OverallStatisticsComponent implements OnInit {
  basic_statics: any;
  available_data: any;
  assigned_data: any;
  annotated_data: any;
  unannotated_data: any;

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.get('admin/overall-statistics/').subscribe( res => {
      
      if(res.status == 'success') {
        this.basic_statics = res.basic_statics;
        this.available_data = res.available_data;
        this.assigned_data = res.assigned_data;
        this.annotated_data = res.annotated_data;
        this.unannotated_data = res.unannotated_data;
      }
    });
  }

}
