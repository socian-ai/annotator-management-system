import { Component, OnInit } from '@angular/core';
import * as echarts from 'echarts';
import { ApiService } from '../../../core';

@Component({
  selector: 'app-domain-distribution',
  templateUrl: './domain-distribution.component.html',
  styleUrls: ['./domain-distribution.component.css']
})
export class DomainDistributionComponent implements OnInit {
  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.apiService.get('admin/domain-distributions').subscribe(res => {
      if (res.status == 'success') {
        
        this.barChart(res.sentence_subjectivity, 'sentence_subjectivity', 'Sentence (Subjectivity)');
        this.barChart(res.polarity.sentence, 'sentence_polarity', 'Sentence (Polarity)');
        this.barChart(res.polarity.paragraph, 'paragraph_polarity', 'Paragraph (Polarity)');
        this.barChart(res.polarity.document, 'document_polarity', 'Document (Polarity)');

        // Emotion
        this.barChart(res.emotion.sentence, 'sentence_emotion', 'Sentence');
        this.barChart(res.emotion.paragraph, 'paragraph_emotion', 'Paragraph');
      }
    }, err => {
      console.log(err);
    });
  }

  barChart(data, container, title = null) {
    
    if(data == undefined || data.length == 0) return;

    let x_data = [];
    let y_data = [];
    data.forEach( dat => {
      x_data.push(dat._id);
      y_data.push(dat.count);
    });
    
    let option = {
      title: {
        text: title
      },
      color: ['#3398DB'],
      tooltip: {
        trigger: 'axis',
        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
          type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: [
        {
          type: 'category',
          data: x_data,
          axisTick: {
            alignWithLabel: true
          }
        }
      ],
      yAxis: [
        {
          type: 'value'
        }
      ],
      series: [
        {
          name: 'Count',
          type: 'bar',
          barWidth: '60%',
          data: y_data
        }
      ]
    };

    echarts.init(document.getElementById(container)).setOption(option);
  }

}
