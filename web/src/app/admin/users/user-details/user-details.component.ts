import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from '../../../core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})

export class UserDetailsComponent implements OnInit {
  displayedColumns: string[] = ['serial', 'text', 'label'];
  dataSource: any;
  details: any;
  selectedValue = 'sentence-sentiment';
  userID: string;

  sentiments = [
    { value: 'sentence-sentiment', viewValue: 'sentence level sentiment' },
    { value: 'paragraph-sentiment', viewValue: 'paragraph level sentiment' },
    { value: 'document-sentiment', viewValue: 'document level sentiment' },
    { value: 'sentence-emotion', viewValue: 'sentence level emotion' },
    { value: 'paragraph-emotion', viewValue: 'paragraph level emotion' }
  ];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  
  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.userID = this.route.snapshot.params['id'];
    this.changeDataType('sentence-sentiment');
  }

  changeDataType(event) {
    this.apiService.get("admin/users/" + this.userID + "?dataType=" + event).subscribe(res => {
      console.log(res);
      
      if (res.status == 'success') {
        this.details = res.details;
      }
    });

    this.apiService.patch("admin/annotator/" + this.userID + '/' + event).subscribe(res => {
      if (res.status == 'success') {
        this.dataSource = new MatTableDataSource(res.submitted_list);
      }
    });
  }
}
