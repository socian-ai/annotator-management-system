import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../../../shared/dialog/dialog.component';

export interface User {
  status: string;
  users: number;
}

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class UserShowComponent implements OnInit {

  displayedColumns: string[] = ['serial', 'name', 'email', 'phone', 'type', 'status', 'action'];
  dataSource: any;
  sentiments = [
    { value: 'all-users', viewValue: 'All Users' },
    { value: 'all-annotators', viewValue: 'All Annotators' },
    { value: 'all-linguistics', viewValue: 'All Linguistics' },
    { value: 'assigned-annotators', viewValue: 'Assigned Annotators' },
    { value: 'free-annotators', viewValue: 'Free Annotators' },
    { value: 'assigned-linguistics', viewValue: 'Assigned Linguistics' },
    { value: 'free-linguistics', viewValue: 'Free Linguistics' }
  ];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private apiService: ApiService,
    public dialog: MatDialog,
    public activatedRoute: ActivatedRoute,
    breakpointObserver: BreakpointObserver
  ) {

    breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = result.matches ?
        ['serial', 'name', 'email', 'phone', 'type', 'status', 'action'] :
        ['serial', 'name', 'email', 'phone', 'type', 'status', 'action'];
    });
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(params => {
      this.getUsers(params['userType']);
    });
  }

  getUsers(params) {
    this.apiService.patch('admin/users-list/' + params).subscribe(res => {
      if (res.status == 'success') {
        this.dataSource = new MatTableDataSource(res.users);
      }
    });
  }

  active(id) {
    let fields = {
      "status": 'active'
    };

    this.apiService.put('admin/users/' + id, fields).subscribe(res => {
      if (res.status == 'success') {
        this.ngOnInit();
      }
    }, err => {
      console.log(err);
    });
  }

  delete(id: string) {
    this.apiService.delete('admin/users/' + id).subscribe(res => {
      if (res.status == 'success') {
        this.ngOnInit();
      }
    }, err => {
      console.log(err);
    });
  }

  openDialog(id: string): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      data: {
        title: 'Confirmation',
        message: "Are you sure you want to delete the user?",
        result: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.delete(id);
      }
    });
  }
}
