import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ApiService } from '../../../core';
import {Router, ActivatedRoute} from '@angular/router';

interface gender {
  value: string;
  viewValue: string;
}

interface type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'user-dit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class UserEditComponent implements OnInit {
  formGroup: FormGroup;
  fields: any;
  user: any;

  genders: gender[] = [
    {value: 'male', viewValue: 'Male'},
    {value: 'female', viewValue: 'Female'}
  ];

  types: type[] = [
    {value: 'admin', viewValue: 'Admin'},
    {value: 'annotator', viewValue: 'Annotator'},
    {value: 'linguistic', viewValue: 'Linguistic'}
  ];

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute
  ) {

  }

  ngOnInit(): void {
    let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    let ageCheck = /^[1-9][1-9]|[2-9]\d+$/;
    
    this.formGroup = this.formBuilder.group({
      'email': [{value: null, disabled: true}],
      'name': [null, Validators.required],
      'phone': [null, [Validators.required, this.getErrorPhone]],
      'age': [null, [Validators.required, Validators.pattern(ageCheck)]],
      'gender': [null, Validators.required],
      'profession': [null, Validators.required],
      'type': [null, Validators.required]
    });

    this.apiService.patch("admin/users/" + this.route.snapshot.params['id']).subscribe( res => {
      if(res.status == 'success'){
        this.user = res.user;
        this.updateFormValue(this.user);
      }
    });
  }

  updateFormValue(user) {
    this.formGroup.patchValue({
      name: user.name,
      email: user.email,
      phone: user.phone,
      age: user.age,
      gender: user.gender,
      profession: user.profession,
      type: user.type
    });

    // this.formGroup.get('email').disable();
  }

  getErrorAge() {
    return this.formGroup.get('age').hasError('required') ? 'Field is required' :
      this.formGroup.get('age').hasError('pattern') ? 'Age must be greater than 10' : '';
  }

  getErrorPhone(control) {
    let enteredPhone = control.value;
    let phoneCheck = /^(?=.*[0-9])(?=.{10,})/;
    return (!phoneCheck.test(enteredPhone) && enteredPhone) ? { 'requirements': true } : null;
  }

  update(formData) {
    this.fields = formData;

    this.apiService.put('admin/users/' + this.route.snapshot.params['id'], this.fields).subscribe( res => {
      if(res.status == 'success'){
        this.router.navigateByUrl('/admin/users');
      }
    }, err => {
      console.log(err);
    });
  }

}
