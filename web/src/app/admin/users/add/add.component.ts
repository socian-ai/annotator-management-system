import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ApiService } from '../../../core';
import { Router } from '@angular/router';

interface gender {
  value: string;
  viewValue: string;
}

interface type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'user-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class UserAddComponent implements OnInit {
  formGroup: FormGroup;
  fields: any;

  genders: gender[] = [
    {value: 'male', viewValue: 'Male'},
    {value: 'female', viewValue: 'Female'}
  ];

  types: type[] = [
    {value: 'admin', viewValue: 'Admin'},
    {value: 'annotator', viewValue: 'Annotator'},
    {value: 'linguistic', viewValue: 'Linguistic'}
  ];

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private router: Router
  ) { }

  ngOnInit(): void {
    let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    let phoneCheck = /^(?=.*[0-9])(?=.{10,})/;

    this.formGroup = this.formBuilder.group({
      'email': [null, [Validators.required, Validators.pattern(emailregex)]],
      'name': [null, Validators.required],
      'password': [null, [Validators.required, this.checkPassword]],
      'passwordConfirm': [null, [Validators.required]],
      'phone': [null, [Validators.required, Validators.pattern(phoneCheck)]],
      'age': [null, [Validators.required]],
      'gender': [null, Validators.required],
      'profession': [null, Validators.required],
      'type': [null, Validators.required]
    });

    this.formGroup.valueChanges.subscribe(field => {
      if (field.age < 18 || field.age > 100) {
        this.formGroup.get('age').setErrors({ ageError: true });
      } else {
        this.formGroup.get('age').setErrors(null);
      }

      if (field.password !== field.passwordConfirm) {
        this.formGroup.get('passwordConfirm').setErrors({ notSame: true });
      } else {
        this.formGroup.get('passwordConfirm').setErrors(null);
      }
    });
  }

  checkPassword(control) {
    let enteredPassword = control.value
    let passwordCheck = /^(?=.*[a-zA-Z\-0-9])(?=.{6,})/;
    return (!passwordCheck.test(enteredPassword) && enteredPassword) ? { 'requirements': true } : null;
  }

  getErrorEmail() {
    return this.formGroup.get('email').hasError('required') ? 'Field is required' :
      this.formGroup.get('email').hasError('pattern') ? 'Not a valid email address' :
        this.formGroup.get('email').hasError('alreadyInUse') ? 'This email address is already in use' : '';
  }

  getErrorAge() {
    return this.formGroup.get('age').hasError('required') ? 'Field is required' :
      this.formGroup.get('age').hasError('ageError') ? 'Age must be greater than 17 and less than 100' : '';
  }

  getErrorPhone() {
    return this.formGroup.get('phone').hasError('required') ? 'Field is required' :
      this.formGroup.get('phone').hasError('pattern') ? 'Phone needs to be at least 11 number' :
        this.formGroup.get('phone').hasError('alreadyInUse') ? 'This phone is already in use' : '';
  }

  createUser(formData) {
    this.apiService.post('admin/users', formData).subscribe( res => {
      if(res.status == 'success'){
        this.router.navigateByUrl('/admin/users');
      }
    }, err => {
      err.errors.forEach(error => {
        if(error.param == "email") {
           this.formGroup.get('email').setErrors({ alreadyInUse: true });
        }

        if(error.param == "phone") {
          this.formGroup.get('phone').setErrors({ alreadyInUse: true });
        }
      })
    });
  }

}
