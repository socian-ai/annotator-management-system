import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from '../../core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  group: any;
  validation_complete: Number;
  number_of_data: Number;
  displayedColumns: string[] = [];
  dataSource: any;
  data: any;
  kappa_socres: any;
  instanceType: string = 'sentence-sentiment';
  dataType: string = 'final-data';
  filterType: string = 'all-data';
  groupID: string;
  
  @ViewChild('paginator') paginator: MatPaginator;

  filter_types = [
    { value: 'all-data', viewValue: 'All Data' },
    { value: 'validation-complete', viewValue: 'Validation Complete' },
    { value: 'validation-incomplete', viewValue: 'Validation Incomplete' }
  ];

  sentiments = [
    { value: 'sentence-sentiment', viewValue: 'sentence level sentiment' },
    { value: 'paragraph-sentiment', viewValue: 'paragraph level sentiment' },
    { value: 'document-sentiment', viewValue: 'document level sentiment' },
    { value: 'sentence-emotion', viewValue: 'sentence level emotion' },
    { value: 'paragraph-emotion', viewValue: 'paragraph level emotion' }
  ];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      
      if (params['dataType'] && params['filterType'] && params['instanceType']) {
        this.filterType = params['filterType'];
        this.dataType = params['dataType'];
        this.instanceType = params['instanceType'];
      }

      if (params['groupID']) {
        this.groupID = params['groupID'];
      }

      this.getData();
    });
  }

  getData() {
    this.apiService.get(
      "admin/group?groupID=" + this.groupID
      + "&instanceType=" + this.instanceType + "&filterType=" + this.filterType).subscribe(res => {
        if (res.status == 'success') {
          this.group = res.group;
          this.kappa_socres = res.kappa_socres;
          this.validation_complete = res.validation_complete;
          this.number_of_data = res.number_of_data;
          this.data = res.data;

          this.displayedColumns = ['serial', 'data'];

          for (let user of this.group.users) {
            this.displayedColumns.push(user.name);
          }

          this.displayedColumns.push('linguistic');

          if (this.dataType == 'final-data') {
            this.displayedColumns.push('final');
          }

          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.paginator = this.paginator;
        }
      });
  }
  
  filterData(value) {
    if (value == 'validation-complete') {
      this.dataSource.data = this.data.filter( instance => instance.validation)
    } else if (value == 'validation-incomplete'){
      this.dataSource.data = this.data.filter( instance => !instance.validation)
    } else {
      this.dataSource.data = this.data
    } 
  }
}
