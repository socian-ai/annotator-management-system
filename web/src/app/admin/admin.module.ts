import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminAuthResolverService } from './admin-auth-resolver.service';
import { SharedModule } from '../shared';
import { AdminRoutingModule } from './admin-routing.module';
import { UserShowComponent } from './users/show/show.component';
import { UserAddComponent } from './users/add/add.component';
import { UserEditComponent } from './users/edit/edit.component';
import { GroupShowComponent } from './groups/show/show.component';
import { GroupAddComponent } from './groups/add/add.component';
import { GroupEditComponent } from './groups/edit/edit.component';
import { ClassDistributionComponent } from './graphs/class-distribution/class-distribution.component';
import { DomainDistributionComponent } from './graphs/domain-distribution/domain-distribution.component';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatDialogModule } from '@angular/material/dialog';
import { AssignUsersComponent } from './groups/assign-users/assign-users.component';
import { AssignTasksComponent } from './groups/assign-tasks/assign-tasks.component';
import { UploadComponent } from './data/upload/upload.component';
import { UploadHistoryComponent } from './data/upload-history/upload-history.component';
import { DownloadComponent } from './data/download/download.component';
import { UserDetailsComponent } from './users/user-details/user-details.component';
import { OverallStatisticsComponent } from './graphs/overall-statistics/overall-statistics.component';
import { GroupStatusComponent } from './groups/group-status/group-status.component';
import { HistoryComponent } from './history/history.component';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdminRoutingModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule
  ],
  declarations: [
    UserShowComponent,
    UserAddComponent,
    UserEditComponent,
    GroupShowComponent,
    GroupAddComponent,
    GroupEditComponent,
    ClassDistributionComponent,
    DomainDistributionComponent,
    AssignUsersComponent,
    AssignTasksComponent,
    UploadComponent,
    UploadHistoryComponent,
    DownloadComponent,
    UserDetailsComponent,
    OverallStatisticsComponent,
    GroupStatusComponent,
    HistoryComponent
  ],
  providers: [
    AdminAuthResolverService
  ]
})
export class AdminModule {}
