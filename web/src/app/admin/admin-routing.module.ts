import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserShowComponent } from './users/show/show.component';
import { UserAddComponent } from './users/add/add.component';
import { UserEditComponent } from './users/edit/edit.component';
import { GroupShowComponent } from './groups/show/show.component';
import { GroupAddComponent } from './groups/add/add.component';
import { GroupEditComponent } from './groups/edit/edit.component';
import { AssignUsersComponent } from './groups/assign-users/assign-users.component';
import { AssignTasksComponent } from './groups/assign-tasks/assign-tasks.component';
import { ClassDistributionComponent } from './graphs/class-distribution/class-distribution.component';
import { DomainDistributionComponent } from './graphs/domain-distribution/domain-distribution.component';
import { AdminAuthResolverService } from './admin-auth-resolver.service';
import { UploadComponent } from './data/upload/upload.component';
import { UploadHistoryComponent } from './data/upload-history/upload-history.component';
import { DownloadComponent } from './data/download/download.component';
import { UserDetailsComponent } from './users/user-details/user-details.component';
import { OverallStatisticsComponent } from './graphs/overall-statistics/overall-statistics.component';
import { GroupStatusComponent } from './groups/group-status/group-status.component';
import { HistoryComponent } from './history/history.component';


const routes: Routes = [
  {
    path: 'admin/users',
    component: UserShowComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/users/add',
    component: UserAddComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/users/edit/:id',
    component: UserEditComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/users/details/:id',
    component: UserDetailsComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/groups',
    component: GroupShowComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/groups/add',
    component: GroupAddComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/groups/details/:id',
    component: GroupStatusComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/groups/:id',
    component: GroupEditComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/groups/assign-users/:id',
    component: AssignUsersComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/groups/assign-tasks/:id',
    component: AssignTasksComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/group-history',
    component: HistoryComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/class-distribution',
    component: ClassDistributionComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/domain-distribution',
    component: DomainDistributionComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/overall-statistics',
    component: OverallStatisticsComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/data-upload',
    component: UploadComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/upload-history',
    component: UploadHistoryComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  },
  {
    path: 'admin/download-data',
    component: DownloadComponent,
    resolve: {
      isAuthenticated: AdminAuthResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
