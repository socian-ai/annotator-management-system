import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from '../../../core';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../../../shared/dialog/dialog.component';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class GroupShowComponent implements OnInit {
  groups: any;
  displayedColumns: string[] = ['serial', 'name', 'description', 'group_type', 'users', 'action'];
  dataSource = new MatTableDataSource(this.groups);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private apiService: ApiService, 
    public dialog: MatDialog,
    breakpointObserver: BreakpointObserver
  ) {

    breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = result.matches ?
        ['serial', 'name', 'description', 'group_type', 'users', 'action'] :
        ['serial', 'name', 'description', 'group_type', 'users', 'action'];
    });
  }

  ngOnInit() {
    this.getAllGroups();
  }

  getAllGroups() {
    this.apiService.get('admin/groups').subscribe(res => {
      if (res.status == 'success') {
        this.groups = res.groups;
        this.dataSource.data = this.groups;
      }
    });
  }

  delete(id) {
    this.apiService.delete('admin/groups/' + id).subscribe(res => {
      if (res.status == 'success') {
        this.groups = this.groups.filter(group => group._id != id);
        this.dataSource.data = this.groups;
      }
    }, err => {
      console.log(err);
    });
  }

  openDialog(id: string): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      data: {
        title: 'Confirmation',
        message: "Are you sure you want to delete the Group?",
        result: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.delete(id);
      }
    });
  }

  groupChange(groupType) {
    this.apiService.get('admin/groups?groupType=' + groupType).subscribe(res => {
      if (res.status == 'success') {
        this.groups = res.groups;
        this.dataSource.data = this.groups;
      }
    });
  }
}
