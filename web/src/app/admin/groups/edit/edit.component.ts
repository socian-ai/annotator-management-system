import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { ApiService } from '../../../core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'edit-group',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class GroupEditComponent implements OnInit {
  formGroup: FormGroup;

  selectedUsers: any;
  group: any;

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required],
      'description': [null, Validators.required]
    });

    this.apiService.patch('admin/groups/' + this.route.snapshot.params['id']).subscribe( res => {
      if(res.status == 'success'){
        this.group = res.group;

        this.formGroup.patchValue({
          name: this.group.name,
          description: this.group.description
        });
      }
    });
  }

  checkUser(id) {
    let is_confirm = confirm("Are you sure you want to add/remove this user from the group?");
    if(is_confirm) {
      this.apiService.put('admin/groups', {"id": id}).subscribe( res => {
        if(res.status == 'success'){
          if(!res.available) {
            alert("Sorry, This user can't be added/removed.");

            let users = this.formGroup.getRawValue().users;

            if(this.group.users.includes(id)) {
              users.push(id)
            } else {
              if(users.length > 0) {
                users = users .filter(user => user != id);
              }
            }

            this.formGroup.patchValue({
              users: users
            });
          }
        }
      }, err => {
        console.log(err);
      });
    } else {
      let users = this.formGroup.getRawValue().users;

      if(this.group.users.includes(id)) {
        users.push(id)
      } else {
        if(users.length > 0) {
          users = users .filter(user => user != id);
        }
      }

      this.formGroup.patchValue({
        users: users
      });
    }

  }

  filterOptions(value) {
    this.selectedUsers = this.search(value);
  }

  search(value: string) {
    // let filter = value.toLowerCase();
    // return this.users.filter(user => {
    //   return user.name.toLowerCase().startsWith(filter) || user.email.toLowerCase().startsWith(filter)
    // });
  }

  updateGroup(formData) {
    this.apiService.put('admin/groups/' + this.route.snapshot.params['id'], formData).subscribe( res => {
      if(res.status == 'success'){
        this.router.navigateByUrl('/admin/groups');
      }
    }, err => {
      console.log(err);
    });
  }
}
