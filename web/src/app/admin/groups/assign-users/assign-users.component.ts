import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import {Router, ActivatedRoute} from '@angular/router';
import { ApiService } from '../../../core';

import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';

@Component({
  selector: 'app-assign-users',
  templateUrl: './assign-users.component.html',
  styleUrls: ['./assign-users.component.css']
})
export class AssignUsersComponent implements OnInit {
  selection = new SelectionModel(true, []);
  displayedColumns: string[] = ['serial', 'name', 'email', 'type', 'phone'];
  dataSource: any;
  group: any;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private apiService: ApiService, 
    breakpointObserver: BreakpointObserver,
    private router: Router,
    private route: ActivatedRoute
    ) {

    breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = result.matches ? 
          ['serial', 'name', 'email', 'type', 'phone'] : 
          ['serial', 'name', 'email', 'type', 'phone'];
    });
   }

  ngOnInit(): void {

    this.apiService.patch('admin/groups/' + this.route.snapshot.params['id']).subscribe( res => {
      if(res.status == 'success'){
        this.group = res.group;

        this.apiService.get('admin/groups/users/' + this.route.snapshot.params['id']).subscribe( res => {
          if(res.status == 'success'){
            this.dataSource = new MatTableDataSource(res.user);
          }
        });
      }
    });
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
        
  }

  assign() {
    if(this.selection.selected.length > 0) {
      let fields = {
        group_id: this.group._id,
        users: []
      }

      this.selection.selected.forEach( user => {
        fields.users.push(user._id);
      });

      if(this.group.users.length == 0) {
        this.apiService.post('admin/groups/assign-users', fields).subscribe( res => {
          if(res.status == 'success'){
            this.router.navigateByUrl('/admin/groups');
          }
        });
      } else {
        this.apiService.post('admin/groups/add-users', fields).subscribe( res => {
          if(res.status == 'success'){
            this.router.navigateByUrl('/admin/groups/details/' + fields.group_id);
          }
        });
      }
      
    } else {
      alert("You must assgin at least 1 user");
    }
  }
}
