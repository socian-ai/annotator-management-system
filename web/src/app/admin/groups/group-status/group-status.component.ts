import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from '../../../core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-group-status',
  templateUrl: './group-status.component.html',
  styleUrls: ['./group-status.component.css']
})
export class GroupStatusComponent implements OnInit {
  group: any;
  users: any;
  deactiveUsers = [];
  linguistics: any;
  is_fleiss_kappa_active: boolean = true;
  group_details: any;
  annotator_status = [];
  selectedValue = 'sentence-sentiment';
  displayedColumns: string[] = [];
  dataSource: any;
  sentiments = [
    { value: 'sentence-sentiment', viewValue: 'sentence level sentiment' },
    { value: 'paragraph-sentiment', viewValue: 'paragraph level sentiment' },
    { value: 'document-sentiment', viewValue: 'document level sentiment' },
    { value: 'sentence-emotion', viewValue: 'sentence level emotion' },
    { value: 'paragraph-emotion', viewValue: 'paragraph level emotion' }
  ];

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.calculatePerformance();
    this.changeDataType(this.selectedValue);
  }

  changeDataType(dataType) {
    this.is_fleiss_kappa_active = true;

    this.apiService.get("admin/groups/" + this.route.snapshot.params['id'] + "?instanceType=" + dataType).subscribe(res => {
      if (res.status == 'success') {
        this.group = res.group;
        this.group_details = res.group_details;
      }
    });

    this.apiService.get("admin/groups/label-data/" + this.route.snapshot.params['id'] + "/" + dataType).subscribe(res => {

      if (res.status == 'success') {
        this.users = res.users;
        this.linguistics = res.linguistics;
        this.displayedColumns = ['serial', 'data'];

        for (let user of this.users) {
          this.displayedColumns.push(user.name);
        }
        let i = 1;
        for (let user of this.linguistics) {
          this.displayedColumns.push('L' + i + ': ' + user.name);
          i++;
        }

        this.dataSource = new MatTableDataSource(res.data);
      }
    });
  }

  calculatePerformance() {
    this.apiService.get("admin/groups/annotator-status/" + this.route.snapshot.params['id'] + '/' + this.selectedValue).subscribe(res => {
      if (res.status == 'success') {
        this.annotator_status = res.annotator_status;

        let accuracy = this.annotator_status.some((annotator) => annotator.accuracy < 70);
        
        if (!accuracy) {
          this.is_fleiss_kappa_active = false;
        } else {
          this.is_fleiss_kappa_active = true;
        }
      }
    });
  }

  calculateKappa() {
    this.apiService.get("admin/groups/calculate-kappa/" + this.route.snapshot.params['id'] + '/' + this.selectedValue).subscribe(res => {
      if (res.status == 'success') {
        this.router.navigateByUrl('/admin/group-history?groupID=' + this.route.snapshot.params['id']);
      }
    });
  }

  selectUser(id, event) {
    if (event.checked) {
      this.deactiveUsers.push(id);
    } else {
      this.deactiveUsers = this.deactiveUsers.filter(user => user != id);
    }
  }

  deactiveUser() {
    if (this.deactiveUsers.length > 0) {
      let fields = {
        users: this.deactiveUsers,
        group_id: this.route.snapshot.params['id']
      };

      this.apiService.post("admin/groups/remove-users", fields).subscribe(res => {
        if (res.status == 'success') {
          this.calculatePerformance();
        }
      });
    } else {
      return false
    }
  }
}
