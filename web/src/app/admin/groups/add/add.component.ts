import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from '../../../core';
import { Router } from '@angular/router';


interface Type {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'add-group',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class GroupAddComponent implements OnInit {
  formGroup: FormGroup;
  error: boolean = false;
  selectedValue: string;

  types: Type[] = [
    {value: 'Linguistic', viewValue: 'Linguistic'},
    {value: 'Annotator', viewValue: 'Annotator'}
  ];

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      'name': [null, Validators.required],
      'description': [null, Validators.required],
      'group_type': [null, Validators.required]
    });
  }

  createGroup(formData) {
    this.apiService.post('admin/groups', formData).subscribe( res => {
      if(res.status == 'success'){
        this.router.navigateByUrl('/admin/groups');
      } else {
        this.error = true;
      }
    }, err => {
      console.log(err);
      
    });
  }
}
