import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

interface SelectData {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-assign-tasks',
  templateUrl: './assign-tasks.component.html',
  styleUrls: ['./assign-tasks.component.css']
})
export class AssignTasksComponent implements OnInit {
  group: any;
  groups: any;
  formGroup: FormGroup;
  success: String;
  isSubmit: boolean = false;

  analysisType: SelectData[] = [
    { value: 'sentiment', viewValue: 'Sentiment' },
    { value: 'emotion', viewValue: 'Emotion' }
  ];

  instanceTypes: SelectData[] = [

  ];

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute
  ) {

  }

  ngOnInit(): void {
    let instanceCheck = /^[1-9]\d*$/;

    this.formGroup = this.formBuilder.group({
      'group_id': [null, Validators.required],
      'linguistic_group_id': [null, Validators.required],
      'annotators': [null, Validators.required],
      'analysisType': [null, Validators.required],
      'instanceType': [null, Validators.required],
      'no_of_instance': [null, [Validators.required, Validators.pattern(instanceCheck)]],
      'data_validation_percentage': [null, [Validators.required, Validators.pattern(instanceCheck)]],
      'linguistics': [null, Validators.required]
    });

    this.apiService.patch('admin/groups/' + this.route.snapshot.params['id']).subscribe(res => {
      if (res.status == 'success') {
        this.group = res.group;
        this.setGroupAndAnnotator();
      }
    });

    this.apiService.get('admin/groups?groupType=Linguistic').subscribe(res => {
      if (res.status == 'success') {
        this.groups = res.groups;
      }
    });
  }

  setGroupAndAnnotator() {
    let usrs = [];
    this.group.users.forEach(user => {
      usrs.push({
        "user_id": user._id
      })
    });
    this.formGroup.get('group_id').setValue(this.group._id);
    this.formGroup.get('annotators').setValue(usrs);
  }

  getErrorNoOfInstance() {
    return this.formGroup.get('no_of_instance').hasError('required') ? 'Field is required' :
      this.formGroup.get('no_of_instance').hasError('pattern') ? 'Number must be greater than 0' : '';
  }

  changeType(value) {
    this.formGroup.get('instanceType').setValue("");
    this.success = undefined;

    if (value == 'sentiment') {
      this.instanceTypes = [
        { value: 'sentence-sentiment', viewValue: 'Sentence' },
        { value: 'paragraph-sentiment', viewValue: 'Paragraph' },
        { value: 'document-sentiment', viewValue: 'Document' }
      ]
    } else if (value == 'emotion') {
      this.instanceTypes = [
        { value: 'sentence-emotion', viewValue: 'Sentence' },
        { value: 'paragraph-emotion', viewValue: 'Paragraph' },
      ]
    }
  }

  linguisticChange(group_id) {
    let linguistics = [];

    this.groups.forEach(group => {
      if (group._id == group_id) {
        group.users.forEach(user => {
          linguistics.push({
            "user_id": user._id
          })
        });
      }
    });

    this.formGroup.get('linguistic_group_id').setValue(group_id);
    this.formGroup.get('linguistics').setValue(linguistics);
  }

  assign(formData) {
    this.isSubmit = true;

    this.apiService.post('admin/groups/assign-tasks', formData).subscribe(res => {
      if (res.status == 'success') {
        this.success = res.message;

        this.formGroup.reset();
        this.setGroupAndAnnotator();
        this.isSubmit = false;
      }
    });
  }

}
