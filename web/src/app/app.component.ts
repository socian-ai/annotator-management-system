import { Component} from '@angular/core';
import { UserService, JwtService, HeaderService } from './core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'web';
  isAuthenticate = false;
  toggle: boolean = true;

  constructor (
    private userService: UserService,
    private router: Router,
    private jwtService: JwtService,
    private headerService: HeaderService
  ) {}

  ngOnInit() {
    this.userService.populate();

    this.userService.isAuthenticated.subscribe( authenticated => {
      if(authenticated) {
        this.isAuthenticate = true;

        setTimeout( () => {
          let user = this.jwtService.getDecodedAccessToken();

          if(this.router.url == "/") {
            if(user.type == "admin") {
              this.router.navigateByUrl('/admin/users?userType=all-users');
            } else if(user.type == "annotator") {
              this.router.navigateByUrl('/annotator/instructions');
            } else if(user.type == "linguistic") {
              this.router.navigateByUrl('/annotator/instructions');
            }
          }
        }, 1000);
      } else {
        setTimeout(()=> {
          if(this.router.url == "/register" || this.router.url 
          == "/forget-password" || this.router.url.split("/")[1] 
          == "reset"|| this.router.url.split("/")[1] == "verify") {
            
          } else {
            this.router.navigateByUrl('/');
          }
        }, 1000);
      }
    });

    this.headerService.leftSidebar.subscribe( data => {
        if(this.toggle) {
          this.toggle = false;
        } else {
          this.toggle = true;
        }
    })
  }
}
