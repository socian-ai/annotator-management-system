import { Component } from '@angular/core';
import { JwtService, ApiService } from '../../core/services';
import { Router } from '@angular/router';

@Component({
  selector: 'left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.css']
})
export class LeftSidebarComponent {
  userType: string;
  active: string;
  user: any;
  groups: any;
  history_groups: any;

  constructor(
    private router: Router,
    private jwtService: JwtService,
    private apiService: ApiService
  ) { }

  ngOnInit(): void {

    this.user = this.jwtService.getDecodedAccessToken();
    this.userType = this.user.type;

    setTimeout(() => {
      this.active = this.router.url;
    }, 1000);
  }

  logout() {
    window.localStorage.removeItem('token');
    window.location.reload();
  }

  checkReview() {
    if (this.userType == 'linguistic') {
      this.apiService.get('linguist/review/group/current-user').subscribe(res => {
        if (res.status == "success") {
          this.groups = res.groups;
        }
      });
    }
  }

  checkHistory() {
    if (this.userType == 'linguistic') {
      this.apiService.get('linguist/history/completed-groups').subscribe(res => {
        if (res.status == "success") {
          this.history_groups = res.groups;
        }
      });
    } else if (this.userType == 'admin') {
      this.apiService.get('admin/history/groups/completed').subscribe(res => {
        if (res.status == "success") {
          this.history_groups = res.groups;
        }
      });
    }
  }

  menuPermission(type) {
    if (this.user.group == null) {
      return false;
    }

    if (this.user.group.permission == type) {
      return true;
    } else {
      return false;
    }
  }
}
