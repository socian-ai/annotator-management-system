import { Component } from '@angular/core';
import { JwtService, HeaderService } from '../../core/services';
import { Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent{
  userType: string;
  active: string;
  user: any;

  constructor(private router: Router, private jwtService: JwtService, private headerService: HeaderService) { }

  ngOnInit(): void {
    this.user = this.jwtService.getDecodedAccessToken();
    this.userType = this.user.type;

    setTimeout( () => {
      this.active = this.router.url;
    }, 1000);
  }

  logout() {
    window.localStorage.removeItem('token');
    window.location.reload();
  }

  toggle() {
    this.headerService.expandCollapse("HI");
  }

  menuPermission(type) {
    if(this.user.group.permission == type) {
      return true;
    } else {
      return false;
    }
  }
}
