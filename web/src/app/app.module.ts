import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { AnnotatorModule } from './annotator/annotator.module';
import { AdminModule } from './admin/admin.module';
import { LinguistModule } from './linguist/linguist.module';
import { MaterialModule} from "./material.module";
import { FlexLayoutModule } from '@angular/flex-layout';
import { UserProfileComponent } from './profile/user-profile.component';
import { UserAuthResolverService } from './user-auth-resolver.service';

import {
  SharedModule,
  LeftSidebarComponent,
  HeaderComponent
} from './shared';
import { CoreModule } from './core/core.module';


@NgModule({
  declarations: [
    AppComponent,
    LeftSidebarComponent,
    HeaderComponent,
    UserProfileComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    SharedModule,
    AnnotatorModule,
    AuthModule,
    AppRoutingModule,
    AdminModule,
    LinguistModule,
    MaterialModule,
    FlexLayoutModule
  ],
  providers: [
    UserAuthResolverService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
