#!/usr/bin/env node

import db from "../db";

import User from "../models/User";
import Group from "../models/Group";
import Sentence from "../models/Sentence";
import Paragraph from "../models/Paragraph";
import Document from "../models/Document";
import GroupController from "../controllers/GroupController";

let args = process.argv;
args.shift();
args.shift();

db(() => {
    function createSentence() {
        return new Promise((resolve, reject) => {
            
            const group = new Group({
                "name": "Test Group"
            });
            
            group.save( err => {
                if (err) {
                    reject(err);
                } else {
                    resolve("Group created");
                }
            });
            
            console.log(group);

            const data = [
                {
                    "group": group._id,
                    "text": "This is demo text 1"
                },
                {
                    "group": group._id,
                    "text": "This is demo text 2"
                },
                {
                    "group": group._id,
                    "text": "This is demo text 3"
                }
            ];

            data.forEach(item => {
                const sentence = new Sentence(item);

                sentence.save(err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve("Sentence created");
                    }
                });
            });
        });
    }

    switch (args[0]) {
        case "admin":
            const user = new User({
                "name": "Socian Admin",
                "email": "admin@socian.ai",
                "phone": "8801914151587",
                "password": "123456",
                "age": 4,
                "gender": "male",
                "profession": "admin",
                "type": "admin"
            });

            user.save(err => {
                if (err) {
                    console.error(err);
                    process.exit(1);
                }

                console.log("Admin account created");
                process.exit(1);
            });

            break;
            
        case "sentence":
            createSentence().then(res => {
                console.log(res);
                process.exit(1);
            }, err => {
                console.error(err);
                process.exit(1);
            });

            break;
        default:
            console.log("Unrecognized argument");
            process.exit(1);
            break;
    }
});