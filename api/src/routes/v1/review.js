import express from "express";
import db from "../../db.js";
import jwtMiddleware from "../../middlewares/jwt.js";
import linguisticMiddleware from "../../middlewares/linguistic.js";

import ReviewSentenceSentimentController from "../../controllers/ReviewSentenceSentimentController.js";
import ReviewParagraphSentimentController from "../../controllers/ReviewParagraphSentimentController.js";
import ReviewDocumentSentimentController from "../../controllers/ReviewDocumentSentimentController.js";
import ReviewSentenceEmotionController from "../../controllers/ReviewSentenceEmotionController.js";
import ReviewParagraphEmotionController from "../../controllers/ReviewParagraphEmotionController.js";
import ReviewController from "../../controllers/ReviewController.js";
import GroupController from "../../controllers/GroupController.js";
import HistoryController from "../../controllers/HistoryController.js";

let router = express.Router();

// User jwtMiddleware
router.use(jwtMiddleware);

db(() => {
  router.route("/group").get(linguisticMiddleware, ReviewController.list);
  router.route("/final-data-validation").put(linguisticMiddleware, ReviewController.validationUpdate);
  
  router
    .route("/history/group")
    .get(linguisticMiddleware, HistoryController.list);
  // Get All Linguistic Review Data
  router.route("/group").get(linguisticMiddleware, ReviewController.list);
  // Get a Linguistic Review Data By Instance ID
  router.route("/get-a-instance").get(linguisticMiddleware, ReviewController.getDataByDataType);
  // Update a Linguistic Review Data By Instance ID
  router.route("/update-a-instance").put(linguisticMiddleware, ReviewController.updateDataByDataType);
  //  GET All Current Linguist In-Progress Groups
  router.route("/group/current-user").get(GroupController.currentUserGroups);

  router
    .route("/paragraph-emotion/:id")
    .patch(
      linguisticMiddleware,
      ReviewParagraphEmotionController.getParagraphByID
    );

  // Sentence Sentiment
  router
    .route("/sentence-sentiment")
    .get(linguisticMiddleware, ReviewSentenceSentimentController.list)
    .patch(linguisticMiddleware, ReviewSentenceSentimentController.retrive)
    .put(linguisticMiddleware, ReviewSentenceSentimentController.update);

  router
    .route("/sentence-sentiment/:id")
    .patch(
      linguisticMiddleware,
      ReviewSentenceSentimentController.getSentenceByID
    );

  // Paragraph Sentiment
  router
    .route("/paragraph-sentiment")
    .get(linguisticMiddleware, ReviewParagraphSentimentController.list)
    .patch(linguisticMiddleware, ReviewParagraphSentimentController.retrive)
    .put(linguisticMiddleware, ReviewParagraphSentimentController.update);

  router
    .route("/paragraph-sentiment/:id")
    .patch(
      linguisticMiddleware,
      ReviewParagraphSentimentController.getParagraphByID
    );

  // Document Sentiment
  router
    .route("/document-sentiment")
    .get(linguisticMiddleware, ReviewDocumentSentimentController.list)
    .patch(linguisticMiddleware, ReviewDocumentSentimentController.retrive)
    .put(linguisticMiddleware, ReviewDocumentSentimentController.update);

  router
    .route("/document-sentiment/:id")
    .patch(
      linguisticMiddleware,
      ReviewDocumentSentimentController.getDocumentByID
    );

  // Sentence Sentiment Emotion
  router
    .route("/sentence-emotion")
    .get(linguisticMiddleware, ReviewSentenceEmotionController.list)
    .patch(linguisticMiddleware, ReviewSentenceEmotionController.retrive)
    .put(linguisticMiddleware, ReviewSentenceEmotionController.update);

  router
    .route("/sentence-emotion/:id")
    .patch(
      linguisticMiddleware,
      ReviewSentenceEmotionController.getSentenceByID
    );

  // Paragraph Sentiment Emotion
  router
    .route("/paragraph-emotion")
    .get(linguisticMiddleware, ReviewParagraphEmotionController.list)
    .patch(linguisticMiddleware, ReviewParagraphEmotionController.retrive)
    .put(linguisticMiddleware, ReviewParagraphEmotionController.update);

  router
    .route("/paragraph-emotion/:id")
    .patch(
      linguisticMiddleware,
      ReviewParagraphEmotionController.getParagraphByID
    );

});

export default router;
