import express from "express";
import db from "../../db.js";
import multer from "multer";

// Import controllers
import AuthController from "../../controllers/AuthController.js";
import RegistrationController from "../../controllers/RegistrationController.js";
// Import Middleware
import jwtMiddleware from "../../middlewares/jwt.js";

// Routes started
let router = express.Router();

router.get("/", (req, res) => {
    res.json({ "message": "Welcome to our /v1 API" });
});

db(() => {

    // User Registration
    router.post("/register", RegistrationController.register);
    // Verify Account
    router.patch("/verify/:token", RegistrationController.verifyAccount);
    // User Login
    router.post("/login", AuthController.login);
    // Forget Fassword and Reset
    router.post("/forget-password", AuthController.forgetPassword);
    router.patch("/reset/:token", AuthController.checkRestPasswordLink);
    router.post("/reset", AuthController.restPassword);

    // Profile
    router.route("/me")
        .get(jwtMiddleware, AuthController.profile)
        .put(jwtMiddleware, AuthController.update);
});

export default router;