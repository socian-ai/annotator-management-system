import express from "express";
import db from "../../db.js";
import jwtMiddleware from "../../middlewares/jwt.js";
import linguisticMiddleware from "../../middlewares/linguistic.js";

import LinguisticSentenceSentimentController from "../../controllers/LinguisticSentenceSentimentController.js";
import LinguisticParagraphSentimentController from "../../controllers/LinguisticParagraphSentimentController.js";
import LinguisticDocumentSentimentController from "../../controllers/LinguisticDocumentSentimentController.js";
import LinguisticSentenceEmotionController from "../../controllers/LinguisticSentenceEmotionController.js";
import LinguisticParagraphEmotionController from "../../controllers/LinguisticParagraphEmotionController.js";
import HistoryController from "../../controllers/HistoryController.js";
import GroupController from "../../controllers/GroupController.js";

let router = express.Router();

// User jwtMiddleware
router.use(jwtMiddleware);

db(() => {
  // Sentence Sentiment
  router
    .route("/sentence-sentiment")
    .get(linguisticMiddleware, LinguisticSentenceSentimentController.list)
    .patch(linguisticMiddleware, LinguisticSentenceSentimentController.retrive)
    .put(linguisticMiddleware, LinguisticSentenceSentimentController.update);

  router
    .route("/sentence-sentiment/:id")
    .patch(
      linguisticMiddleware,
      LinguisticSentenceSentimentController.getSentenceByID
    );

  // Paragraph Sentiment
  router
    .route("/paragraph-sentiment")
    .get(linguisticMiddleware, LinguisticParagraphSentimentController.list)
    .patch(linguisticMiddleware, LinguisticParagraphSentimentController.retrive)
    .put(linguisticMiddleware, LinguisticParagraphSentimentController.update);

  router
    .route("/paragraph-sentiment/:id")
    .patch(
      linguisticMiddleware,
      LinguisticParagraphSentimentController.getParagraphByID
    );

  // Document Sentiment
  router
    .route("/document-sentiment")
    .get(linguisticMiddleware, LinguisticDocumentSentimentController.list)
    .patch(linguisticMiddleware, LinguisticDocumentSentimentController.retrive)
    .put(linguisticMiddleware, LinguisticDocumentSentimentController.update);

  router
    .route("/document-sentiment/:id")
    .patch(
      linguisticMiddleware,
      LinguisticDocumentSentimentController.getDocumentByID
    );

  // Sentence Sentiment Emotion
  router
    .route("/sentence-emotion")
    .get(linguisticMiddleware, LinguisticSentenceEmotionController.list)
    .patch(linguisticMiddleware, LinguisticSentenceEmotionController.retrive)
    .put(linguisticMiddleware, LinguisticSentenceEmotionController.update);

  router
    .route("/sentence-emotion/:id")
    .patch(
      linguisticMiddleware,
      LinguisticSentenceEmotionController.getSentenceByID
    );

  // Paragraph Sentiment Emotion
  router
    .route("/paragraph-emotion")
    .get(linguisticMiddleware, LinguisticParagraphEmotionController.list)
    .patch(linguisticMiddleware, LinguisticParagraphEmotionController.retrive)
    .put(linguisticMiddleware, LinguisticParagraphEmotionController.update);

  router
    .route("/paragraph-emotion/:id")
    .patch(
      linguisticMiddleware,
      LinguisticParagraphEmotionController.getParagraphByID
    );

  // Accepted Data
  router
    .route("/accepted-data/:dataType")
    .patch(linguisticMiddleware, HistoryController.linguistAcceptedData);

  router.route("/history").get(linguisticMiddleware, HistoryController.list);
  //  GET All Current Linguist Completed Groups
  router.route("/history/completed-groups").get(linguisticMiddleware, GroupController.currentUserCompletedGroups);
});

export default router;
