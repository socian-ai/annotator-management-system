import express from "express";
import db from "../../db.js";
import multer from "multer";
import adminMiddleware from "../../middlewares/admin.js";

// Controllers
import GroupController from "../../controllers/GroupController.js";
import UserController from "../../controllers/UserController.js";
import AdminUploadController from "../../controllers/AdminUploadController.js";
import DashboardController from "../../controllers/DashboardController.js";
import HistoryController from "../../controllers/HistoryController.js";
import ReviewController from "../../controllers/ReviewController.js";
import CalculateKappaController from "../../controllers/CalculateKappaController.js";

// const upload = multer({ dest: `${__dirname}/../../../uploads` });
// Routes
let router = express.Router();

db(() => {
  // Users POST
  router
    .route("/users")
    .post(adminMiddleware, UserController.create);

  // Users List By Params
  router
    .route("/users-list/:userType")
    .patch(adminMiddleware, UserController.list);

  // User By ID
  router
    .route("/users/:userID")
    .get(adminMiddleware, UserController.details)
    .put(adminMiddleware, UserController.update)
    .patch(adminMiddleware, UserController.retrive)
    .delete(adminMiddleware, UserController.delete);

  // Groups POST | GET
  router
    .route("/groups")
    .post(adminMiddleware, GroupController.create)
    .get(adminMiddleware, GroupController.list);

  // Group By ID
  router
    .route("/groups/:groupID")
    .patch(adminMiddleware, GroupController.retrive)
    .put(adminMiddleware, GroupController.update)
    .delete(adminMiddleware, GroupController.delete)
    .get(adminMiddleware, GroupController.details);

  // Annotator Labelled Data By User ID and Data Type
  router
    .route("/annotator/:userID/:dataType")
    .patch(adminMiddleware, HistoryController.annotatorLabelledData);
  // Annotation Status
  router
    .route("/groups/annotator-status/:groupID/:dataType")
    .get(adminMiddleware, GroupController.groupAnnotatorStatus);

  // Remove user to group and Group Datas
  router
    .route("/groups/remove-users")
    .post(adminMiddleware, UserController.removeUseToGroup);

  // add user by user id and group id
  router
    .route("/groups/add-users")
    .post(adminMiddleware, UserController.addUserToGroup);

  //get users list by groupID
  router
    .route("/groups/users/:groupID")
    .get(adminMiddleware, UserController.getUserList);

  //  GET Group Labelled Data By groupID
  router
    .route("/groups/current-user")
    .get(adminMiddleware, GroupController.currentUserGroups);

  //  GET Group Labelled Data By groupID
  router
    .route("/groups/label-data/:groupID/:dataType")
    .get(adminMiddleware, GroupController.labelledData);

  router
    .route("/groups/assign-users")
    .post(adminMiddleware, GroupController.assignUsersToGroup);
  router
    .route("/groups/assign-tasks")
    .post(adminMiddleware, GroupController.assignTasksToGroup);

  // Upload Data
  router
    .route("/data-upload")
    .post(adminMiddleware, AdminUploadController.dataUpload);

  // Upload History
  router
    .route("/upload-history")
    .get(adminMiddleware, AdminUploadController.uploadHistory);

  // Data Download
  router
    .route("/data-download/:dataType")
    .patch(adminMiddleware, AdminUploadController.download);

  router
    .route("/overall-statistics")
    .get(adminMiddleware, DashboardController.overallStatistics);

  router
    .route("/class-distributions")
    .get(adminMiddleware, DashboardController.classDistributions);

  router
    .route("/domain-distributions")
    .get(adminMiddleware, DashboardController.domainDistributions);

  router
    .route("/groups/calculate-kappa/:groupID/:dataType")
    .get(adminMiddleware, CalculateKappaController.calculateKappa);

  //  GET All Completed Groups List
  router.route("/history/groups/completed").get(adminMiddleware, GroupController.getAllCompletedGroups);
  // Get All Final Data
  router.route("/group").get(adminMiddleware, HistoryController.completedGroupHistory);
});

export default router;
