import express from "express";
import db from "../../db.js";
import multer from "multer";
import jwtMiddleware from "../../middlewares/jwt.js";
import annotatorMiddleware from "../../middlewares/annotator.js";

import AuthController from "../../controllers/AuthController.js";
import SentenceSentimentController from "../../controllers/SentenceSentimentController.js";
import ParagraphSentimentController from "../../controllers/ParagraphSentimentController.js";
import DocumentSentimentController from "../../controllers/DocumentSentimentController.js";
import SentenceEmotionController from "../../controllers/SentenceEmotionController.js";
import ParagraphEmotionController from "../../controllers/ParagraphEmotionController.js";
import HistoryController from "../../controllers/HistoryController.js";

// const upload = multer({ "dest": `${__dirname}/../../../uploads` });
let router = express.Router();

// User jwtMiddleware
router.use(jwtMiddleware);

db(() => {
    router.post("/logout", AuthController.logout);

    // Profile
    router.route("/group")
        .get(annotatorMiddleware, AuthController.group);

    // Sentence Sentiment
    router.route("/sentence-sentiment")
        .get(annotatorMiddleware, SentenceSentimentController.list)
        .patch(annotatorMiddleware, SentenceSentimentController.retrive)
        .put(annotatorMiddleware, SentenceSentimentController.update);

    router.route("/sentence-sentiment/:id")
        .patch(annotatorMiddleware, SentenceSentimentController.getSentenceByID);

    // Paragraph Sentiment
    router.route("/paragraph-sentiment")
        .get(annotatorMiddleware, ParagraphSentimentController.list)
        .patch(annotatorMiddleware, ParagraphSentimentController.retrive)
        .put(annotatorMiddleware, ParagraphSentimentController.update);

    router.route("/paragraph-sentiment/:id")
        .patch(annotatorMiddleware, ParagraphSentimentController.getParagraphByID);

    // Document Sentiment
    router.route("/document-sentiment")
        .get(annotatorMiddleware, DocumentSentimentController.list)
        .patch(annotatorMiddleware, DocumentSentimentController.retrive)
        .put(annotatorMiddleware, DocumentSentimentController.update);

    router.route("/document-sentiment/:id")
        .patch(annotatorMiddleware, DocumentSentimentController.getDocumentByID);

    // Sentence Sentiment Emotion
    router.route("/sentence-emotion")
        .get(annotatorMiddleware, SentenceEmotionController.list)
        .patch(annotatorMiddleware, SentenceEmotionController.retrive)
        .put(annotatorMiddleware, SentenceEmotionController.update);

    router.route("/sentence-emotion/:id")
        .patch(annotatorMiddleware, SentenceEmotionController.getSentenceByID);

    // Paragraph Sentiment Emotion
    router.route("/paragraph-emotion")
        .get(annotatorMiddleware, ParagraphEmotionController.list)
        .patch(annotatorMiddleware, ParagraphEmotionController.retrive)
        .put(annotatorMiddleware, ParagraphEmotionController.update);

    router.route("/paragraph-emotion/:id")
        .patch(annotatorMiddleware, ParagraphEmotionController.getParagraphByID);
    // Rejected Data
    router.route("/rejected-data/:dataType")
        .patch(annotatorMiddleware, HistoryController.rejectedData);
    // Accepted Data
    router.route("/accepted-data/:dataType")
        .patch(annotatorMiddleware, HistoryController.acceptedData);
});

export default router;