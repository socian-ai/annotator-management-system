import mongoose from "mongoose";

let Schema = mongoose.Schema;
let UploadHistorySchema = new Schema({
  data_type: { type: String, required: true },
  uploaded_data: { type: Number, required: true },
  similar_data: { type: Number, required: false },
  stored_data: { type: Number, required: false },
  
  created_at: { type: String, default: Date.now() },
  updated_at: { type: String, default: Date.now() },
});

let UploadHistory = mongoose.model(
  "upload_history",
  UploadHistorySchema
);

export default UploadHistory;
