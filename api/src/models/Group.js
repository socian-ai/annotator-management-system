import mongoose from "mongoose";

let Schema = mongoose.Schema;
let GroupSchema = new Schema({
    "name": { "type": String, "required": true },
    "description": { "type": String, "required": true },
    "users": [{ "type": Schema.Types.ObjectId, "ref": "User", "default": [] }],
    "group_type": { "type": String, "required": true, "default": "annotator" },
    "status": { "type": String, "required": false, "default": "active" },
    "created_at": { "type": String, "default": Date.now() },
    "updated_at": { "type": String, "default": Date.now() }
});

let Group = mongoose.model("Group", GroupSchema);

export default Group;