import mongoose from "mongoose";

let Schema = mongoose.Schema;
let ParagraphEmotionSchema = new Schema({
  unannotated_id: { type: String, required: true },
  text: { type: String, required: true },
  group: { type: Schema.Types.ObjectId, ref: "Group", required: false },
  linguistic_group: {
    type: Schema.Types.ObjectId,
    ref: "Group",
    required: false,
  },
  domain: { type: String, required: false },
  root_url: { type: String, required: false },
  category: { type: String, required: false },
  published_date: { type: String, required: false },
  parse_time: { type: String, required: false },
  linguistics: [
    {
      user_id: { type: Schema.Types.ObjectId, ref: "User", required: false },
      answer: { type: Object, default: {} },
    },
  ],
  annotators: [
    {
      user_id: { type: Schema.Types.ObjectId, ref: "User", required: false },
      answer: { type: Object, default: {} },
    },
  ],
  validation: { type: Boolean, required: false, default: false },
  created_at: { type: String, default: Date.now() },
  updated_at: { type: String, default: Date.now() },
});

let ParagraphEmotion = mongoose.model(
  "paragraph_emotion",
  ParagraphEmotionSchema
);

export default ParagraphEmotion;
