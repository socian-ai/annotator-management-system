import mongoose from "mongoose";

let Schema = mongoose.Schema;
let KappaScoreSchema = new Schema({
  group: { type: Schema.Types.ObjectId, ref: "Group", required: true },
  data_type: { type: String, required: true },
  data_from: { type: Number, required: true },
  data_to: { type: Number, required: true },
  score: { type: String, required: true },
  
  created_at: { type: String, default: Date.now() },
  updated_at: { type: String, default: Date.now() },
});

let KappaScore = mongoose.model(
  "kappa_score",
  KappaScoreSchema
);

export default KappaScore;
