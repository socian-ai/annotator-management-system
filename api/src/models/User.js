import mongoose from "mongoose";
import bcrypt from "bcryptjs";

let Schema = mongoose.Schema;

let UserSchema = new Schema({
    "name": { "type": String, "required": true },
    "email": { "type": String, "required": true, "unique": true },
    "phone": { "type": String, "required": true },
    "password": { "type": String, "required": true },
    "age": { "type": Number, "required": true },
    "gender": { "type": String, "required": true },
    "profession": { "type": String, "required": true },
    "type": { "type": String, "required": true },
    "status": { "type": String, "required": false, "default": "unverified" },
    "reset_token": { "type": String, "required": false, "default": null },
    "verify_token": { "type": String, "required": false, "default": null },
    "created_at": { "type": String, "default": Date.now() },
    "updated_at": { "type": String, "default": Date.now() }
});

// Hash the user's password before saving
UserSchema.pre("save", function(next) {
    hashPassword(this, next);
});

// Compare password with hashed
UserSchema.methods = {
    "comparePassowrd": function(candidatePassword, callback) {
        bcrypt.compare(candidatePassword, this.password, (err, matched) => {
            if (err) return callback(err);
            callback(undefined, matched);
        });
    }
};

/**
 * Password hash.
 *
 * @param user
 * @param next
 * @returns {*}
 */
function hashPassword(user, next) {
    let saltWorkFactor = 10;

    if (!user.isModified("password")) return next();

    bcrypt.genSalt(saltWorkFactor, (err, salt) => {
        if (err) return next(err);

        bcrypt.hash(user.password, salt, (err, hash) => {
            if (err) return next(err);

            user.password = hash;
            next();
        });
    });
}

let User = mongoose.model("User", UserSchema);

export default User;