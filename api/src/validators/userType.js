export default (value) => {
    const types = ["admin", "annotator", "linguistic"];

    if (types.indexOf(value) === -1) {
        return false;
    }

    return true;
}