import fs from "fs";

export default (value, file, size) => {
    const mimetypes = ["image/jpeg", "image/jpg", "image/png", "image/gif"];

    if (mimetypes.indexOf(file.mimetype) === -1) {
        fs.unlink(file.path);
        return false;
    }

    return true;
}