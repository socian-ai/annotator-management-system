import fs from "fs";

export default (value, file) => {
    if (file === undefined) {
        fs.unlink(file.path);
        return false;
    }
}