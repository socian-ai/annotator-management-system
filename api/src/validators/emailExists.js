export default (value, model, column) => {
    return new Promise((resolve, reject) => {
        let findable = {};

        if (column === undefined) {
            findable = {"email": value};
        } else {
            findable[column] = value;
        }

        model.find(findable, (err, hasRecord) => {
            if (hasRecord.length > 0) {
                reject(new Error(hasRecord));
            } else {
                resolve(hasRecord);
            }
        });
    })
}