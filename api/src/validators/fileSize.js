import fs from "fs";

export default (value, file, size) => {
    if (file.size > size) {
        fs.unlink(file.path);
        return false;
    }

    return true;
}