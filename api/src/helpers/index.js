import nodemailer from "nodemailer";

export default {
    "getExtension": fileName => {
        return fileName.split(".").pop();
    },

    "imageValidation": file => {
        file = file.split(".").pop();

        if (file === "jpg" || file === "jpeg" || file === "png") return true;

        return false;
    },

    "mail": (to, subject, body, from, callback) => {

        const transporter = nodemailer.createTransport({
            "host": "socian.ai",
            "port": 587,
            "secure": false,
            "auth": {
                "user": process.env.MAIL_ADDRESS,
                "pass": process.env.MAIL_PASS
            },
            "tls": {
                "rejectUnauthorized": false
            }
        });
        const mailer = {
            "from": (from !== undefined && typeof from === "string") ? from : process.env.MAIL_FROM,
            "to": to,
            "subject": subject,
            "html": body
        };

        if (from !== undefined && typeof from === "function") {
            transporter.sendMail(mailer, from);
        } else if (callback !== undefined) {
            transporter.sendMail(mailer, callback);
        } else {
            transporter.sendMail(mailer);
        }
    }
}