import express from "express";
import bodyParser from "body-parser";
import validator from "express-validator";
import cors from "cors";
import dotenv from "dotenv";

// Middlewares
import token from "./middlewares/jwt.js";

// Import routes.
import routes from "./routes/v1/index.js";
import annotatorRoutes from "./routes/v1/annotator.js";
import linguisticRoutes from "./routes/v1/linguistic.js";
import reviewRoutes from "./routes/v1/review.js";
import adminRoutes from "./routes/v1/admin.js";
import imageValidator from "./validators/imageValidator.js";
import fileSize from "./validators/fileSize.js";
import fileRequired from "./validators/fileRequired.js";
import userType from "./validators/userType.js";
import phoneExists from "./validators/phoneExists.js";
import emailExists from "./validators/emailExists.js";

let app = express();

// Setup application
dotenv.config();
// app.use("/uploads", express.static(`${__dirname}/../uploads`));
app.use(bodyParser.json());

app.use(
  validator({
    customValidators: {
      fileRequired: fileRequired,
      isImage: imageValidator,
      fileSize: fileSize,
      emailExists: emailExists,
      phoneExists: phoneExists,
    },
  })
);

// Apply cors
let whitelisted = [
  "http://es4.southcentralus.cloudapp.azure.com:5005",
  "http://localhost:4200",
  "http://mysticclouds.com:5005",
];
let corsOptions = {
  origin: (origin, callback) => {
    if (!origin || whitelisted.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      console.log(origin);
      callback(new Error(`Request is not allowed for ${origin}`));
    }
  },
  methods: "GET,POST,PATCH,PUT,DELETE,OPTIONS",
  credentials: true,
};
app.use(cors(corsOptions));

// Setup routes
app.get("/", (req, res) => {
  res.json({ message: "Welcome to our API" });
});

app.use("/api/v1", routes);
app.use("/api/v1/annotator", token, annotatorRoutes);
app.use("/api/v1/linguist", token, linguisticRoutes);
app.use("/api/v1/linguist/review", token, reviewRoutes);
app.use("/api/v1/admin", token, adminRoutes);

// Create server
const port = process.env.PORT || 5003;

app.listen(port, function () {
  console.log(`Server is listening on port ${port}`);
});
