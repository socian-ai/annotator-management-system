import SentenceSentiment from "../models/SentenceSentiment.js";

export default class LinguisticSentenceSentimentController {
    /**
     * Sentence Sentiment Submitted List
     *
     * @param req
     * @param res
     */
    static list(req, res) {
        SentenceSentiment.find({
                linguistics: {
                    $elemMatch: {
                        user_id: req.decodedToken._id,
                        answer: { $exists: true, $not: { $size: 0 } },
                    },
                },
            },
            null, { sort: { created_at: -1 } },
            (err, results) => {
                if (err) {
                    res.status(500).json({ status: "error", message: err });
                } else {
                    res.status(200).json({
                        status: "success",
                        submitted_list: results,
                    });
                }
            }
        );
    }

    /**
     * Get a Sentence by Group ID
     *
     * @param req
     * @param res
     *
     */
    static retrive(req, res) {
        SentenceSentiment.findOne({
                linguistics: {
                    $elemMatch: { user_id: req.decodedToken._id, answer: undefined },
                },
            },
            (err, sentence) => {
                if (err) {
                    res.status(500).json({ status: "error", message: err });
                } else {
                    res.status(200).json({ status: "success", sentence: sentence });
                }
            }
        ).select("-linguistics -created_at -updated_at");
    }

    /**
     * Get a Sentence by ID
     *
     * @param req
     * @param res
     *
     */
    static getSentenceByID(req, res) {
        SentenceSentiment.findById({ _id: req.params.id }, (err, sentence) => {
            if (err) {
                res.status(500).json({ status: "error", message: err });
            } else {
                if (sentence.linguistics.length > 0) {
                    let is_user_exist = false;
                    sentence.linguistics.forEach((user) => {
                        if (user.user_id == req.decodedToken._id) {
                            is_user_exist = true;
                        }
                    });

                    if (is_user_exist) {
                        res.status(200).json({ status: "success", sentence: sentence });
                    } else {
                        res
                            .status(200)
                            .json({
                                status: "failed",
                                message: "Sorry you can't edit this sentence sentiment",
                            });
                    }
                } else {
                    res
                        .status(200)
                        .json({
                            status: "failed",
                            message: "Sorry you can't edit this sentence sentiment",
                        });
                }
            }
        }).select("-created_at -updated_at");
    }

    /**
     * Update Sentence Sentiment
     *
     * @param req
     * @param res
     */
    static update(req, res) {
        SentenceSentiment.findById({ _id: req.body.id }, (err, sentence) => {
            if (err) {
                res.status(500).json({ status: "error", message: err });
            } else {
                sentence.linguistics.forEach((linguistic) => {
                    if (linguistic.user_id == req.decodedToken._id) {
                        if (req.body.skip) {
                            linguistic.answer = {
                                "skip": true
                            }
                        } else if (req.body.subjectivity == 'objective') {
                            linguistic.answer = {
                                "subjectivity": req.body.subjectivity
                            };
                        } else {
                            linguistic.answer = {
                                "subjectivity": req.body.subjectivity,
                                "polarity": req.body.polarity
                            };
                        }
                    }
                });

                sentence.save((err) => {
                    if (err) {
                        res
                            .status(500)
                            .json({
                                status: "error",
                                message: "Whoops! Something went wrong",
                            });
                    } else {
                        if (req.body.update) {
                            res
                                .status(200)
                                .json({
                                    status: "success",
                                    message: "Sentence Sentiment Updated Success",
                                });
                        } else {
                            LinguisticSentenceSentimentController.retrive(req, res);
                        }
                    }
                });
            }
        });
    }
}