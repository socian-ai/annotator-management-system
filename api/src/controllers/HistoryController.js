import LinguisticData from "../models/LinguisticData.js";
import FinalData from "../models/FinalData.js";
import Group from "../models/Group.js";
import SentenceSentiment from "../models/SentenceSentiment.js";
import ParagraphSentiment from "../models/ParagraphSentiment.js";
import DocumentSentiment from "../models/DocumentSentiment.js";
import SentenceEmotion from "../models/SentenceEmotion.js";
import ParagraphEmotion from "../models/ParagraphEmotion.js";
import KappaScore from "../models/KappaScore.js";
import MajorityMatchController from "./MajorityMatchController.js";

export default class HistoryController {
  /**
   * Rejected Data List
   *
   * @param req
   * @param res
   */
  static rejectedData(req, res) {
    LinguisticData.find(
      {
        annotators: { $elemMatch: { user_id: req.decodedToken._id } },
        data_type: req.params.dataType,
      },
      (err, results) => {
        if (err) {
          res.status(500).json({ status: "error", message: err });
        } else {
          res.status(200).json({
            status: "success",
            rejected_data: results,
          });
        }
      }
    );
  }

  /**
   * Accepted Data List
   *
   * @param req
   * @param res
   */
  static acceptedData(req, res) {
    FinalData.find(
      {
        annotators: { $elemMatch: { user_id: req.decodedToken._id } },
        data_type: req.params.dataType,
      },
      (err, results) => {
        if (err) {
          res.status(500).json({ status: "error", message: err });
        } else {
          res.status(200).json({
            status: "success",
            accepted_data: results,
          });
        }
      }
    );
  }

  /**
   * Linguist Accepted Data List
   *
   * @param req
   * @param res
   */
  static linguistAcceptedData(req, res) {
    FinalData.find(
      {
        linguistics: { $elemMatch: { user_id: req.decodedToken._id } },
        data_type: req.params.dataType,
      },
      (err, results) => {
        if (err) {
          res.status(500).json({ status: "error", message: err });
        } else {
          res.status(200).json({
            status: "success",
            accepted_data: results,
          });
        }
      }
    );
  }

  /**
   * Annotator Labelled Data By User ID and Data Type
   *
   * @param req(userID, dataType)
   * @param res
   */
  static annotatorLabelledData(req, res) {
    if (req.params.dataType == "sentence-sentiment") {
      SentenceSentiment.find(
        {
          annotators: {
            $elemMatch: {
              user_id: req.params.userID,
              answer: { $exists: true, $not: { $size: 0 } },
            },
          },
        },
        (err, results) => {
          if (err) {
            res.status(500).json({ status: "error", message: err });
          } else {
            res.status(200).json({
              status: "success",
              submitted_list: results,
            });
          }
        }
      );
    } else if (req.params.dataType == "paragraph-sentiment") {
      ParagraphSentiment.find(
        {
          annotators: {
            $elemMatch: {
              user_id: req.params.userID,
              answer: { $exists: true, $not: { $size: 0 } },
            },
          },
        },
        (err, results) => {
          if (err) {
            res.status(500).json({ status: "error", message: err });
          } else {
            res.status(200).json({
              status: "success",
              submitted_list: results,
            });
          }
        }
      );
    } else if (req.params.dataType == "document-sentiment") {
      DocumentSentiment.find(
        {
          annotators: {
            $elemMatch: {
              user_id: req.params.userID,
              answer: { $exists: true, $not: { $size: 0 } },
            },
          },
        },
        (err, results) => {
          if (err) {
            res.status(500).json({ status: "error", message: err });
          } else {
            res.status(200).json({
              status: "success",
              submitted_list: results,
            });
          }
        }
      );
    } else if (req.params.dataType == "sentence-emotion") {
      SentenceEmotion.find(
        {
          annotators: {
            $elemMatch: {
              user_id: req.params.userID,
              answer: { $exists: true, $not: { $size: 0 } },
            },
          },
        },
        (err, results) => {
          if (err) {
            res.status(500).json({ status: "error", message: err });
          } else {
            res.status(200).json({
              status: "success",
              submitted_list: results,
            });
          }
        }
      );
    } else if (req.params.dataType == "paragraph-emotion") {
      ParagraphEmotion.find(
        {
          annotators: {
            $elemMatch: {
              user_id: req.params.userID,
              answer: { $exists: true, $not: { $size: 0 } },
            },
          },
        },
        (err, results) => {
          if (err) {
            res.status(500).json({ status: "error", message: err });
          } else {
            res.status(200).json({
              status: "success",
              submitted_list: results,
            });
          }
        }
      );
    }
  }

  /**
   * 
   * @param {*} req 
   * @param {*} res 
   */

  static async list(req, res) {
    const groupID = req.query.groupID;
    const dataType = req.query.dataType;

    if (dataType == "sentence-sentiment") {
      const ss = await SentenceSentiment.find({
        linguistic_group: groupID,
        validation: true,
      });
      res.json({ status: "success", data: ss });
    } else if (dataType == "sentence-emotion") {
      const se = await SentenceEmotion.find({
        linguistic_group: groupID,
        validation: true,
      });
      res.json({ status: "success", data: se });
    } else if (dataType == "paragraph-sentiment") {
      const ps = await ParagraphSentiment.find({
        linguistic_group: groupID,
        validation: true,
      });
      res.json({ status: "success", data: ps });
    } else if (dataType == "paragraph-emotion") {
      const pe = await ParagraphEmotion.find({
        linguistic_group: groupID,
        validation: true,
      });
      res.json({ status: "success", data: pe });
    } else if (dataType == "document_sentiment") {
      const ds = await DocumentSentiment.find({
        linguistic_group: groupID,
        validation: true,
      });
      res.json({ status: "success", data: ds });
    }
  }


  /**
   * Admin Kappa Applied Group History
   * 
   * @param req
   * @param res
   */
  static async completedGroupHistory(req, res) {
    const groupID = req.query.groupID;
    const instanceType = req.query.instanceType;
    const filterType = req.query.filterType;

    if (instanceType == "sentence-sentiment") {
      const sentence_sentiment_list = await HistoryController.sentenceSentimentList(groupID, filterType);
      res.json(sentence_sentiment_list);
    } else if (instanceType == "sentence-emotion") {
      const sentence_emotion_list = await HistoryController.sentenceEmotionList(groupID, filterType);
      res.json(sentence_emotion_list);
    } else if (instanceType == "paragraph-sentiment") {
      const paragraph_sentiment_list = await HistoryController.paragraphSentimentList(groupID, filterType);
      res.json(paragraph_sentiment_list);
    } else if (instanceType == "paragraph-emotion") {
      const paragraph_emotion_list = await HistoryController.paragraphEmotionList(groupID, filterType);
      res.json(paragraph_emotion_list);
    } else if (instanceType == "document-sentiment") {
      const document_sentiment_list = await HistoryController.documentSentimentList(groupID, filterType);
      res.json(document_sentiment_list);
    } else {
      res.json({
        "status": "failed",
        "message": "Invalid request"
      });
    }
  }

  /**
   * 
   * @param {dataType, filterType} 
   * 
   */

  static async sentenceSentimentList(groupID, filterType) {
    const group = await Group.findById(groupID).populate("users", "name email status");
    let dataQuery = { group: groupID, data_type: 'sentence-sentiment' };

    if (filterType == 'validation-complete') {
      dataQuery['validation'] = true;
    } else if (filterType == 'validation-incomplete') {
      dataQuery['validation'] = false;
    }

    let sentence_sentiment_final_data = await FinalData.find(dataQuery).lean();
    let sentence_sentiment_linguistic_data = await LinguisticData.find(dataQuery).lean();
    const kappa_socres = await KappaScore.find(dataQuery);

    const sentence_sentiment_validation_count = await FinalData.count({
      group: groupID,
      validation: true,
      data_type: 'sentence-sentiment'
    });

    let sentence_sentiment_data = sentence_sentiment_final_data.concat(sentence_sentiment_linguistic_data);
    sentence_sentiment_data = sentence_sentiment_data.filter( sentence => {
      if(sentence.linguistics.length){
        sentence['linguistic_answer'] =  MajorityMatchController.sentenceSentimentMajorityMatch(sentence.linguistics);
      }
      
      return sentence
    });

    return {
      status: "success",
      group: group,
      data: sentence_sentiment_data,
      validation_complete: sentence_sentiment_validation_count,
      number_of_data: sentence_sentiment_data.length,
      kappa_socres: kappa_socres
    }
  }

  /**
   * 
   * @param {dataType, filterType} 
   * 
   */

  static async paragraphSentimentList(groupID, filterType) {
    const group = await Group.findById(groupID).populate("users", "name email status");
    let dataQuery = { group: groupID, data_type: 'paragraph-sentiment' };

    if (filterType == 'validation-complete') {
      dataQuery['validation'] = true;
    } else if (filterType == 'validation-incomplete') {
      dataQuery['validation'] = false;
    }

    let paragraph_sentiment_final_data = await FinalData.find(dataQuery).lean();
    let paragraph_sentiment_linguistic_data = await LinguisticData.find(dataQuery).lean();
    const kappa_socres = await KappaScore.find(dataQuery);

    const paragraph_sentiment_validation_count = await FinalData.count({
      group: groupID,
      validation: true,
      data_type: 'paragraph-sentiment'
    });

    let paragraph_sentiment_data = paragraph_sentiment_final_data.concat(paragraph_sentiment_linguistic_data);

    paragraph_sentiment_data = paragraph_sentiment_data.filter( paragraph => {
      if(paragraph.linguistics.length){
        paragraph['linguistic_answer'] =  MajorityMatchController.paragraphAndDocumentSentimentMajorityMatch(paragraph.linguistics);
      }
      
      return paragraph
    });

    return {
      status: "success",
      group: group,
      data: paragraph_sentiment_data,
      validation_complete: paragraph_sentiment_validation_count,
      number_of_data: paragraph_sentiment_data.length,
      kappa_socres: kappa_socres
    }
  }

  /**
   * 
   * @param {dataType, filterType} 
   * 
   */

  static async documentSentimentList(groupID, filterType) {
    const group = await Group.findById(groupID).populate("users", "name email status");
    let dataQuery = { group: groupID, data_type: 'document-sentiment' };

    if (filterType == 'validation-complete') {
      dataQuery['validation'] = true;
    } else if (filterType == 'validation-incomplete') {
      dataQuery['validation'] = false;
    }

    let document_sentiment_final_data = await FinalData.find(dataQuery).lean();
    let document_sentiment_linguistic_data = await LinguisticData.find(dataQuery).lean();
    const kappa_socres = await KappaScore.find(dataQuery);

    const document_sentiment_validation_count = await FinalData.count({
      group: groupID,
      validation: true,
      data_type: 'document-sentiment'
    });

    let document_sentiment_data = document_sentiment_final_data.concat(document_sentiment_linguistic_data);
    document_sentiment_data = document_sentiment_data.filter( document => {
      if(document.linguistics.length){
        document['linguistic_answer'] =  MajorityMatchController.paragraphAndDocumentSentimentMajorityMatch(document.linguistics);
      }
      
      return document
    });

    return {
      status: "success",
      group: group,
      data: document_sentiment_data,
      validation_complete: document_sentiment_validation_count,
      number_of_data: document_sentiment_data.length,
      kappa_socres: kappa_socres
    }
  }

  /**
   * 
   * @param {dataType, filterType} 
   * 
   */

  static async sentenceEmotionList(groupID, filterType) {
    const group = await Group.findById(groupID).populate("users", "name email status");
    let dataQuery = { group: groupID, data_type: 'sentence-emotion' };

    if (filterType == 'validation-complete') {
      dataQuery['validation'] = true;
    } else if (filterType == 'validation-incomplete') {
      dataQuery['validation'] = false;
    }

    let sentence_emotion_final_data = await FinalData.find(dataQuery).lean();
    let sentence_emotion_linguistic_data = await LinguisticData.find(dataQuery).lean();
    const kappa_socres = await KappaScore.find(dataQuery);

    const sentence_emotion_validation_count = await FinalData.count({
      group: groupID,
      validation: true,
      data_type: 'sentence-emotion'
    });

    let sentence_emotion_data = sentence_emotion_final_data.concat(sentence_emotion_linguistic_data);
    sentence_emotion_data = sentence_emotion_data.filter( sentence => {
      if(sentence.linguistics.length){
        sentence['linguistic_answer'] =  MajorityMatchController.sentenceEmotionMajorityMatch(sentence.linguistics);
      }
      
      return sentence
    });

    return {
      status: "success",
      group: group,
      data: sentence_emotion_data,
      validation_complete: sentence_emotion_validation_count,
      number_of_data: sentence_emotion_data.length,
      kappa_socres: kappa_socres
    }
  }

  /**
   * 
   * @param {dataType, filterType} 
   * 
   */

  static async paragraphEmotionList(groupID, filterType) {
    const group = await Group.findById(groupID).populate("users", "name email status");
    let dataQuery = { group: groupID, data_type: 'paragraph-emotion' };

    if (filterType == 'validation-complete') {
      dataQuery['validation'] = true;
    } else if (filterType == 'validation-incomplete') {
      dataQuery['validation'] = false;
    }

    let paragraph_emotion_final_data = await FinalData.find(dataQuery).lean();
    let paragraph_emotion_linguistic_data = await LinguisticData.find(dataQuery).lean();
    const kappa_socres = await KappaScore.find(dataQuery);

    const paragraph_emotion_validation_count = await FinalData.count({
      group: groupID,
      validation: true,
      data_type: 'paragraph-emotion'
    });

    let paragraph_emotion_data = paragraph_emotion_final_data.concat(paragraph_emotion_linguistic_data);
    paragraph_emotion_data = paragraph_emotion_data.filter( paragraph => {
      if(paragraph.linguistics.length){
        paragraph['linguistic_answer'] =  MajorityMatchController.paragraphEmotionMajorityMatch(paragraph.linguistics);
      }
      
      return paragraph
    });

    return {
      status: "success",
      group: group,
      data: paragraph_emotion_data,
      validation_complete: paragraph_emotion_validation_count,
      number_of_data: paragraph_emotion_data.length,
      kappa_socres: kappa_socres
    }
  }

}
