import User from "../models/User.js";

import Group from "../models/Group.js";
import config from "../config/index.js";
import helper from "../helpers/index.js";
import template from "../email_template/index.js";
import jwt from "jsonwebtoken";
import request from "request";

class AuthController {
    /**
     * Validation for user info.
     *
     * @param req
     */
    static userValidation(req) {
        req.checkBody({
            name: {
                notEmpty: {
                    errorMessage: "User name is required",
                },
            },
            email: {
                notEmpty: {
                    errorMessage: "Enter a email",
                },
                isEmail: {
                    errorMessage: "Enter a valid email",
                },
            },
            phone: {
                notEmpty: {
                    errorMessage: "Enter a phone number",
                },
            },
            age: {
                notEmpty: {
                    errorMessage: "Enter user age",
                },
            },
            gender: {
                notEmpty: {
                    errorMessage: "Select user gender",
                },
            },
            profession: {
                notEmpty: {
                    errorMessage: "Enter user profession",
                },
            },
            type: {
                notEmpty: {
                    errorMessage: "Select user type",
                },
            },
            password: {
                notEmpty: {
                    errorMessage: "Please type your password",
                },
            },
        });

        req.checkBody("email", "Email already exists").emailExists(User);
        req.checkBody("phone", "Phone already exists").phoneExists(User);

        /*if (req.hasOwnProperty("file") && req.file !== "") {
                req.checkBody("avatar", "Please select an image Jpeg, Png or Gif").isImage(req.file);
                req.checkBody("avatar", "File size cannot be more than 5 MB").fileSize(req.file, 5000000);
            }*/

        req.sanitize("name").escape();
        req.sanitize("name").trim();
        req.sanitize("email").escape();
        req.sanitize("email").trim();
        req.sanitize("phone").escape();
        req.sanitize("phone").trim();
        req.sanitize("age").escape();
        req.sanitize("age").trim();
        req.sanitize("gender").escape();
        req.sanitize("gender").escape();
        req.sanitize("profession").trim();
        req.sanitize("profession").trim();
        req.sanitize("type").trim();
        req.sanitize("type").trim();
    }

    /**
     * Log a user in.
     *
     * @param req
     * @param res
     */
    static login(req, res) {
        // Validate inputs
        req.checkBody({
            email: {
                notEmpty: {
                    errorMessage: "Please type your registered email",
                },
                isEmail: {
                    errorMessage: "Please type your valid email",
                },
            },
            password: {
                notEmpty: {
                    errorMessage: "Please type your password",
                },
            },
            recaptcha: {
                notEmpty: {
                    errorMessage: "Captcha is not valid",
                },
            },
        });

        req.sanitize("email").escape();

        req.getValidationResult().then((errors) => {
            if (!errors.isEmpty()) {
                res
                    .status(422)
                    .json({ status: "validationError", errors: errors.array() });
            } else {
                // Find the user

                AuthController.verifyRecaptcha(req, (isVerified) => {
                    if (isVerified) {
                        User.findOne({ email: req.body.email }).exec((err, user) => {
                            if (err) {
                                return res.status(500).json({ status: "error", message: err });
                            }

                            if (!user) {
                                return res.status(404).json({
                                    status: "error",
                                    message: "The requested user could not be found",
                                });
                            }

                            if (user.status == "unverified" || user.status == "deleted") {
                                return res.status(404).json({
                                    status: "error",
                                    message: "Either your account unverified or deleted. Please contact with admin.",
                                });
                            }

                            user.comparePassowrd(req.body.password, (err, matched) => {
                                if (matched && matched === true) {
                                    let data = {
                                        _id: user._id,
                                        name: user.name,
                                        email: user.email,
                                        phone: user.phone,
                                        age: user.age,
                                        gender: user.gender,
                                        profession: user.profession,
                                        type: user.type,
                                    };
                                    let token = jwt.sign(data, config.jwtSecret, {
                                        expiresIn: "30 day",
                                    });

                                    return res
                                        .status(200)
                                        .json({ status: "success", token: token });
                                } else {
                                    return res
                                        .status(401)
                                        .json({
                                            status: "error",
                                            message: "Your password is invalid",
                                        });
                                }
                            });
                        });
                    } else {
                        return res
                            .status(401)
                            .json({ status: "error", message: "Invalid request" });
                    }
                });
            }
        });
    }

    /***
     * Google Recaptcha Validate
     *
     */
    static verifyRecaptcha(req, callback) {
        let serverSecretKey = "";
        let token = req.body.recaptcha;

        if (req.body.platform == "android") {
            serverSecretKey = "6LfcEb4ZAAAAACyP9sm5qAdOLScsJSRCogAq6etL"; //the secret key from your google admin console;
            // serverSecretKey = "6LcCMr8ZAAAAAGj2kOStvQLiVWSM3HHpKkT2r2ZN"; //the secret key from your google admin console;
        } else {
            serverSecretKey = "6LfqurEZAAAAALsnUiWQv2ocSmh38T3DdVGwRZAf"; //the secret key from your google admin console;
            // serverSecretKey = "6LcCMr8ZAAAAAGj2kOStvQLiVWSM3HHpKkT2r2ZN"; //the secret key from your google admin console;
        }

        const url = `https://www.google.com/recaptcha/api/siteverify?secret=${serverSecretKey}&response=${token}&remoteip=${req.connection.remoteAddress}`;

        //note that remoteip is the users ip address and it is optional
        // in node req.connection.remoteAddress gives the users ip address

        if (token === null || token === undefined) {
            return callback(false);
        }

        request(url, function(err, response, body) {
            //the body is the data that contains success message
            body = JSON.parse(body);

            //check if the validation failed
            if (!body.success) {
                return callback(false);
            }

            return callback(true);
        });
    }

    /**
     * User Logout.
     *
     * @param req
     * @param res
     * @returns {Response | *}
     */
    static logout(req, res) {
        return res
            .status(200)
            .json({ status: "success", message: "You have been logged out" });
    }

    /**
     * Get the specified user's profile.
     *
     * @param req
     * @param res
     * @returns {Response | *}
     */
    static profile(req, res) {
        User.findById(req.decodedToken._id, (err, result) => {
            if (err) {
                return res.status(404).json({
                    status: "error",
                    message: "The requested user could not be found",
                });
            }

            return res.status(200).json({ status: "success", user: result });
        });
    }

    /**
     * Get the specified Annotator Group.
     *
     * @param req
     * @param res
     * @returns {Response | *}
     */
    static group(req, res) {
        Group.findOne({ users: { $in: req.decodedToken._id } })
            .populate("users", "name email")
            .exec((err, group) => {
                if (err) {
                    res.status(500).json({ status: "error", message: err });
                } else {
                    return res.status(200).json({ status: "success", group: group });
                }
            });
    }

    /**
     * Update the specified user's profile
     *
     * @requires "userID" header
     *
     * @param req
     * @param res
     */
    static update(req, res) {
        User.findById(req.decodedToken._id, (err, user) => {
            if (err) {
                return res.status(404).json({
                    status: "error",
                    message: "The requested user could not be found",
                });
            }

            user.name = req.body.name;
            user.phone = req.body.phone;
            user.age = req.body.age;
            user.gender = req.body.gender;
            user.profession = req.body.profession;

            if (req.body.password && req.body.password.length == 6) {
                user.password = req.body.password;
            }

            user.save((err) => {
                if (err) {
                    res.status(500).json({
                        status: "error",
                        message: err.message,
                    });
                } else {
                    res
                        .status(200)
                        .json({
                            status: "success",
                            message: "Profile updated successfully",
                        });
                }
            });
        });
    }

    /**
     * Forget Password
     *
     * @requires "email"
     *
     * @param req
     * @param res
     */
    static forgetPassword(req, res) {
        User.findOne({ email: req.body.email }, (err, user) => {
            if (err || !user) {
                return res.status(404).json({
                    status: "error",
                    message: "The requested user could not be found",
                });
            }

            let token = (
                Math.random().toString(36) + new Date().valueOf().toString()
            ).substr(2, 35);

            user.reset_token = token;

            user.save((err, user) => {
                if (err) {
                    res.status(500).json({ status: "error", message: err });
                } else {
                    helper.mail(
                        req.body.email,
                        "Password Reset Link",
                        template.password_reset(token)
                    );

                    res.status(200).json({
                        status: "success",
                        message: "Reset link sent",
                    });
                }
            });
        });
    }

    /**
     * Check Reset Password Token
     *
     * @requires "email"
     *
     * @param req
     * @param res
     */
    static checkRestPasswordLink(req, res) {
        User.findOne({ reset_token: req.params.token }, (err, user) => {
            if (err || !user) {
                return res.status(404).json({
                    status: "error",
                    message: "Invalid Request",
                });
            }

            res.status(200).json({
                status: "success",
                message: "Token is valid",
            });
        });
    }

    /**
     * Reset Password
     *
     * @requires "email"
     *
     * @param req
     * @param res
     */
    static restPassword(req, res) {
        if (req.body.reset_token == null || req.body.reset_token == "") {
            return res.status(404).json({
                status: "error",
                message: "Invalid Request",
            });
        } else {
            User.findOne({ reset_token: req.body.reset_token }, (err, user) => {
                if (err || !user) {
                    return res.status(404).json({
                        status: "error",
                        message: "Invalid Request",
                    });
                }

                user.password = req.body.password;
                user.reset_token = null;

                user.save((err, user) => {
                    if (err) {
                        res.status(500).json({ status: "error", message: err });
                    } else {
                        res.status(200).json({
                            status: "success",
                            message: "Reset password successfull. You may now login to the site.",
                        });
                    }
                });
            });
        }
    }
}

export default AuthController;