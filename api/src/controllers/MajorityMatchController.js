export default class MajorityMatchController {

    /**
     * Sentence Sentiment Majority Match
     * @param {*} data 
     */
    static sentenceSentimentMajorityMatch(data) {
        let sentiment_check = [];
        let answer = {};
        let skip = false;

        for (let item of data) {
            if (item.answer['skip']) {
                skip = true;
            } else if (item.answer['subjectivity'] === 'objective') {
                sentiment_check.push('objective');
            } else {
                sentiment_check.push(item.answer['polarity']);
            }
        }

        if (skip) {
            return answer;
        }

        let maj = MajorityMatchController.majorityCheck(sentiment_check);

        if (maj.length) {
            if (maj === 'objective') {
                answer['subjectivity'] = 'objective';
            } else {
                answer['subjectivity'] = 'subjective';
                answer['polarity'] = maj;
            }
        }

        return answer;
    }

    /**
     * Sentence Emotion Majority Match
     * @param {*} data 
     */

    static sentenceEmotionMajorityMatch(data) {
        let emotion_check = [];
        let emotion_all = [];
        let answer = {};
        let skip = false;

        let emotion_counter = 0;
        for (let item of data) {
            if (item.answer['skip']) {
                skip = true;
            } else {
                emotion_check.push(item.answer['emotions']);
                for (let i = 0; i < item.answer['emotions'].length; i++) {
                    emotion_all.push(item.answer['emotions'][i]);
                }
                emotion_counter++;
            }
        };

        if (skip) {
            answer['emotions'] = [];
            return answer;
        }

        let emotion_match = MajorityMatchController.emotion_majority(emotion_all, emotion_counter);

        answer['emotions'] = emotion_match;

        return answer;
    }

    /**
     * Paragraph Emotion Majority Match
     * @param {*} data 
     */

    static paragraphEmotionMajorityMatch(data) {
        let answer = {};
        let emotion_check = [];
        let emotion_all = [];
        let skip = false;
        let emotion_counter = 0;
        let emotion_topic_names = [], emotion_topic_names_copy = [];
        let emotion_topic_emotions = [];
        let emotionTopicObj = [];

        for (let item of data) {
            if (item.answer['skip']) {
                skip = true;
            } else {
                emotion_check.push(item.answer['sentiment_overall']);
                for (let i = 0; i < item.answer['sentiment_overall'].length; i++) {
                    emotion_all.push(item.answer['sentiment_overall'][i]);
                }
                item.answer['topics'].forEach((item) => {
                    emotionTopicObj.push(item);
                });
                emotion_counter++;
            }
        }

        if (skip) {
            answer['topics'] = [];
            answer['sentiment_overall'] = [];
            return answer;
        }

        emotionTopicObj.forEach((item, index) => {
            emotion_topic_names_copy.push(item['name']);
            emotion_topic_names.push(item['name']);
            emotion_topic_emotions.push(item['emotions']);
        });

        let emotion_topic_name_match = MajorityMatchController.emotion_majority(emotion_topic_names, emotion_counter);

        let emotion_topics_emotions_to_work_with = [];
        let emotion_topics_to_work_with = [];
        for (let i = 0; i < emotion_topic_name_match.length; i++) {
            let dummy_array = [];
            for (let j = 0; j < emotion_topic_names_copy.length; j++) {
                if (emotion_topic_name_match[i] === emotion_topic_names_copy[j]) {
                    for (let k = 0; k < emotion_topic_emotions[j].length; k++) {
                        dummy_array.push(emotion_topic_emotions[j][k]);
                    }
                }
            }
            emotion_topics_to_work_with.push(dummy_array);
        }

        let majority_name_wise = [];
        for (let i = 0; i < emotion_topic_name_match.length; i++) {
            let emotion_obj = {};
            let temp_emo_maj = MajorityMatchController.emotion_majority(emotion_topics_to_work_with[i], emotion_counter);

            if (temp_emo_maj[0].length > 2) {
                emotion_obj['name'] = emotion_topic_name_match[i];
                emotion_obj['emotions'] = temp_emo_maj;
            }
            majority_name_wise.push(emotion_obj);
        }

        let emotion_match = MajorityMatchController.emotion_majority(emotion_all, emotion_counter);

        answer['topics'] = majority_name_wise;
        answer['sentiment_overall'] = emotion_match;

        return answer;
    }

    /**
     * Emotion Majority Vote
     * @param {*} array 
     */

    static emotion_majority(arr, emotion_counter) {
        var a = [], b = [], prev, final_array = [];
        var newarr = arr;
        arr.sort();
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] !== prev) {
                a.push(arr[i]);
                b.push(1);
            } else {
                b[b.length - 1]++;
            }
            prev = arr[i];
        }
        for (i = 0; i < b.length; i++) {
            if (b[i] >= (emotion_counter - 1)) {
                final_array.push(a[i]);
            }
        }
        return final_array;
    }

    /**
     * Paragraph and Document Sentiment Majority Match
     * @param {*} data 
     */

    static paragraphAndDocumentSentimentMajorityMatch(data) {
        let answer = {};
        let sentiment_over_check = [];
        let topics_ok = [];
        let all_keys = [];
        let unique_keys = [];
        let sentimentTopicObj = [];
        let skip = false;

        for (let item of data) {
            if (item.answer['skip']) {
                skip = true;
            } else {
                sentiment_over_check.push(item.answer['sentiment_overall']);
                item.answer['topics'].forEach((item) => {
                    sentimentTopicObj.push(item);
                });
            }
        }

        if (skip) {
            answer['topics'] = [];
            answer['sentiment_overall'] = '';
            return answer;
        }

        let maj_sent_over = MajorityMatchController.majorityCheck(sentiment_over_check);

        sentimentTopicObj.forEach((item, index) => {
            let jsonObj = {};

            jsonObj[item['name']] = item['polarity'];
            topics_ok.push(jsonObj);
            all_keys.push(Object.keys(jsonObj)[0]);
        });

        unique_keys = Array.from(new Set(all_keys));
        let obj_new = {};
        const array_data = [];

        for (let i = 0; i < unique_keys.length; i++) {
            let newarray = [];
            let key_counter = 0;
            let polarity_val = [];
            for (let j = 0; j < topics_ok.length; j++) {
                if (typeof topics_ok[j][unique_keys[i]] !== 'undefined') {
                    key_counter++;
                    polarity_val.push(topics_ok[j][unique_keys[i]]);
                }
            }
            if (key_counter > 1) {
                if (polarity_val.length > 1) {
                    let maj = MajorityMatchController.majorityCheck(polarity_val);

                    if (maj === 'objective' || maj === 'WP' || maj === 'SP'
                        || maj === 'WN' || maj === 'SN' || maj === 'NU') {
                        obj_new['name'] = unique_keys[i];
                        obj_new['polarity'] = polarity_val[1];
                        array_data.push(JSON.stringify(obj_new));
                    }
                }
            }
        }

        answer['topics'] = array_data;
        answer['sentiment_overall'] = maj_sent_over;

        return answer;
    }

    /**
     * Sentiment Majority Vote
     * @param {*} array 
     */

    static majorityCheck(array) {
        if (array.length == 0)
            return '';
        var modeMap = {};
        var maxEl = '', maxCount = 1;
        for (var i = 0; i < array.length; i++) {
            var el = array[i];
            if (modeMap[el] == null)
                modeMap[el] = 1;
            else
                modeMap[el]++;
            if (modeMap[el] > maxCount) {
                maxEl = el;
                maxCount = modeMap[el];
            }
        }
        return maxEl;
    }
}