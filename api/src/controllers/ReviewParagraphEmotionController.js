import LinguisticData from "../models/LinguisticData.js";

export default class ReviewParagraphEmotionController {
  /**
   * ParagraphEmotion Submitted List
   *
   * @param req
   * @param res
   */
  static list(req, res) {
    LinguisticData.find(
      {
        // linguistics: {
        //   $elemMatch: {
        //     user_id: req.decodedToken._id,
        //     answer: { $exists: true, $not: { $size: 0 } },
        //   },
        // },
        data_type: "paragraph-emotion",
      },
      (err, results) => {
        if (err) {
          res.status(500).json({ status: "error", message: err });
        } else {
          res.status(200).json({
            status: "success",
            submitted_list: results,
          });
        }
      }
    );
  }

  /**
   * Get a ParagraphEmotion by Group ID
   *
   * @param req
   * @param res
   *
   */
  static retrive(req, res) {
    LinguisticData.findOne(
      {
        // linguistics: {
        //   $elemMatch: { user_id: req.decodedToken._id, answer: undefined },
        // },
        data_type: "paragraph-emotion",
      },
      (err, paragraph_emotion) => {
        if (err) {
          res.status(500).json({ status: "error", message: err });
        } else {
          res.status(200).json({ status: "success", paragraph_emotion: paragraph_emotion });
        }
      }
    ).select("-linguistics -created_at -updated_at");
  }

  /**
   * Get a ParagraphEmotion by ID
   *
   * @param req
   * @param res
   *
   */
  static getParagraphByID(req, res) {
    LinguisticData.findById({ _id: req.params.id }, (err, document) => {
      if (err) {
        res.status(500).json({ status: "error", message: err });
      } else {
        if (document.linguistics.length > 0) {
          let is_user_exist = false;
          document.linguistics.forEach((user) => {
            if (user.user_id == req.decodedToken._id) {
              is_user_exist = true;
            }
          });

          if (is_user_exist) {
            res.status(200).json({ status: "success", document: document });
          } else {
            res.status(200).json({
              status: "failed",
              message: "Sorry you can't edit this document sentiment",
            });
          }
        } else {
          res.status(200).json({
            status: "failed",
            message: "Sorry you can't edit this document sentiment",
          });
        }
      }
    }).select("-created_at -updated_at");
  }

  /**
   * Update ParagraphEmotion
   *
   * @param req
   * @param res
   */
  static update(req, res) {
    LinguisticData.findById({ _id: req.body.id }, (err, paragraph) => {
      if (err) {
        res.status(500).json({ status: "error", message: err });
      } else {
        paragraph.linguistics.forEach((linguistic) => {
          if (linguistic.user_id == req.decodedToken._id) {
            linguistic.answer = req.body.answer;
          }
        });

        paragraph.save((err) => {
          if (err) {
            res.status(500).json({
              status: "error",
              message: "Whoops! Something went wrong",
            });
          } else {
            if (req.body.update) {
              res.status(200).json({
                status: "success",
                message: "Paragraph Sentiment Updated Success",
              });
            } else {
              ReviewParagraphEmotionController.retrive(req, res);
            }
          }
        });
      }
    });
  }
}
