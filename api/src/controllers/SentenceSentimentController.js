import SentenceSentiment from "../models/SentenceSentiment.js";

export default class SentenceSentimentController {

    /**
     * Sentence Sentiment Submitted List
     *
     * @param req
     * @param res
     */
    static list(req, res) {
        SentenceSentiment.find({"annotators": { $elemMatch: {"user_id": req.decodedToken._id, "answer": {$exists: true, $not: {$size: 0}} }}}, (err, results) => {
            if (err) {
                res.status(500).json({"status": "error", "message": err});
            } else {
                res.status(200).json({
                    "status": "success",
                    "submitted_list": results
                })
            }
        });
    }

    /**
     * Get a Sentence by Group ID
     *
     * @param req
     * @param res
     *
     */
    static retrive(req, res) {
        SentenceSentiment.findOne({"annotators": { $elemMatch: {"user_id": req.decodedToken._id, "answer": undefined }}}, (err, sentence) => {
            if (err) {
                res.status(500).json({"status": "error", "message": err});
            } else {
                res.status(200).json({"status": "success", "sentence": sentence});
            }
        }).select("-annotators -created_at -updated_at");
    }

    /**
     * Get a Sentence by ID
     *
     * @param req
     * @param res
     *
     */
    static getSentenceByID(req, res) {
        SentenceSentiment.findById({"_id": req.params.id}, (err, sentence) => {
            if (err) {
                res.status(500).json({"status": "error", "message": err});
            } else {
                if(sentence.annotators.length > 0) {
                    let is_user_exist = false;
                    sentence.annotators.forEach( user => {
                        if(user.user_id == req.decodedToken._id) {
                            is_user_exist = true;
                        }
                    });

                    if(is_user_exist) {
                        res.status(200).json({"status": "success", "sentence": sentence});
                    } else {
                        res.status(200).json({"status": "failed", "message": "Sorry you can't edit this sentence sentiment"});
                    }
                }else{
                    res.status(200).json({"status": "failed", "message": "Sorry you can't edit this sentence sentiment"});
                }
            }
        }).select("-created_at -updated_at");
    }

    /**
     * Update Sentence Sentiment
     *
     * @param req
     * @param res
     */
    static update(req, res) {
        SentenceSentiment.findById({"_id": req.body.id}, (err, sentence) => {
            if (err) {
                res.status(500).json({"status": "error", "message": err});
            } else {
                sentence.annotators.forEach(annotator => {
                    if(annotator.user_id == req.decodedToken._id) {
                        if(req.body.skip) {
                            annotator.answer = {
                                "skip": true
                            }
                        } else if (req.body.subjectivity == 'objective') {
                            annotator.answer = {
                                "subjectivity": req.body.subjectivity
                            };
                        } else {
                            annotator.answer = {
                                "subjectivity": req.body.subjectivity,
                                "polarity": req.body.polarity
                            };
                        }
                    }
                });

                sentence.save(err => {
                    if (err) {
                        res.status(500).json({"status": "error", "message": "Whoops! Something went wrong"});
                    } else {
                        if(req.body.update) {
                            res.status(200).json({"status": "success", "message": "Sentence Sentiment Updated Success"});
                        } else {
                            SentenceSentimentController.retrive(req, res);
                        }
                    }
                });

            }
        });
    }
}