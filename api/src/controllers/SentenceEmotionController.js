import SentenceEmotion from "../models/SentenceEmotion.js";

export default class SentenceEmotionController {

    /**
     * SentenceEmotion Submitted List
     *
     * @param req
     * @param res
     */
    static list(req, res) {
        SentenceEmotion.find({ "annotators": { $elemMatch: { "user_id": req.decodedToken._id, "answer": { $exists: true, $not: { $size: 0 } } } } }, (err, results) => {
            if (err) {
                res.status(500).json({ "status": "error", "message": err });
            } else {
                res.status(200).json({
                    "status": "success",
                    "submitted_list": results
                })
            }
        });
    }

    /**
     * Get a SentenceEmotion by Group ID
     *
     * @param req
     * @param res
     *
     */
    static retrive(req, res) {
        SentenceEmotion.findOne({ "annotators": { $elemMatch: { "user_id": req.decodedToken._id, "answer": undefined } } }, (err, sentenceEmotion) => {
            if (err) {
                res.status(500).json({ "status": "error", "message": err });
            } else {
                res.status(200).json({ "status": "success", "sentence_emotion": sentenceEmotion });

            }
        }).select("-annotators -created_at -updated_at");
    }

    /**
     * Get a SentenceEmotion by ID
     *
     * @param req
     * @param res
     *
     */
    static getSentenceByID(req, res) {
        SentenceEmotion.findById({ "_id": req.params.id }, (err, sentenceEmotion) => {
            if (err) {
                res.status(500).json({ "status": "error", "message": err });
            } else {
                if (sentenceEmotion.annotators.length > 0) {
                    let is_user_exist = false;
                    sentenceEmotion.annotators.forEach(user => {
                        if (user.user_id == req.decodedToken._id) {
                            is_user_exist = true;
                        }
                    });

                    if (is_user_exist) {
                        res.status(200).json({ "status": "success", "sentence_emotion": sentenceEmotion });
                    } else {
                        res.status(200).json({ "status": "failed", "message": "Sorry you can't edit this sentence emotion" });
                    }
                } else {
                    res.status(200).json({ "status": "failed", "message": "Sorry you can't edit this sentence emotion" });
                }
            }
        }).select("-created_at -updated_at");
    }

    /**
     * Update SentenceEmotion
     *
     * @param req
     * @param res
     */
    static update(req, res) {
        SentenceEmotion.findById({ "_id": req.body.id }, (err, sentenceEmotion) => {
            if (err) {
                res.status(500).json({ "status": "error", "message": err });
            } else {
                sentenceEmotion.annotators.forEach(annotator => {
                    if (annotator.user_id == req.decodedToken._id) {
                        annotator.answer = req.body.answer
                    }
                });

                sentenceEmotion.save(err => {
                    if (err) {
                        res.status(500).json({ "status": "error", "message": "Whoops! Something went wrong" });
                    } else {
                        if (req.body.update) {
                            res.status(200).json({ "status": "success", "message": "Sentence Emotion Updated Success" });
                        } else {
                            SentenceEmotionController.retrive(req, res);
                        }
                    }
                });

            }
        });
    }
}