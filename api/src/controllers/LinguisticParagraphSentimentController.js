import ParagraphSentiment from "../models/ParagraphSentiment.js";

export default class LinguisticParagraphSentimentController {
    /**
     * Paragraph Sentiment Submitted List
     *
     * @param req
     * @param res
     */
    static list(req, res) {
        ParagraphSentiment.find({
                linguistics: {
                    $elemMatch: {
                        user_id: req.decodedToken._id,
                        answer: { $exists: true, $not: { $size: 0 } },
                    },
                },
            },
            null, { sort: { created_at: -1 } },
            (err, results) => {
                if (err) {
                    res.status(500).json({ status: "error", message: err });
                } else {
                    res.status(200).json({
                        status: "success",
                        submitted_list: results,
                    });
                }
            }
        );
    }

    /**
     * Get a Paragraph by Group ID
     *
     * @param req
     * @param res
     *
     */
    static retrive(req, res) {
        ParagraphSentiment.findOne({
                linguistics: {
                    $elemMatch: { user_id: req.decodedToken._id, answer: undefined },
                },
            },
            (err, paragraph) => {
                if (err) {
                    res.status(500).json({ status: "error", message: err });
                } else {
                    res.status(200).json({ status: "success", paragraph: paragraph });
                }
            }
        ).select("-linguistics -created_at -updated_at");
    }

    /**
     * Get a Paragraph by ID
     *
     * @param req
     * @param res
     *
     */
    static getParagraphByID(req, res) {
        ParagraphSentiment.findById({ _id: req.params.id }, (err, paragraph) => {
            if (err) {
                res.status(500).json({ status: "error", message: err });
            } else {
                if (paragraph.linguistics.length > 0) {
                    let is_user_exist = false;
                    paragraph.linguistics.forEach((user) => {
                        if (user.user_id == req.decodedToken._id) {
                            is_user_exist = true;
                        }
                    });

                    if (is_user_exist) {
                        res.status(200).json({ status: "success", paragraph: paragraph });
                    } else {
                        res.status(200).json({
                            status: "failed",
                            message: "Sorry you can't edit this paragraph sentiment",
                        });
                    }
                } else {
                    res.status(200).json({
                        status: "failed",
                        message: "Sorry you can't edit this paragraph sentiment",
                    });
                }
            }
        }).select("-created_at -updated_at");
    }

    /**
     * Update Paragraph Sentiment
     *
     * @param req
     * @param res
     */
    static update(req, res) {
        ParagraphSentiment.findById({ _id: req.body.id }, (err, paragraph) => {
            if (err) {
                res.status(500).json({ status: "error", message: err });
            } else {
                paragraph.linguistics.forEach((linguistic) => {
                    if (linguistic.user_id == req.decodedToken._id) {
                        linguistic.answer = req.body.answer;
                    }
                });

                paragraph.save((err) => {
                    if (err) {
                        res.status(500).json({
                            status: "error",
                            message: "Whoops! Something went wrong",
                        });
                    } else {
                        if (req.body.update) {
                            res.status(200).json({
                                status: "success",
                                message: "Paragraph Sentiment Updated Success",
                            });
                        } else {
                            LinguisticParagraphSentimentController.retrive(req, res);
                        }
                    }
                });
            }
        });
    }
}