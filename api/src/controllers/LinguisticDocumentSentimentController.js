import DocumentSentiment from "../models/DocumentSentiment.js";

export default class LinguisticDocumentSentimentController {
    /**
     * Document Sentiment Submitted List
     *
     * @param req
     * @param res
     */
    static list(req, res) {
        DocumentSentiment.find({
                linguistics: {
                    $elemMatch: {
                        user_id: req.decodedToken._id,
                        answer: { $exists: true, $not: { $size: 0 } },
                    },
                },
            },
            null, { sort: { created_at: -1 } },
            (err, results) => {
                if (err) {
                    res.status(500).json({ status: "error", message: err });
                } else {
                    res.status(200).json({
                        status: "success",
                        submitted_list: results,
                    });
                }
            }
        );
    }

    /**
     * Get a Document by Group ID
     *
     * @param req
     * @param res
     *
     */
    static retrive(req, res) {
        DocumentSentiment.findOne({
                linguistics: {
                    $elemMatch: { user_id: req.decodedToken._id, answer: undefined },
                },
            },
            (err, document) => {
                if (err) {
                    res.status(500).json({ status: "error", message: err });
                } else {
                    res.status(200).json({ status: "success", document: document });
                }
            }
        ).select("-linguistics -created_at -updated_at");
    }

    /**
     * Get a Document by ID
     *
     * @param req
     * @param res
     *
     */
    static getDocumentByID(req, res) {
        DocumentSentiment.findById({ _id: req.params.id }, (err, document) => {
            if (err) {
                res.status(500).json({ status: "error", message: err });
            } else {
                if (document.linguistics.length > 0) {
                    let is_user_exist = false;
                    document.linguistics.forEach((user) => {
                        if (user.user_id == req.decodedToken._id) {
                            is_user_exist = true;
                        }
                    });

                    if (is_user_exist) {
                        res.status(200).json({ status: "success", document: document });
                    } else {
                        res.status(200).json({
                            status: "failed",
                            message: "Sorry you can't edit this document sentiment",
                        });
                    }
                } else {
                    res.status(200).json({
                        status: "failed",
                        message: "Sorry you can't edit this document sentiment",
                    });
                }
            }
        }).select("-created_at -updated_at");
    }

    /**
     * Update Document Sentiment
     *
     * @param req
     * @param res
     */
    static update(req, res) {
        DocumentSentiment.findById({ _id: req.body.id }, (err, document) => {
            if (err) {
                res.status(500).json({ status: "error", message: err });
            } else {
                document.linguistics.forEach((linguistic) => {
                    if (linguistic.user_id == req.decodedToken._id) {
                        linguistic.answer = req.body.answer;
                    }
                });

                document.save((err) => {
                    if (err) {
                        res.status(500).json({
                            status: "error",
                            message: "Whoops! Something went wrong",
                        });
                    } else {
                        if (req.body.update) {
                            res.status(200).json({
                                status: "success",
                                message: "Document Sentiment Updated Success",
                            });
                        } else {
                            LinguisticDocumentSentimentController.retrive(req, res);
                        }
                    }
                });
            }
        });
    }
}