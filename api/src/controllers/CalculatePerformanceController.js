import SentenceSentiment from "../models/SentenceSentiment.js";
import ParagraphSentiment from "../models/ParagraphSentiment.js";
import DocumentSentiment from "../models/DocumentSentiment.js";
import SentenceEmotion from "../models/SentenceEmotion.js";
import ParagraphEmotion from "../models/ParagraphEmotion.js";
import MajorityMatchController from "./MajorityMatchController.js";

export default class CalculatePerformanceController {

  /**
   * Annotator performance
   * 
   * @param {*} req 
   * @param {*} res 
   */
  static annotatorPerformance(userID) {
    return Promise.all([
      //overall accuracy
      SentenceSentiment.find().and([
        {
          "linguistics": {
            $elemMatch: {
              answer: { $exists: true },
            }
          }
        },
        {
          annotators: {
            $elemMatch: {
              user_id: userID,
              answer: { $exists: true }
            }
          }
        }
      ]),
      ParagraphSentiment.find().and([
        {
          "linguistics": {
            $elemMatch: {
              answer: { $exists: true },
            }
          }
        },
        {
          annotators: {
            $elemMatch: {
              user_id: userID,
              answer: { $exists: true }
            }
          }
        }
      ]),
      DocumentSentiment.find().and([
        {
          "linguistics": {
            $elemMatch: {
              answer: { $exists: true },
            }
          }
        },
        {
          annotators: {
            $elemMatch: {
              user_id: userID,
              answer: { $exists: true }
            }
          }
        }
      ]),
      SentenceEmotion.find().and([
        {
          "linguistics": {
            $elemMatch: {
              answer: { $exists: true },
            }
          }
        },
        {
          annotators: {
            $elemMatch: {
              user_id: userID,
              answer: { $exists: true }
            }
          }
        }
      ]),
      ParagraphEmotion.find().and([
        {
          "linguistics": {
            $elemMatch: {
              answer: { $exists: true },
            }
          }
        },
        {
          annotators: {
            $elemMatch: {
              user_id: userID,
              answer: { $exists: true }
            }
          }
        }
      ]),

    ]).then(([
      sentence_sentiment_data,
      paragraph_sentiment_data,
      document_sentiment_data,
      sentence_emotion_data,
      paragraph_emotion_data
    ]) => {

      let sentence_sentiment_accuracy = CalculatePerformanceController.senteneSentimentPerformance(
        sentence_sentiment_data, userID
      )

      let paragraph_sentiment_accuracy = CalculatePerformanceController.paragraphSentimentPerformance(
        paragraph_sentiment_data, userID
      )

      let document_sentiment_accuracy = CalculatePerformanceController.documentSentimentPerformance(
        document_sentiment_data, userID
      )

      let sentence_emotion_accuracy = CalculatePerformanceController.sentenceEmotionPerformance(
        sentence_emotion_data, userID
      )

      let paragraph_emotion_accuracy = CalculatePerformanceController.paragraphEmotionPerformance(
        paragraph_emotion_data, userID
      )

      return parseInt(
        (
          sentence_sentiment_accuracy + paragraph_sentiment_accuracy
          + document_sentiment_accuracy + sentence_emotion_accuracy + paragraph_emotion_accuracy
        ) / 5
      )

    }).catch((err) => {
      throw err; // res.sendStatus(500) might be better here.
    });
  }

  /**
  * Group Annotator performance
  * 
  * @param {*} req 
  * @param {*} res 
  */
  static groupAnnotatorPerformance(userID, groupID, dataType) {
    if (dataType == "sentence-sentiment") {
      return Promise.all([
        //overall accuracy
        SentenceSentiment.find().and([
          {
            "group": groupID,
          },
          {
            "linguistics": {
              $elemMatch: {
                answer: { $exists: true },
              }
            }
          },
          {
            annotators: {
              $elemMatch: {
                user_id: userID,
                answer: { $exists: true }
              }
            }
          }
        ]),
      ]).then(([
        sentence_sentiment_data,
      ]) => {

        let sentence_sentiment_accuracy = CalculatePerformanceController.senteneSentimentPerformance(
          sentence_sentiment_data, userID
        )

        return parseInt(sentence_sentiment_accuracy)

      }).catch((err) => {
        throw err; // res.sendStatus(500) might be better here.
      });
    } else if (dataType == "paragraph-sentiment") {
      return Promise.all([
        //overall accuracy

        ParagraphSentiment.find().and([
          {
            "group": groupID,
          },
          {
            "linguistics": {
              $elemMatch: {
                answer: { $exists: true },
              }
            }
          },
          {
            annotators: {
              $elemMatch: {
                user_id: userID,
                answer: { $exists: true }
              }
            }
          }
        ]),

      ]).then(([
        paragraph_sentiment_data
      ]) => {

        let paragraph_sentiment_accuracy = CalculatePerformanceController.paragraphSentimentPerformance(
          paragraph_sentiment_data, userID
        )

        return parseInt(paragraph_sentiment_accuracy)

      }).catch((err) => {
        throw err; // res.sendStatus(500) might be better here.
      });

    } else if (dataType == "document-sentiment") {

      return Promise.all([
        //overall accuracy

        DocumentSentiment.find().and([
          {
            "group": groupID,
          },
          {
            "linguistics": {
              $elemMatch: {
                answer: { $exists: true },
              }
            }
          },
          {
            annotators: {
              $elemMatch: {
                user_id: userID,
                answer: { $exists: true }
              }
            }
          }
        ]),

      ]).then(([
        document_sentiment_data
      ]) => {

        let document_sentiment_accuracy = CalculatePerformanceController.documentSentimentPerformance(
          document_sentiment_data, userID
        )

        return parseInt(document_sentiment_accuracy)

      }).catch((err) => {
        throw err; // res.sendStatus(500) might be better here.
      });
    } else if (dataType == "sentence-emotion") {
      return Promise.all([
        //overall accuracy

        SentenceEmotion.find().and([
          {
            "group": groupID,
          },
          {
            "linguistics": {
              $elemMatch: {
                answer: { $exists: true },
              }
            }
          },
          {
            annotators: {
              $elemMatch: {
                user_id: userID,
                answer: { $exists: true }
              }
            }
          }
        ]),

      ]).then(([
        sentence_emotion_data
      ]) => {

        let sentence_emotion_accuracy = CalculatePerformanceController.sentenceEmotionPerformance(
          sentence_emotion_data, userID
        )

        return parseInt(sentence_emotion_accuracy)

      }).catch((err) => {
        throw err; // res.sendStatus(500) might be better here.
      });

    } else if (dataType == "paragraph-emotion") {
      return Promise.all([
        //overall accuracy

        ParagraphEmotion.find().and([
          {
            "group": groupID,
          },
          {
            "linguistics": {
              $elemMatch: {
                answer: { $exists: true },
              }
            }
          },
          {
            annotators: {
              $elemMatch: {
                user_id: userID,
                answer: { $exists: true }
              }
            }
          }
        ])

      ]).then(([
        paragraph_emotion_data
      ]) => {

        let paragraph_emotion_accuracy = CalculatePerformanceController.paragraphEmotionPerformance(
          paragraph_emotion_data, userID
        )

        return parseInt(paragraph_emotion_accuracy)

      }).catch((err) => {
        throw err; // res.sendStatus(500) might be better here.
      });
    }
  }

  /**
   * Calculate sentence sentiment performance
   * 
   * @param {*} req 
   * @param {*} res 
   */
  static senteneSentimentPerformance(sentence_sentiment_data, userID) {
    let total_instance = sentence_sentiment_data.length;
    if (total_instance == 0) return 0
    let correct_instance = 0;

    for (let sentence of sentence_sentiment_data) {
      let linguistic_answer = {};

      if(sentence.linguistics.length == 1){
        linguistic_answer = sentence.linguistics[0].answer;
      } else {
        linguistic_answer = MajorityMatchController.sentenceSentimentMajorityMatch(
          sentence.linguistics
        );
      }
      
      if (!Object.keys(linguistic_answer).length) {
        total_instance--;
        CalculatePerformanceController.deleteData(sentence._id, 'sentence-sentiment');
      }

      let user_index = sentence.annotators.findIndex(
        (item) => String(item.user_id) == String(userID)
      );

      if (JSON.stringify(linguistic_answer) == JSON.stringify(sentence.annotators[user_index].answer)) {
        correct_instance++;
      }
    }

    if (total_instance == 0) return 0
    return (correct_instance / total_instance) * 100
  }


  /**
  * Calculate paragraph sentiment performance
  * 
  * @param {*} req 
  * @param {*} res 
  */
  static paragraphSentimentPerformance(paragraph_sentiment_data, userID) {
    let total_instance = paragraph_sentiment_data.length;
    if (total_instance == 0) return 0
    let correct_instance = 0;

    for (let paragraph of paragraph_sentiment_data) {
      let linguistic_answer = {};
      
      if(paragraph.linguistics.length == 1){
        linguistic_answer = paragraph.linguistics[0].answer;
      } else {
        linguistic_answer = MajorityMatchController.paragraphAndDocumentSentimentMajorityMatch(
          paragraph.linguistics
        );
      }

      if (!linguistic_answer.sentiment_overall.length) {
        total_instance--;
        CalculatePerformanceController.deleteData(paragraph._id, 'paragraph-sentiment');
      }

      let user_index = paragraph.annotators.findIndex(
        (item) => String(item.user_id) == String(userID)
      );

      if (linguistic_answer.sentiment_overall == paragraph.annotators[user_index].answer.sentiment_overall) {
        correct_instance++;
      }
    }

    if (total_instance == 0) return 0
    return (correct_instance / total_instance) * 100
  }


  /**
  * Calculate document sentiment performance
  * 
  * @param {*} req 
  * @param {*} res 
  */
  static documentSentimentPerformance(document_sentiment_data, userID) {
    let total_instance = document_sentiment_data.length;
    if (total_instance == 0) return 0
    let correct_instance = 0;

    for (let document of document_sentiment_data) {
      let linguistic_answer = {};
      
      if(document.linguistics.length == 1){
        linguistic_answer = document.linguistics[0].answer;
      } else {
        linguistic_answer = MajorityMatchController.paragraphAndDocumentSentimentMajorityMatch(
          document.linguistics
        );
      }

      if (!linguistic_answer.sentiment_overall.length) {
        total_instance--;
        CalculatePerformanceController.deleteData(document._id, 'document-sentiment');
      }

      let user_index = document.annotators.findIndex(
        (item) => String(item.user_id) == String(userID)
      );

      if (linguistic_answer.sentiment_overall == document.annotators[user_index].answer.sentiment_overall) {
        correct_instance++;
      }
    }

    if (total_instance == 0) return 0
    return (correct_instance / total_instance) * 100
  }

  /**
  * Calculate sentence emotion performance
  * 
  * @param {*} req 
  * @param {*} res 
  */
  static sentenceEmotionPerformance(sentence_emotion_data, userID) {
    let total_instance = sentence_emotion_data.length;
    if (total_instance == 0) return 0
    let correct_instance = 0;

    for (let sentence of sentence_emotion_data) {
      let linguistic_answer = {};
      
      if(sentence.linguistics.length == 1){
        linguistic_answer = sentence.linguistics[0].answer;
      } else {
        linguistic_answer = MajorityMatchController.sentenceEmotionMajorityMatch(
          sentence.linguistics
        );
      }

      let user_index = sentence.annotators.findIndex(
        (item) => String(item.user_id) == String(userID)
      );

      if (!linguistic_answer.emotions.length) {
        total_instance--;
        CalculatePerformanceController.deleteData(sentence._id, 'sentence-emotion');
      } else {
        let matchElement = false;

        for (let emotion of sentence.annotators[user_index].answer.emotions) {
          if (linguistic_answer.emotions.includes(emotion)) {
            matchElement = true;
          }
        }

        if (matchElement) {
          correct_instance++;
        }
      }
    }

    if (total_instance == 0) return 0
    return (correct_instance / total_instance) * 100
  }

  /**
  * Calculate paragraph emotion performance
  * 
  * @param {*} req 
  * @param {*} res 
  */
  static paragraphEmotionPerformance(paragraph_emotion_data, userID) {
    let total_instance = paragraph_emotion_data.length;
    if (total_instance == 0) return 0
    let correct_instance = 0;

    for (let paragraph of paragraph_emotion_data) {
      let linguistic_answer = {};
      
      if(paragraph.linguistics.length == 1){
        linguistic_answer = paragraph.linguistics[0].answer;
      } else {
        linguistic_answer = MajorityMatchController.paragraphEmotionMajorityMatch(
          paragraph.linguistics
        );
      }

      let user_index = paragraph.annotators.findIndex(
        (item) => String(item.user_id) == String(userID)
      );

      if (!linguistic_answer.sentiment_overall.length) {
        total_instance--;
        CalculatePerformanceController.deleteData(paragraph._id, 'paragraph-emotion');
      } else {
        let matchElement = false;

        for (let emotion of paragraph.annotators[user_index].answer.sentiment_overall) {
          if (linguistic_answer.sentiment_overall.includes(emotion)) {
            matchElement = true;
          }
        }

        if (matchElement) {
          correct_instance++;
        }
      }
    }

    if (total_instance == 0) return 0
    return (correct_instance / total_instance) * 100
  }

  /**
    * Delete Data
    * 
    * @param {*} req 
    * @param {*} res 
  */
  static async deleteData(id, dataType) {
    if (dataType == 'sentence-sentiment') {
      await SentenceSentiment.find({ _id: id }).remove().exec();
    } else if (dataType == 'paragraph-sentiment') {
      await ParagraphSentiment.find({ _id: id }).remove().exec();
    } else if (dataType == 'document-sentiment') {
      await DocumentSentiment.find({ _id: id }).remove().exec();
    } else if (dataType == 'sentence-emotion') {
      SentenceEmotion.find({ _id: id }).remove().exec();
    } else if (dataType == 'paragraph-emotion') {
      await ParagraphEmotion.find({ _id: id }).remove().exec();
    }
  }

}