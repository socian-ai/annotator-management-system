import mongoose from "mongoose";

mongoose.Promise = global.Promise;

import User from "../models/User.js";
import Group from "../models/Group.js";
import SentenceSentiment from "../models/SentenceSentiment.js";
import ParagraphSentiment from "../models/ParagraphSentiment.js";
import DocumentSentiment from "../models/DocumentSentiment.js";
import SentenceEmotion from "../models/SentenceEmotion.js";
import ParagraphEmotion from "../models/ParagraphEmotion.js";
import FinalData from "../models/FinalData.js";

export default class DashboardController {


  static overallStatistics(req, res) {
    try {
      Promise.all([
        // Total Annotators
        User.count({
          status : "active",
          type: 'annotator'
        }).exec(),
        // Total Linguistic
        User.count({
          status : "active",
          type: 'linguistic'
        }).exec(),
        // Total Group
        Group.count({
          status : "active"
        }).exec(),
        // Available Data
        SentenceSentiment.count({"group": undefined}).exec(),
        ParagraphSentiment.count({"group": undefined}).exec(),
        DocumentSentiment.count({"group": undefined}).exec(),
        SentenceEmotion.count({"group": undefined}).exec(),
        ParagraphEmotion.count({"group": undefined}).exec(),
        // Assigned data
        SentenceSentiment.count({ "group": { $exists: true } }).exec(),
        ParagraphSentiment.count({ "group": { $exists: true } }).exec(),
        DocumentSentiment.count({ "group": { $exists: true } }).exec(),
        SentenceEmotion.count({ "group": { $exists: true } }).exec(),
        ParagraphEmotion.count({ "group": { $exists: true } }).exec(),
        // Annotated Data
        FinalData.count({ "data_type": 'sentence-sentiment' }).exec(),
        FinalData.count({ "data_type": 'paragraph-sentiment' }).exec(),
        FinalData.count({ "data_type": 'document-sentiment' }).exec(),
        FinalData.count({ "data_type": 'sentence-emotion' }).exec(),
        FinalData.count({ "data_type": 'paragraph-emotion' }).exec(),
        // Unannotated data
        SentenceSentiment.count({}).exec(),
        ParagraphSentiment.count({}).exec(),
        DocumentSentiment.count({}).exec(),
        SentenceEmotion.count({}).exec(),
        ParagraphEmotion.count({}).exec(),
      ])
        .then(([
          annotators, linguistic, groups,
          sentence_sentiment, paragraph_sentiment,
          document_sentiment, sentence_emotion,
          paragraph_emotion,
          sentence_sentiment_assigned,
          pagragraph_sentiment_assigned,
          document_sentiment_assigned,
          sentence_emotion_assigned,
          paragraph_emotion_assigned,
          sentence_sentiment_annotated,
          paragraph_sentiment_annotated,
          document_sentiment_annotated,
          sentence_emotion_annotated,
          paragraph_emotion_annotated,
          sentence_sentiment_unannotated,
          paragraph_sentiment_unannotated,
          document_sentiment_unannotated,
          sentence_emotion_unannotated,
          paragraph_emotion_unannotated
        ]) => {
          res.json({
            status: "success",
            basic_statics: {
              total_annotators: annotators,
              total_linguistics: linguistic,
              total_groups: groups
            },
            available_data: {
              "sentence_sentiment": sentence_sentiment,
              "paragraph_sentiment": paragraph_sentiment,
              "document_sentiment": document_sentiment,
              "sentence_emotion": sentence_emotion,
              "paragraph_emotion": paragraph_emotion,
            },
            assigned_data: {
              "sentence_sentiment": sentence_sentiment_assigned,
              "paragraph_sentiment": pagragraph_sentiment_assigned,
              "document_sentiment": document_sentiment_assigned,
              "sentence_emotion": sentence_emotion_assigned,
              "paragraph_emotion": paragraph_emotion_assigned
            },
            annotated_data: {
              "sentence_sentiment": sentence_sentiment_annotated,
              "paragraph_sentiment": paragraph_sentiment_annotated,
              "document_sentiment": document_sentiment_annotated,
              "sentence_emotion": sentence_emotion_annotated,
              "paragraph_emotion": paragraph_emotion_annotated
            },
            unannotated_data: {
              "sentence_sentiment": sentence_sentiment_unannotated,
              "paragraph_sentiment": paragraph_sentiment_unannotated,
              "document_sentiment": document_sentiment_unannotated,
              "sentence_emotion": sentence_emotion_unannotated,
              "paragraph_emotion": paragraph_emotion_unannotated
            }
          });
        })
        .catch((err) => {
          throw err; // res.sendStatus(500) might be better here.
        });
    } catch (error) {
      console.log(error);
      res
        .status(500)
        .json({ status: "error", message: "somethings went wrong." });
    }
  }

  static classDistributions(req, res) {
    try {
      Promise.all([
        // Subjectivity
        FinalData.count({
          "data_type" : "sentence-sentiment",
          "answer.subjectivity": 'subjective'
        }).exec(),
        FinalData.count({
          "data_type" : "sentence-sentiment",
          "answer.subjectivity": 'objective'
        }).exec(),

        // Sentence Polarity
        FinalData.count({
          "data_type": "sentence-sentiment",
          "answer.polarity": 'SP'
        }).exec(),
        FinalData.count({
          "data_type": "sentence-sentiment",
          "answer.polarity": 'WP'
        }).exec(),
        FinalData.count({
          "data_type": "sentence-sentiment",
          "answer.polarity": 'SN'
        }).exec(),
        FinalData.count({
          "data_type": "sentence-sentiment",
          "answer.polarity": 'WN'
        }).exec(),
        FinalData.count({
          "data_type": "sentence-sentiment",
          "answer.polarity": 'NU'
        }).exec(),

        // Paragraph Polarity
        FinalData.count({
          "data_type": "paragraph-sentiment",
          "answer.sentiment_overall": 'SP'
        }).exec(),
        FinalData.count({
          "data_type": "paragraph-sentiment",
          "answer.sentiment_overall": 'WP'
        }).exec(),
        FinalData.count({
          "data_type": "paragraph-sentiment",
          "answer.sentiment_overall": 'SN'
        }).exec(),
        FinalData.count({
          "data_type": "paragraph-sentiment",
          "answer.sentiment_overall": 'WN'
        }).exec(),
        FinalData.count({
          "data_type": "paragraph-sentiment",
          "answer.sentiment_overall": 'NU'
        }).exec(),

        // Doccument Polarity
        FinalData.count({
          "data_type": "document-sentiment",
          "answer.sentiment_overall": 'SP'
        }).exec(),
        FinalData.count({
          "data_type": "document-sentiment",
          "answer.sentiment_overall": 'WP'
        }).exec(),
        FinalData.count({
          "data_type": "document-sentiment",
          "answer.sentiment_overall": 'SN'
        }).exec(),
        FinalData.count({
          "data_type": "document-sentiment",
          "answer.sentiment_overall": 'WN'
        }).exec(),
        FinalData.count({
          "data_type": "document-sentiment",
          "answer.sentiment_overall": 'NU'
        }).exec(),

        // Sentence Emotion
        FinalData.count({
          "data_type": "sentence-emotion",
          "answer.emotions": "Happy"
        }).exec(),
        FinalData.count({
          "data_type": "sentence-emotion",
          "answer.emotions": "Neutral"
        }).exec(),
        FinalData.count({
          "data_type": "sentence-emotion",
          "answer.emotions": "Fear"
        }).exec(),
        FinalData.count({
          "data_type": "sentence-emotion",
          "answer.emotions": "Anger/Disgust"
        }).exec(),
        FinalData.count({
          "data_type": "sentence-emotion",
          "answer.emotions": "Sadness"
        }).exec(),
        FinalData.count({
          "data_type": "sentence-emotion",
          "answer.emotions": "Surprise"
        }).exec(),

        // Sentence Emotion
        FinalData.count({
          "data_type": "paragraph-emotion",
          "answer.emotions": "Happy"
        }).exec(),
        FinalData.count({
          "data_type": "paragraph-emotion",
          "answer.emotions": "Neutral"
        }).exec(),
        FinalData.count({
          "data_type": "paragraph-emotion",
          "answer.emotions": "Fear"
        }).exec(),
        FinalData.count({
          "data_type": "paragraph-emotion",
          "answer.emotions": "Anger/Disgust"
        }).exec(),
        FinalData.count({
          "data_type": "paragraph-emotion",
          "answer.emotions": "Sadness"
        }).exec(),
        FinalData.count({
          "data_type": "paragraph-emotion",
          "answer.emotions": "Surprise"
        }).exec(),
      ])
        .then(([
          subjective, objective,
          ss_sp, ss_wp, ss_sn, ss_wn, ss_nu,
          ps_sp, ps_wp, ps_sn, ps_wn, ps_nu,
          ds_sp, ds_wp, ds_sn, ds_wn, ds_nu,
          se_happy, se_neutral, se_fear, se_anger, se_sadness, se_surprise,
          pe_happy, pe_neutral, pe_fear, pe_anger, pe_sadness, pe_surprise,
        ]) => {
          res.json({
            status: "success",
            subjectivity: {
              subjective: subjective,
              objective: objective
            },
            sentence_polarity: {
              "strongly_positive": ss_sp,
              "weekly_positive": ss_wp,
              "strongly_negative": ss_sn,
              "weekly_negative": ss_wn,
              "neutral": ss_nu,
            },
            paragraph_polarity: {
              "strongly_positive": ps_sp,
              "weekly_positive": ps_wp,
              "strongly_negative": ps_sn,
              "weekly_negative": ps_wn,
              "neutral": ps_nu,
            },
            document_polarity: {
              "strongly_positive": ds_sp,
              "weekly_positive": ds_wp,
              "strongly_negative": ds_sn,
              "weekly_negative": ds_wn,
              "neutral": ds_nu,
            },
            sentence_emotion: {
              "happy": se_happy,
              "neutral": se_neutral,
              "fear": se_fear,
              "anger": se_anger,
              "sadness": se_sadness,
              "surprise": se_surprise
            },
            paragraph_emotion: {
              "happy": pe_happy,
              "neutral": pe_neutral,
              "fear": pe_fear,
              "anger": pe_anger,
              "sadness": pe_sadness,
              "surprise": pe_surprise
            }
          });
        })
        .catch((err) => {
          throw err; // res.sendStatus(500) might be better here.
        });
    } catch (error) {
      console.log(error);
      res
        .status(500)
        .json({ status: "error", message: "somethings went wrong." });
    }
  }

  static domainDistributions(req, res) {
    try {
      Promise.all([
        // Sentence Subjectivity
        FinalData.aggregate([
          {
            "$match": { "data_type": "sentence-sentiment" }
          },
          {
            $group: {
              _id: "$domain",
              count: { $sum: 1 }
            }
          }
        ]).exec(),

        // Polarity
        FinalData.aggregate([
          {
            $match: {
              $and: [
                { "data_type": "sentence-sentiment" },
                { "answer.polarity": {$exists: true, $not: {$size: 0}}}
              ]
            }
          },
          {
            $group: {
              _id: "$domain",
              count: { $sum: 1 }
            }
          }
        ]).exec(),

        FinalData.aggregate([
          {
            $match: {
              $and: [
                { "data_type": "paragraph-sentiment" },
                { "answer.sentiment_overall": {$exists: true, $not: {$size: 0}}}
              ]
            }
          },
          {
            $group: {
              _id: "$domain",
              count: { $sum: 1 }
            }
          }
        ]).exec(),

        FinalData.aggregate([
          {
            $match: {
              $and: [
                { "data_type": "document-sentiment" },
                { "answer.sentiment_overall": {$exists: true, $not: {$size: 0}}}
              ]
            }
          },
          {
            $group: {
              _id: "$domain",
              count: { $sum: 1 }
            }
          }
        ]).exec(),

        // Emotion Domain Distribution
        FinalData.aggregate([
          {
            "$match": { "data_type": "sentence-emotion" }
          },
          {
            $group: {
              _id: "$domain",
              count: { $sum: 1 }
            }
          }
        ]).exec(),

        FinalData.aggregate([
          {
            "$match": { "data_type": "paragraph-emotion" }
          },
          {
            $group: {
              _id: "$domain",
              count: { $sum: 1 }
            }
          }
        ]).exec(),

      ])
        .then(([
          sentence_subjectivity, sentence_sentiment_polarity,
          paragraph_sentiment_polarity, document_sentiment_polarity,
          sentence_emotion, paragraph_emotion
        ]) => {
          res.json({
            status: "success",
            sentence_subjectivity: sentence_subjectivity,
            polarity: {
              sentence: sentence_sentiment_polarity,
              paragraph: paragraph_sentiment_polarity,
              document: document_sentiment_polarity,
            },
            emotion: {
              sentence: sentence_emotion,
              paragraph: paragraph_emotion
            }
          });
        })
        .catch((err) => {
          throw err; // res.sendStatus(500) might be better here.
        });
    } catch (error) {
      console.log(error);
      res
        .status(500)
        .json({ status: "error", message: "somethings went wrong." });
    }
  }
}
