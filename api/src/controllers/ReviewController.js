import LinguisticData from "../models/LinguisticData.js";
import FinalData from "../models/FinalData.js";
import Group from "../models/Group.js";
import MajorityMatchController from "./MajorityMatchController.js";

export default class ReviewController {
  /**
   * @param req
   * @param res
   */
  static async list(req, res) {
    const groupID = req.query.groupID;
    const instanceType = req.query.instanceType;
    const dataType = req.query.dataType;
    const filterType = req.query.filterType;
    const userID = req.decodedToken._id;

    if (instanceType == "sentence-sentiment") {
      const sentence_sentiment_list = await ReviewController.sentenceSentimentList(userID, groupID, dataType, filterType);
      res.json(sentence_sentiment_list);
    } else if (instanceType == "sentence-emotion") {
      const sentence_emotion_list = await ReviewController.sentenceEmotionList(userID, groupID, dataType, filterType);
      res.json(sentence_emotion_list);
    } else if (instanceType == "paragraph-sentiment") {
      const paragraph_sentiment_list = await ReviewController.paragraphSentimentList(userID, groupID, dataType, filterType);
      res.json(paragraph_sentiment_list);
    } else if (instanceType == "paragraph-emotion") {
      const paragraph_emotion_list = await ReviewController.paragraphEmotionList(userID, groupID, dataType, filterType);
      res.json(paragraph_emotion_list);
    } else if (instanceType == "document-sentiment") {
      const document_sentiment_list = await ReviewController.documentSentimentList(userID, groupID, dataType, filterType);
      res.json(document_sentiment_list);
    } else {
      res.json({
        "status": "failed",
        "message": "Invalid request"
      });
    }
  }

  /**
   * 
   * @param {dataType, filterType} 
   * 
   */

  static async sentenceSentimentList(userID, groupID, dataType, filterType) {
    const group = await Group.findById(groupID).populate("users", "name email status");
    let dataQuery = { linguistic_group: groupID, data_type: 'sentence-sentiment' };

    if (filterType == 'validation-complete') {
      dataQuery['validation'] = true;
    } else if (filterType == 'validation-incomplete') {
      dataQuery['validation'] = false;
    }

    if (dataType == 'linguistic-data') {
      dataQuery['linguistic_validator'] = userID;
      let sentence_sentiment_data = await LinguisticData.find(dataQuery).lean();

      const sentence_sentiment_count = await LinguisticData.count({
        linguistic_group: groupID,
        linguistic_validator: userID,
        data_type: 'sentence-sentiment'
      });

      const sentence_sentiment_validation_count = await LinguisticData.count({
        linguistic_group: groupID,
        validation: true,
        linguistic_validator: userID,
        data_type: 'sentence-sentiment'
      });

      sentence_sentiment_data = sentence_sentiment_data.filter( sentence => {
        if(sentence.linguistics.length){
          sentence['linguistic_answer'] =  MajorityMatchController.sentenceSentimentMajorityMatch(sentence.linguistics);
        }
        
        return sentence
      });

      let annotators = [];
      if (sentence_sentiment_data.length) {
        const annotator_group_id = sentence_sentiment_data[0].group;

        const annotator_group = await Group.findById(annotator_group_id)
          .select("name status group_type")
          .populate("users", "name email status");

        annotators = annotator_group.users;
      }

      return {
        status: "success",
        group: group,
        data: sentence_sentiment_data,
        annotators: annotators,
        validation_complete: sentence_sentiment_validation_count,
        number_of_data: sentence_sentiment_count
      }
    } else if (dataType == 'final-data') {
      let sentence_sentiment_data = await FinalData.find(dataQuery).lean();

      const sentence_sentiment_count = await FinalData.count({
        linguistic_group: groupID,
        data_type: 'sentence-sentiment'
      });

      const sentence_sentiment_validation_count = await FinalData.count({
        linguistic_group: groupID,
        validation: true,
        data_type: 'sentence-sentiment'
      });

      sentence_sentiment_data = sentence_sentiment_data.filter( sentence => {
        if(sentence.linguistics.length){
          sentence['linguistic_answer'] =  MajorityMatchController.sentenceSentimentMajorityMatch(sentence.linguistics);
        }
        
        return sentence
      });

      let annotators = [];
      if (sentence_sentiment_data.length) {
        const annotator_group_id = sentence_sentiment_data[0].group;

        const annotator_group = await Group.findById(annotator_group_id)
          .select("name status group_type")
          .populate("users", "name email status");

        annotators = annotator_group.users;
      }

      return {
        status: "success",
        group: group,
        data: sentence_sentiment_data,
        annotators: annotators,
        validation_complete: sentence_sentiment_validation_count,
        number_of_data: sentence_sentiment_count
      }
    } else {
      return {
        "status": "Failed",
        "message": "Invalid Request"
      }
    }
  }

  /**
   * 
   * @param {dataType, filterType} 
   * 
   */

  static async paragraphSentimentList(userID, groupID, dataType, filterType) {
    const group = await Group.findById(groupID).populate("users", "name email status");
    let dataQuery = { linguistic_group: groupID, data_type: 'paragraph-sentiment' };

    if (filterType == 'validation-complete') {
      dataQuery['validation'] = true;
    } else if (filterType == 'validation-incomplete') {
      dataQuery['validation'] = false;
    }

    if (dataType == 'linguistic-data') {
      dataQuery['linguistic_validator'] = userID;
      let paragraph_sentiment_data = await LinguisticData.find(dataQuery).lean();

      const paragraph_sentiment_count = await LinguisticData.count({
        linguistic_group: groupID,
        linguistic_validator: userID,
        data_type: 'paragraph-sentiment'
      });

      const paragraph_sentiment_validation_count = await LinguisticData.count({
        linguistic_group: groupID,
        linguistic_validator: userID,
        data_type: 'paragraph-sentiment',
        validation: true
      });

      paragraph_sentiment_data = paragraph_sentiment_data.filter( paragraph => {
        if(paragraph.linguistics.length){
          paragraph['linguistic_answer'] =  MajorityMatchController.paragraphAndDocumentSentimentMajorityMatch(paragraph.linguistics);
        }
        
        return paragraph
      });

      let annotators = [];
      if (paragraph_sentiment_data.length) {
        const annotator_group_id = paragraph_sentiment_data[0].group;

        const annotator_group = await Group.findById(annotator_group_id)
          .select("name status group_type")
          .populate("users", "name email status");

        annotators = annotator_group.users;
      }

      return {
        status: "success",
        group: group,
        data: paragraph_sentiment_data,
        annotators: annotators,
        validation_complete: paragraph_sentiment_validation_count,
        number_of_data: paragraph_sentiment_count
      }
    } else if (dataType == 'final-data') {
      let paragraph_sentiment_data = await FinalData.find(dataQuery).lean();

      const paragraph_sentiment_count = await FinalData.count({
        linguistic_group: groupID,
        data_type: 'paragraph-sentiment'
      });

      const paragraph_sentiment_validation_count = await FinalData.count({
        linguistic_group: groupID,
        validation: true,
        data_type: 'paragraph-sentiment'
      });

      paragraph_sentiment_data = paragraph_sentiment_data.filter( paragraph => {
        if(paragraph.linguistics.length){
          paragraph['linguistic_answer'] =  MajorityMatchController.paragraphAndDocumentSentimentMajorityMatch(paragraph.linguistics);
        }
        
        return paragraph
      });

      let annotators = [];
      if (paragraph_sentiment_data.length) {
        const annotator_group_id = paragraph_sentiment_data[0].group;

        const annotator_group = await Group.findById(annotator_group_id)
          .select("name status group_type")
          .populate("users", "name email status");

        annotators = annotator_group.users;
      }

      return {
        status: "success",
        group: group,
        data: paragraph_sentiment_data,
        annotators: annotators,
        validation_complete: paragraph_sentiment_validation_count,
        number_of_data: paragraph_sentiment_count
      }
    } else {
      return {
        "status": "Failed",
        "message": "Invalid Request"
      }
    }
  }

  /**
   * 
   * @param {dataType, filterType} 
   * 
   */

  static async documentSentimentList(userID, groupID, dataType, filterType) {
    const group = await Group.findById(groupID).populate("users", "name email status");
    let dataQuery = { linguistic_group: groupID, data_type: 'document-sentiment' };

    if (filterType == 'validation-complete') {
      dataQuery['validation'] = true;
    } else if (filterType == 'validation-incomplete') {
      dataQuery['validation'] = false;
    }

    if (dataType == 'linguistic-data') {
      dataQuery['linguistic_validator'] = userID;
      let document_sentiment_data = await LinguisticData.find(dataQuery).lean();

      const document_sentiment_count = await LinguisticData.count({
        linguistic_group: groupID,
        linguistic_validator: userID,
        data_type: 'document-sentiment'
      });

      const document_sentiment_validation_count = await LinguisticData.count({
        linguistic_group: groupID,
        validation: true,
        linguistic_validator: userID,
        data_type: 'document-sentiment'
      });

      document_sentiment_data = document_sentiment_data.filter( document => {
        if(document.linguistics.length){
          document['linguistic_answer'] =  MajorityMatchController.paragraphAndDocumentSentimentMajorityMatch(document.linguistics);
        }
        
        return document
      });

      let annotators = [];
      if (document_sentiment_data.length) {
        const annotator_group_id = document_sentiment_data[0].group;

        const annotator_group = await Group.findById(annotator_group_id)
          .select("name status group_type")
          .populate("users", "name email status");

        annotators = annotator_group.users;
      }

      return {
        status: "success",
        group: group,
        data: document_sentiment_data,
        annotators: annotators,
        validation_complete: document_sentiment_validation_count,
        number_of_data: document_sentiment_count
      }
    } else if (dataType == 'final-data') {
      let document_sentiment_data = await FinalData.find(dataQuery).lean();

      const document_sentiment_count = await FinalData.count({
        linguistic_group: groupID,
        data_type: 'document-sentiment'
      });

      const document_sentiment_validation_count = await FinalData.count({
        linguistic_group: groupID,
        validation: true,
        data_type: 'document-sentiment'
      });

      document_sentiment_data = document_sentiment_data.filter( document => {
        if(document.linguistics.length){
          document['linguistic_answer'] =  MajorityMatchController.paragraphAndDocumentSentimentMajorityMatch(document.linguistics);
        }
        
        return document
      });

      let annotators = [];
      if (document_sentiment_data.length) {
        const annotator_group_id = document_sentiment_data[0].group;

        const annotator_group = await Group.findById(annotator_group_id)
          .select("name status group_type")
          .populate("users", "name email status");

        annotators = annotator_group.users;
      }

      return {
        status: "success",
        group: group,
        data: document_sentiment_data,
        annotators: annotators,
        validation_complete: document_sentiment_validation_count,
        number_of_data: document_sentiment_count
      }
    } else {
      return {
        "status": "Failed",
        "message": "Invalid Request"
      }
    }
  }

  /**
   * 
   * @param {dataType, filterType} 
   * 
   */

  static async sentenceEmotionList(userID, groupID, dataType, filterType) {
    const group = await Group.findById(groupID).populate("users", "name email status");
    let dataQuery = { linguistic_group: groupID, data_type: 'sentence-emotion' };

    if (filterType == 'validation-complete') {
      dataQuery['validation'] = true;
    } else if (filterType == 'validation-incomplete') {
      dataQuery['validation'] = false;
    }

    if (dataType == 'linguistic-data') {
      dataQuery['linguistic_validator'] = userID;
      let sentence_emotion_data = await LinguisticData.find(dataQuery).lean();

      const sentence_emotion_count = await LinguisticData.count({
        linguistic_group: groupID,
        linguistic_validator: userID,
        data_type: 'sentence-emotion'
      });

      const sentence_emotion_validation_count = await LinguisticData.count({
        linguistic_group: groupID,
        validation: true,
        linguistic_validator: userID,
        data_type: 'sentence-emotion'
      });

      sentence_emotion_data = sentence_emotion_data.filter( sentence => {
        if(sentence.linguistics.length){
          sentence['linguistic_answer'] =  MajorityMatchController.sentenceEmotionMajorityMatch(sentence.linguistics);
        }
        
        return sentence
      });

      let annotators = [];
      if (sentence_emotion_data.length) {
        const annotator_group_id = sentence_emotion_data[0].group;

        const annotator_group = await Group.findById(annotator_group_id)
          .select("name status group_type")
          .populate("users", "name email status");

        annotators = annotator_group.users;
      }

      return {
        status: "success",
        group: group,
        data: sentence_emotion_data,
        annotators: annotators,
        validation_complete: sentence_emotion_validation_count,
        number_of_data: sentence_emotion_count
      }
    } else if (dataType == 'final-data') {
      let sentence_emotion_data = await FinalData.find(dataQuery).lean();

      const sentence_emotion_count = await FinalData.count({
        linguistic_group: groupID,
        data_type: 'sentence-emotion'
      });

      const sentence_emotion_validation_count = await FinalData.count({
        linguistic_group: groupID,
        validation: true,
        data_type: 'sentence-emotion'
      });

      sentence_emotion_data = sentence_emotion_data.filter( sentence => {
        if(sentence.linguistics.length){
          sentence['linguistic_answer'] =  MajorityMatchController.sentenceEmotionMajorityMatch(sentence.linguistics);
        }
        
        return sentence
      });

      let annotators = [];
      if (sentence_emotion_data.length) {
        const annotator_group_id = sentence_emotion_data[0].group;

        const annotator_group = await Group.findById(annotator_group_id)
          .select("name status group_type")
          .populate("users", "name email status");

        annotators = annotator_group.users;
      }

      return {
        status: "success",
        group: group,
        data: sentence_emotion_data,
        annotators: annotators,
        validation_complete: sentence_emotion_validation_count,
        number_of_data: sentence_emotion_count
      }
    } else {
      return {
        "status": "Failed",
        "message": "Invalid Request"
      }
    }
  }

  /**
   * 
   * @param {dataType, filterType} 
   * 
   */

  static async paragraphEmotionList(userID, groupID, dataType, filterType) {
    const group = await Group.findById(groupID).populate("users", "name email status");
    let dataQuery = { linguistic_group: groupID, data_type: 'paragraph-emotion' };

    if (filterType == 'validation-complete') {
      dataQuery['validation'] = true;
    } else if (filterType == 'validation-incomplete') {
      dataQuery['validation'] = false;
    }

    if (dataType == 'linguistic-data') {
      dataQuery['linguistic_validator'] = userID;
      let paragraph_emotion_data = await LinguisticData.find(dataQuery).lean();

      const paragraph_emotion_count = await LinguisticData.count({
        linguistic_group: groupID,
        linguistic_validator: userID,
        data_type: 'paragraph-emotion'
      });

      const paragraph_emotion_validation_count = await LinguisticData.count({
        linguistic_group: groupID,
        linguistic_validator: userID,
        validation: true,
        data_type: 'paragraph-emotion'
      });

      paragraph_emotion_data = paragraph_emotion_data.filter( paragraph => {
        if(paragraph.linguistics.length){
          paragraph['linguistic_answer'] =  MajorityMatchController.paragraphEmotionMajorityMatch(paragraph.linguistics);
        }
        
        return paragraph
      });

      let annotators = [];
      if (paragraph_emotion_data.length) {
        const annotator_group_id = paragraph_emotion_data[0].group;

        const annotator_group = await Group.findById(annotator_group_id)
          .select("name status group_type")
          .populate("users", "name email status");

        annotators = annotator_group.users;
      }

      return {
        status: "success",
        group: group,
        data: paragraph_emotion_data,
        annotators: annotators,
        validation_complete: paragraph_emotion_validation_count,
        number_of_data: paragraph_emotion_count
      }
    } else if (dataType == 'final-data') {
      let paragraph_emotion_data = await FinalData.find(dataQuery).lean();

      const paragraph_emotion_count = await FinalData.count({
        linguistic_group: groupID,
        data_type: 'paragraph-emotion'
      });

      const paragraph_emotion_validation_count = await FinalData.count({
        linguistic_group: groupID,
        validation: true,
        data_type: 'paragraph-emotion'
      });

      paragraph_emotion_data = paragraph_emotion_data.filter( paragraph => {
        if(paragraph.linguistics.length){
          paragraph['linguistic_answer'] =  MajorityMatchController.paragraphEmotionMajorityMatch(paragraph.linguistics);
        }
        
        return paragraph
      });

      let annotators = [];
      if (paragraph_emotion_data.length) {
        const annotator_group_id = paragraph_emotion_data[0].group;

        const annotator_group = await Group.findById(annotator_group_id)
          .select("name status group_type")
          .populate("users", "name email status");

        annotators = annotator_group.users;
      }

      return {
        status: "success",
        group: group,
        data: paragraph_emotion_data,
        annotators: annotators,
        validation_complete: paragraph_emotion_validation_count,
        number_of_data: paragraph_emotion_count
      }
    } else {
      return {
        "status": "Failed",
        "message": "Invalid Request"
      }
    }
  }

  /**
   * Update
   *
   * @param req
   * @param res
   */
  static async validationUpdate(req, res) {
    try {
      FinalData.findById({ "_id": req.body.id }, (err, finalData) => {
        if (err) {
          res.status(500).json({ "status": "error", "message": err });
        } else {

          if (finalData.validation) {
            finalData.validation = false;
          } else {
            finalData.validation = true;
          }

          finalData.linguistic_validator = req.decodedToken._id;

          finalData.save(err => {
            if (err) {
              res.status(500).json({ "status": "error", "message": "Whoops! Something went wrong" });
            } else {
              res.status(200).json({ "status": "success", "message": "Final Data Validation Updated Success" });
            }
          });

        }
      });
    } catch (ex) {
      res.json({ status: "false", message: "Somethings went wrong" });
    }
  }

  /**
   * Get a Instance by ID and Data Type
   * @param {instanceID, instanceType, dataType} req 
   * @param {}
   */
  static async getDataByDataType(req, res) {
    const instanceID = req.query.instanceID;
    const dataType = req.query.dataType;

    if (dataType == "linguistic-data") {
      try {
        const data = await LinguisticData.findById(instanceID);

        res.json({ status: "success", data: data });
      } catch (ex) {
        res.json({ status: "false", message: "Somethings went wrong" });
      }
    } else if (dataType == "final-data") {
      try {
        const data = await FinalData.findById(instanceID);
        res.json({ status: "success", data: data });
      } catch (ex) {
        console.log(ex);
        res.json({ status: "false", message: "Somethings went wrong" });
      }
    } else {
      res.json({ status: "false", message: "Somethings went wrong" });
    }
  }

  /**
   * Update a Instance
   *
   * @param req
   * @param res
   */
  static async updateDataByDataType(req, res) {
    const instanceID = req.body.instanceID;
    const dataType = req.body.dataType;

    if (dataType == "linguistic-data") {
      try {
        const data = await LinguisticData.findById(req.body.id).lean();
        let id = data['_id'];
        delete data['_id'];

        if (data['data_type'] == 'sentence-sentiment') {
          if (req.body.skip) {
            data['answer'] = {
              "skip": true
            }
          } else if (req.body.subjectivity == 'objective') {
            data['answer'] = {
              "subjectivity": req.body.subjectivity
            };
          } else {
            data['answer'] = {
              "subjectivity": req.body.subjectivity,
              "polarity": req.body.polarity
            };
          }
          data['answer'] = {
            "skip": true
          }
        } else {
          data['answer'] = req.body.answer;
        }

        data.validation = true;
        data.linguistic_validator = req.decodedToken._id;

        await ReviewController.createFinalDataFromLinguisticData(data, id)

        res.json({ status: "success", message: "Linguistic Data Updated" });
      } catch (ex) {
        res.json({ status: "false", message: "Somethings went wrong" });
      }
    } else if (dataType == "final-data") {
      try {
        FinalData.findById({ "_id": req.body.id }, (err, finalData) => {
          if (err) {
            res.status(500).json({ "status": "error", "message": err });
          } else {
            if (finalData.data_type == 'sentence-sentiment') {
              if (req.body.skip) {
                finalData.answer = {
                  "skip": true
                }
              } else if (req.body.subjectivity == 'objective') {
                finalData.answer = {
                  "subjectivity": req.body.subjectivity
                };
              } else {
                finalData.answer = {
                  "subjectivity": req.body.subjectivity,
                  "polarity": req.body.polarity
                };
              }
            } else {
              finalData.answer = req.body.answer;
            }

            finalData.validation = true;
            finalData.linguistic_validator = req.decodedToken._id;

            finalData.save(err => {
              if (err) {
                res.status(500).json({ "status": "error", "message": "Whoops! Something went wrong" });
              } else {
                res.status(200).json({ "status": "success", "message": "Final Data Updated Success" });
              }
            });

          }
        });
      } catch (ex) {
        console.log(ex);
        res.json({ status: "false", message: "Somethings went wrong" });
      }
    } else {
      res.json({ status: "false", message: "Somethings went wrong" });
    }
  }

  /**
  * Update Data
  * 
  * @param {*} req 
  * @param {*} res 
  */
  static async createFinalDataFromLinguisticData(data, id) {
    let final_data = new FinalData(data);

    final_data.save((err, final_data) => {
      if (err) {
        console.log(err);
      } else {
        return ReviewController.deleteLinguisticData(id);
      }
    });
  }

  /**
  * Delete Data
  * 
  * @param {*} req 
  * @param {*} res 
  */
  static async deleteLinguisticData(id) {
    await LinguisticData.find({ _id: id }).remove().exec();
    return true;
  }
}
