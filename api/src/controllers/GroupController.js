import Group from "../models/Group.js";
import SentenceSentiment from "../models/SentenceSentiment.js";
import ParagraphSentiment from "../models/ParagraphSentiment.js";
import DocumentSentiment from "../models/DocumentSentiment.js";
import SentenceEmotion from "../models/SentenceEmotion.js";
import ParagraphEmotion from "../models/ParagraphEmotion.js";
import FinalData from "../models/FinalData.js";
import CalculatePerformanceController from "./CalculatePerformanceController.js";
import LinguisticData from "../models/LinguisticData.js";

export default class GroupController {
  /**
   * Create Group
   *
   * @param req
   * @param res
   */
  static create(req, res) {
    let group = new Group({
      name: req.body.name,
      description: req.body.description,
      group_type: req.body.group_type,
    });

    group.save((err, group) => {
      console.log(err);
      if (err) {
        res.status(500).json({ status: "error", message: err });
      } else {
        res.status(200).json({
          status: "success",
          message: "Group has been created",
        });
      }
    });
  }

  /**
   * get all groups list for loggedIn user
   *
   * @param req
   * @param res
   */
  static async currentUserGroups(req, res) {
    const groups = await Group.find({
      users: req.decodedToken._id,
      status: "active",
    });

    let instance_wise_groups = {
      "sentence_sentiment": [],
      "paragraph_sentiment": [],
      "document_sentiment": [],
      "sentence_emotion": [],
      "paragraph_emotion": []
    }

    for (let group of groups) {
      const sentence_sentiment = await LinguisticData.count({
        data_type: "sentence-sentiment",
        linguistic_group: group._id,
        linguistic_validator: req.decodedToken._id
      }).exec();

      const paragraph_sentiment = await LinguisticData.count({
        data_type: "paragraph-sentiment",
        linguistic_group: group._id,
        linguistic_validator: req.decodedToken._id
      }).exec();

      const document_sentiment = await LinguisticData.count({
        data_type: "document-sentiment",
        linguistic_group: group._id,
        linguistic_validator: req.decodedToken._id
      }).exec();

      const sentence_emotion = await LinguisticData.count({
        data_type: "sentence-emotion",
        linguistic_group: group._id,
        linguistic_validator: req.decodedToken._id
      }).exec();

      const paragraph_emotion = await LinguisticData.count({
        data_type: "paragraph-emotion",
        linguistic_group: group._id,
        linguistic_validator: req.decodedToken._id,
      }).exec();

      if (sentence_sentiment > 0) {
        instance_wise_groups.sentence_sentiment.push(group);
      }

      if (paragraph_sentiment > 0) {
        instance_wise_groups.paragraph_sentiment.push(group);
      }

      if (document_sentiment > 0) {
        instance_wise_groups.document_sentiment.push(group);
      }

      if (sentence_emotion > 0) {
        instance_wise_groups.sentence_emotion.push(group);
      }

      if (paragraph_emotion > 0) {
        instance_wise_groups.paragraph_emotion.push(group);
      }
    }

    res.json({ status: "success", groups: instance_wise_groups });
  }

  /**
   * Gel All Current Linguistic Completed Groups
   *
   * @param req
   * @param res
   */
  static async currentUserCompletedGroups(req, res) {
    const groups = await Group.find({
      users: req.decodedToken._id,
      status: "active",
    });

    let linguist_groups = [];

    for (let group of groups) {
      // const linguistic_data_count = await LinguisticData.count({
      //   linguistic_group: group._id,
      //   "linguistics.user_id": req.decodedToken._id,
      // }).exec();

      const final_data_count = await FinalData.count({
        linguistic_group: group._id,
        "linguistics.user_id": req.decodedToken._id,
      }).exec();

      // if (final_data_count > 0 && linguistic_data_count == 0) {
      if (final_data_count > 0) {
        linguist_groups.push(group);
      }
    }

    res.json({ status: "success", groups: linguist_groups });
  }


  /**
   * Gel All Completed Groups
   *
   * @param req
   * @param res
   */
  static async getAllCompletedGroups(req, res) {
    const groups = await Group.find({
      status: "active",
      group_type: "Annotator"
    });

    let completed_groups = [];

    for (let group of groups) {
      const final_data_count = await FinalData.exists({
        group: group._id
      });

      if (final_data_count) {
        completed_groups.push(group);
      }
    }

    res.json({ status: "success", groups: completed_groups });
  }

  /**
   * get all review data for loggedIn user, group wise and instanceType wise
   *
   * @param req
   * @param res
   */
  static async groupReviewData(req, res) {
    const userId = req.decodedToken._id;
    const groupID = req.query.groupID;
    const instanceType = req.query.instanceType;
    if (instanceType == "sentence-sentiment") {
      const data = await Group.find({
        users: req.decodedToken._id,
        status: "active",
      });
      res.json({ status: "success", data: data });
    }
  }

  /**
   * GET Group Details by ID
   *
   * @param req
   * @param res
   */
  static details(req, res) {
    try {
      const instanceType = req.query.instanceType;
      if (instanceType === "sentence-sentiment") {
        Promise.all([
          Group.findById(req.params.groupID).select(
            "name description status group_type created_at"
          ),

          //total skipped
          SentenceSentiment.count({
            group: req.params.groupID,
            "annotators.answer": { skip: true },
          }).exec(),
          // //assigned data
          SentenceSentiment.count({
            group: req.params.groupID,
          }).exec(),
          //annotated data
          SentenceSentiment.count({
            group: req.params.groupID,
            annotators: {
              $exists: true,
              $elemMatch: { answer: { $exists: false } },
            },
          }).exec(),
        ])
          .then(([group, skipped, total_instances, labeling_not_completed]) => {
            res.json({
              status: "success",
              group: group,
              group_details: {
                total_skipped: skipped,
                total_instances: total_instances,
                labeling_completed: total_instances - labeling_not_completed,
              },
            });
          })
          .catch((err) => {
            throw err; // res.sendStatus(500) might be better here.
          });
      } else if (instanceType === "sentence-emotion") {
        Promise.all([
          Group.findById(req.params.groupID).select(
            "name description status group_type created_at"
          ),
          //total skipped
          SentenceEmotion.count({
            group: req.params.groupID,
            "annotators.answer": { skip: true },
          }).exec(),
          // //assigned data
          SentenceEmotion.count({
            group: req.params.groupID,
          }).exec(),
          //annotated data
          SentenceEmotion.find({
            group: req.params.groupID,
            annotators: {
              $exists: true,
              $elemMatch: { answer: { $exists: false } },
            },
          }),
        ])
          .then(([group, skipped, total_instances, labeling_not_completed]) => {
            res.json({
              status: "success",
              group: group,
              group_details: {
                total_skipped: skipped,
                total_instances: total_instances,
                labeling_completed: total_instances - labeling_not_completed,
              },
            });
          })
          .catch((err) => {
            throw err; // res.sendStatus(500) might be better here.
          });
      } else if (instanceType === "paragraph-emotion") {
        Promise.all([
          Group.findById(req.params.groupID).select(
            "name description status group_type created_at"
          ),
          //total skipped
          ParagraphEmotion.count({
            group: req.params.groupID,
            "annotators.answer": { skip: true },
          }).exec(),
          // //assigned data
          ParagraphEmotion.count({
            group: req.params.groupID,
          }).exec(),
          //annotated data
          ParagraphEmotion.find({
            group: req.params.groupID,
            annotators: {
              $exists: true,
              $elemMatch: { answer: { $exists: false } },
            },
          }),
        ])
          .then(([group, skipped, total_instances, labeling_not_completed]) => {
            res.json({
              status: "success",
              group: group,
              group_details: {
                total_skipped: skipped,
                total_instances: total_instances,
                labeling_completed: total_instances - labeling_not_completed,
              },
            });
          })
          .catch((err) => {
            throw err; // res.sendStatus(500) might be better here.
          });
      } else if (instanceType === "paragraph-sentiment") {
        Promise.all([
          Group.findById(req.params.groupID).select(
            "name description status group_type created_at"
          ),
          //total skipped
          ParagraphSentiment.count({
            group: req.params.groupID,
            "annotators.answer": { skip: true },
          }).exec(),
          // //assigned data
          ParagraphSentiment.count({
            group: req.params.groupID,
          }).exec(),
          //annotated data
          ParagraphSentiment.find({
            group: req.params.groupID,
            annotators: {
              $exists: true,
              $elemMatch: { answer: { $exists: false } },
            },
          }),
        ])
          .then(([group, skipped, total_instances, labeling_not_completed]) => {
            res.json({
              status: "success",
              group: group,
              group_details: {
                total_skipped: skipped,
                total_instances: total_instances,
                labeling_completed: total_instances - labeling_not_completed,
              },
            });
          })
          .catch((err) => {
            throw err; // res.sendStatus(500) might be better here.
          });
      } else if (instanceType === "document-sentiment") {
        Promise.all([
          Group.findById(req.params.groupID).select(
            "name description status group_type created_at"
          ),
          //total skipped
          DocumentSentiment.count({
            group: req.params.groupID,
            "annotators.answer": { skip: true },
          }).exec(),
          // //assigned data
          DocumentSentiment.count({
            group: req.params.groupID,
          }).exec(),
          //annotated data
          DocumentSentiment.find({
            group: req.params.groupID,
            annotators: {
              $exists: true,
              $elemMatch: { answer: { $exists: false } },
            },
          }),
        ])
          .then(([group, skipped, total_instances, labeling_not_completed]) => {
            res.json({
              status: "success",
              group: group,
              group_details: {
                total_skipped: skipped,
                total_instances: total_instances,
                labeling_completed: total_instances - labeling_not_completed,
              },
            });
          })
          .catch((err) => {
            throw err; // res.sendStatus(500) might be better here.
          });
      } else {
        res.json({
          status: "success",
          group: [],
        });
      }
    } catch (error) {
      console.log(error);
      res
        .status(500)
        .json({ status: "error", message: "somethings went wrong." });
    }
  }

  /**
   * GET Group Annotator Status by groupID
   *
   * @param req
   * @param res
   */
  static async groupAnnotatorStatus(req, res) {
    const dataType = req.params.dataType;

    const group = await Group.findById(req.params.groupID)
      .select("name status group_type")
      .populate("users", "name email status");

    let user_stat = [];
    if (dataType == "sentence-sentiment") {
      if (group.users.length > 0) {
        for (const user of group.users) {
          if (group.group_type == "Annotator") {
            // Calculate Performance
            const annotator_performance_for_sentence_sentiment = await CalculatePerformanceController.groupAnnotatorPerformance(
              user._id,
              group._id,
              dataType
            );
            // Assigned data
            const assigned_sentence_sentiment = await SentenceSentiment.count({
              group: group._id,
              "annotators.user_id": user._id,
            }).exec();
            // Unannotated data
            const unannotated_sentence_sentiment = await SentenceSentiment.count({
              group: req.params.groupID,
              annotators: {
                $exists: true,
                $elemMatch: { answer: { $exists: false } },
              },
            }).exec();

            let annotated_data = assigned_sentence_sentiment - unannotated_sentence_sentiment;
            user_stat.push({
              user: user,
              assigned_data: assigned_sentence_sentiment,
              annotated_data: annotated_data,
              accuracy: annotator_performance_for_sentence_sentiment,
            });
          } else {
            const [
              assigned_sentence_sentiment,
              unannotated_sentence_sentiment,
            ] = await Promise.all([
              // Assigned data
              SentenceSentiment.count({
                linguistic_group: group._id,
                "linguistics.user_id": user._id,
              }).exec(),
              // Unannotated data
              SentenceSentiment.count({
                group: req.params.groupID,
                linguistics: {
                  $exists: true,
                  $elemMatch: { answer: { $exists: false } },
                },
              }).exec(),
            ]);

            let annotated_data = assigned_sentence_sentiment - unannotated_sentence_sentiment;

            user_stat.push({
              user: user,
              assigned_data: assigned_sentence_sentiment,
              annotated_data: annotated_data,
              accuracy: 0,
            });
          }
        }
      }

    } else if (dataType == "paragraph-sentiment") {
      if (group.users.length > 0) {
        for (const user of group.users) {
          if (group.group_type == "Annotator") {
            // Calculate Performance
            const annotator_performance_for_paragraph_sentiment = await CalculatePerformanceController.groupAnnotatorPerformance(
              user._id,
              group._id,
              dataType
            );
            // Assigned data
            const assigned_paragraph_sentiment = await ParagraphSentiment.count({
              group: group._id,
              "annotators.user_id": user._id,
            }).exec();

            // Unannotated data
            const unannotated_paragraph_sentiment = await ParagraphSentiment.count({
              group: req.params.groupID,
              annotators: {
                $exists: true,
                $elemMatch: { answer: { $exists: false } },
              },
            }).exec();

            let annotated_data = assigned_paragraph_sentiment - unannotated_paragraph_sentiment;
            user_stat.push({
              user: user,
              assigned_data: assigned_paragraph_sentiment,
              annotated_data: annotated_data,
              accuracy: annotator_performance_for_paragraph_sentiment,
            });
          } else {
            const [
              assigned_paragraph_sentiment,
              unannotated_paragraph_sentiment,
            ] = await Promise.all([
              // Assigned data
              ParagraphSentiment.count({
                linguistic_group: group._id,
                "linguistics.user_id": user._id,
              }).exec(),
              // Unannotated data
              ParagraphSentiment.count({
                group: req.params.groupID,
                linguistics: {
                  $exists: true,
                  $elemMatch: { answer: { $exists: false } },
                },
              }).exec(),
            ]);

            let annotated_data = assigned_paragraph_sentiment - unannotated_paragraph_sentiment;

            user_stat.push({
              user: user,
              assigned_data: assigned_paragraph_sentiment,
              annotated_data: annotated_data,
              accuracy: 0,
            });
          }
        }
      }

    } else if (dataType == "document-sentiment") {
      if (group.users.length > 0) {
        for (const user of group.users) {
          if (group.group_type == "Annotator") {
            // Calculate Performance
            const annotator_performance_for_document_sentiment = await CalculatePerformanceController.groupAnnotatorPerformance(
              user._id,
              group._id,
              dataType
            );

            // Assigned data
            const assigned_document_sentiment = await DocumentSentiment.count({
              group: group._id,
              "annotators.user_id": user._id,
            }).exec();

            // Unannotated data
            const unannotated_document_sentiment = await DocumentSentiment.count({
              group: req.params.groupID,
              annotators: {
                $exists: true,
                $elemMatch: { answer: { $exists: false } },
              },
            }).exec();

            let annotated_data = assigned_document_sentiment - unannotated_document_sentiment;
            user_stat.push({
              user: user,
              assigned_data: assigned_document_sentiment,
              annotated_data: annotated_data,
              accuracy: annotator_performance_for_document_sentiment,
            });
          } else {
            const [
              assigned_document_sentiment,
              unannotated_document_sentiment,
            ] = await Promise.all([
              // Assigned data
              DocumentSentiment.count({
                linguistic_group: group._id,
                "linguistics.user_id": user._id,
              }).exec(),
              // Unannotated data
              DocumentSentiment.count({
                group: req.params.groupID,
                linguistics: {
                  $exists: true,
                  $elemMatch: { answer: { $exists: false } },
                },
              }).exec(),
            ]);

            let annotated_data = assigned_document_sentiment - unannotated_document_sentiment;

            user_stat.push({
              user: user,
              assigned_data: assigned_document_sentiment,
              annotated_data: annotated_data,
              accuracy: 0,
            });
          }
        }
      }

    } else if (dataType == "sentence-emotion") {
      if (group.users.length > 0) {
        for (const user of group.users) {
          if (group.group_type == "Annotator") {
            //  Calculate Performance
            const annotator_performance_for_sentence_emotion = await CalculatePerformanceController.groupAnnotatorPerformance(
              user._id,
              group._id,
              dataType
            );

            // Assigned data
            const assigned_sentence_emotion = await SentenceEmotion.count({
              group: group._id,
              "annotators.user_id": user._id,
            }).exec();

            // Unannotated data
            const unannotated_sentence_emotion = await SentenceEmotion.count({
              group: req.params.groupID,
              annotators: {
                $exists: true,
                $elemMatch: { answer: { $exists: false } },
              },
            }).exec();

            let annotated_data = assigned_sentence_emotion - unannotated_sentence_emotion;
            user_stat.push({
              user: user,
              assigned_data: assigned_sentence_emotion,
              annotated_data: annotated_data,
              accuracy: annotator_performance_for_sentence_emotion,
            });
          } else {
            const [
              assigned_sentence_emotion,
              unannotated_sentence_emotion,
            ] = await Promise.all([
              // Assigned data
              SentenceEmotion.count({
                linguistic_group: group._id,
                "linguistics.user_id": user._id,
              }).exec(),
              // Unannotated data
              SentenceEmotion.count({
                group: req.params.groupID,
                linguistics: {
                  $exists: true,
                  $elemMatch: { answer: { $exists: false } },
                },
              }).exec(),
            ]);

            let annotated_data = assigned_sentence_emotion - unannotated_sentence_emotion;

            user_stat.push({
              user: user,
              assigned_data: assigned_sentence_emotion,
              annotated_data: annotated_data,
              accuracy: 0,
            });
          }
        }
      }

    } else if (dataType == "paragraph-emotion") {
      if (group.users.length > 0) {
        for (const user of group.users) {
          if (group.group_type == "Annotator") {
            // Calculate Performance
            const annotator_performance_for_paragraph_emotion = await CalculatePerformanceController.groupAnnotatorPerformance(
              user._id,
              group._id,
              dataType
            );
            // Assigned data
            const assigned_paragraph_emotion = await ParagraphEmotion.count({
              group: group._id,
              "annotators.user_id": user._id,
            }).exec();
            // Unannotated data
            const unannotated_paragraph_emotion = await ParagraphEmotion.count({
              group: req.params.groupID,
              annotators: {
                $exists: true,
                $elemMatch: { answer: { $exists: false } },
              },
            }).exec();

            let annotated_data = assigned_paragraph_emotion - unannotated_paragraph_emotion;
            user_stat.push({
              user: user,
              assigned_data: assigned_paragraph_emotion,
              annotated_data: annotated_data,
              accuracy: annotator_performance_for_paragraph_emotion,
            });
          } else {
            const [
              assigned_paragraph_emotion,
              unannotated_paragraph_emotion,
            ] = await Promise.all([
              // Assigned data
              ParagraphEmotion.count({
                linguistic_group: group._id,
                "linguistics.user_id": user._id,
              }).exec(),
              // Unannotated data
              ParagraphEmotion.count({
                group: req.params.groupID,
                linguistics: {
                  $exists: true,
                  $elemMatch: { answer: { $exists: false } },
                },
              }).exec(),
            ]);

            let annotated_data = assigned_paragraph_emotion - unannotated_paragraph_emotion;

            user_stat.push({
              user: user,
              assigned_data: assigned_paragraph_emotion,
              annotated_data: annotated_data,
              accuracy: 0,
            });
          }
        }
      }
    }

    res.json({ status: "success", annotator_status: user_stat });
  }

  /**
   * Get Group Labelled Data By groupID
   *
   * @param req
   * @param res
   */
  static async labelledData(req, res) {
    try {
      const group = await Group.findById(req.params.groupID)
        .select("name status group_type")
        .populate("users", "name email status");

      let labelled_data = [];
      let linguistics = [];

      if (group.group_type == "Annotator") {
        if (req.params.dataType == "sentence-sentiment") {
          const [sentence_sentiment] = await Promise.all([
            SentenceSentiment.find()
              .populate("linguistics.user_id", "name")
              .or([
                {
                  $and: [
                    { group: req.params.groupID },
                    { "annotators.answer": { $exists: true } },
                  ],
                },
                {
                  $and: [
                    { group: req.params.groupID },
                    { "linguistics.answer": { $exists: true } },
                  ],
                },
              ]),
          ]);

          for (let sentence of sentence_sentiment) {
            if (sentence.linguistics && linguistics.length == 0) {
              for (let linguistic of sentence.linguistics) {
                linguistics.push(linguistic.user_id);
              }
            }

            labelled_data.push({
              text: sentence.text,
              annotators: sentence.annotators,
              linguistics: sentence.linguistics,
            });
          }

          res.json({
            status: "success",
            data: labelled_data,
            users: group.users,
            linguistics: linguistics,
          });
        } else if (req.params.dataType == "paragraph-sentiment") {
          const [paragraph_sentiment] = await Promise.all([
            ParagraphSentiment.find()
              .populate("linguistics.user_id", "name")
              .or([
                {
                  $and: [
                    { group: req.params.groupID },
                    { "annotators.answer": { $exists: true } },
                  ],
                },
                {
                  $and: [
                    { group: req.params.groupID },
                    { "linguistics.answer": { $exists: true } },
                  ],
                },
              ]),
          ]);

          for (let sentence of paragraph_sentiment) {
            if (sentence.linguistics && linguistics.length == 0) {
              for (let linguistic of sentence.linguistics) {
                linguistics.push(linguistic.user_id);
              }
            }

            labelled_data.push({
              text: sentence.text,
              annotators: sentence.annotators,
              linguistics: sentence.linguistics,
            });
          }

          res.json({
            status: "success",
            data: labelled_data,
            users: group.users,
            linguistics: linguistics,
          });
        } else if (req.params.dataType == "document-sentiment") {
          const [document_sentiment] = await Promise.all([
            DocumentSentiment.find()
              .populate("linguistics.user_id", "name")
              .or([
                {
                  $and: [
                    { group: req.params.groupID },
                    { "annotators.answer": { $exists: true } },
                  ],
                },
                {
                  $and: [
                    { group: req.params.groupID },
                    { "linguistics.answer": { $exists: true } },
                  ],
                },
              ]),
          ]);

          for (let sentence of document_sentiment) {
            if (sentence.linguistics && linguistics.length == 0) {
              for (let linguistic of sentence.linguistics) {
                linguistics.push(linguistic.user_id);
              }
            }

            labelled_data.push({
              text: sentence.text,
              annotators: sentence.annotators,
              linguistics: sentence.linguistics,
            });
          }

          res.json({
            status: "success",
            data: labelled_data,
            users: group.users,
            linguistics: linguistics,
          });
        } else if (req.params.dataType == "sentence-emotion") {
          const [sentence_emotion] = await Promise.all([
            SentenceEmotion.find()
              .populate("linguistics.user_id", "name")
              .or([
                {
                  $and: [
                    { group: req.params.groupID },
                    { "annotators.answer": { $exists: true } },
                  ],
                },
                {
                  $and: [
                    { group: req.params.groupID },
                    { "linguistics.answer": { $exists: true } },
                  ],
                },
              ]),
          ]);

          for (let sentence of sentence_emotion) {
            if (sentence.linguistics && linguistics.length == 0) {
              for (let linguistic of sentence.linguistics) {
                linguistics.push(linguistic.user_id);
              }
            }

            labelled_data.push({
              text: sentence.text,
              annotators: sentence.annotators,
              linguistics: sentence.linguistics,
            });
          }

          res.json({
            status: "success",
            data: labelled_data,
            users: group.users,
            linguistics: linguistics,
          });
        } else if (req.params.dataType == "paragraph-emotion") {
          const [paragraph_emotion] = await Promise.all([
            ParagraphEmotion.find()
              .populate("linguistics.user_id", "name")
              .or([
                {
                  $and: [
                    { group: req.params.groupID },
                    { "annotators.answer": { $exists: true } },
                  ],
                },
                {
                  $and: [
                    { group: req.params.groupID },
                    { "linguistics.answer": { $exists: true } },
                  ],
                },
              ]),
          ]);

          for (let sentence of paragraph_emotion) {
            if (sentence.linguistics && linguistics.length == 0) {
              for (let linguistic of sentence.linguistics) {
                linguistics.push(linguistic.user_id);
              }
            }

            labelled_data.push({
              text: sentence.text,
              annotators: sentence.annotators,
              linguistics: sentence.linguistics,
            });
          }

          res.json({
            status: "success",
            data: labelled_data,
            users: group.users,
            linguistics: linguistics,
          });
        }
      } else {
        let annotators = [];
        if (req.params.dataType == "sentence-sentiment") {
          const [sentence_sentiment] = await Promise.all([
            SentenceSentiment.find()
              .populate("linguistics.user_id", "name")
              .populate("annotators.user_id", "name")
              .or([
                {
                  $and: [
                    { linguistic_group: req.params.groupID },
                    { "linguistics.answer": { $exists: true } },
                  ],
                },
              ]),
          ]);

          for (let sentence of sentence_sentiment) {
            if (sentence.linguistics && linguistics.length == 0) {
              for (let linguistic of sentence.linguistics) {
                linguistics.push(linguistic.user_id);
              }
            }

            if (sentence.annotators && annotators.length == 0) {
              for (let annotator of sentence.annotators) {
                annotators.push({
                  _id: annotator.user_id._id,
                  name: annotator.user_id.name,
                });
              }
            }

            let annotators_data = [];
            for (let annotator of sentence.annotators) {
              annotator.user_id = annotator.user_id._id;
              annotators_data.push(annotator);
            }

            labelled_data.push({
              text: sentence.text,
              annotators: annotators_data,
              linguistics: sentence.linguistics,
            });
          }

          res.json({
            status: "success",
            data: labelled_data,
            users: annotators,
            linguistics: linguistics,
          });
        } else if (req.params.dataType == "paragraph-sentiment") {
          const [paragraph_sentiment] = await Promise.all([
            ParagraphSentiment.find()
              .populate("linguistics.user_id", "name")
              .populate("annotators.user_id", "name")
              .or([
                {
                  $and: [
                    { linguistic_group: req.params.groupID },
                    { "linguistics.answer": { $exists: true } },
                  ],
                },
              ]),
          ]);

          for (let sentence of paragraph_sentiment) {
            if (sentence.linguistics && linguistics.length == 0) {
              for (let linguistic of sentence.linguistics) {
                linguistics.push(linguistic.user_id);
              }
            }

            if (sentence.annotators && annotators.length == 0) {
              for (let annotator of sentence.annotators) {
                annotators.push({
                  _id: annotator.user_id._id,
                  name: annotator.user_id.name,
                });
              }
            }

            let annotators_data = [];
            for (let annotator of sentence.annotators) {
              annotator.user_id = annotator.user_id._id;
              annotators_data.push(annotator);
            }

            labelled_data.push({
              text: sentence.text,
              annotators: annotators_data,
              linguistics: sentence.linguistics,
            });
          }

          res.json({
            status: "success",
            data: labelled_data,
            users: annotators,
            linguistics: linguistics,
          });
        } else if (req.params.dataType == "document-sentiment") {
          const [document_sentiment] = await Promise.all([
            DocumentSentiment.find()
              .populate("linguistics.user_id", "name")
              .populate("annotators.user_id", "name")
              .or([
                {
                  $and: [
                    { linguistic_group: req.params.groupID },
                    { "linguistics.answer": { $exists: true } },
                  ],
                },
              ]),
          ]);

          for (let sentence of document_sentiment) {
            if (sentence.linguistics && linguistics.length == 0) {
              for (let linguistic of sentence.linguistics) {
                linguistics.push(linguistic.user_id);
              }
            }

            if (sentence.annotators && annotators.length == 0) {
              for (let annotator of sentence.annotators) {
                annotators.push({
                  _id: annotator.user_id._id,
                  name: annotator.user_id.name,
                });
              }
            }

            let annotators_data = [];
            for (let annotator of sentence.annotators) {
              annotator.user_id = annotator.user_id._id;
              annotators_data.push(annotator);
            }

            labelled_data.push({
              text: sentence.text,
              annotators: annotators_data,
              linguistics: sentence.linguistics,
            });
          }

          res.json({
            status: "success",
            data: labelled_data,
            users: annotators,
            linguistics: linguistics,
          });
        } else if (req.params.dataType == "sentence-emotion") {
          const [sentence_emotion] = await Promise.all([
            SentenceEmotion.find()
              .populate("linguistics.user_id", "name")
              .populate("annotators.user_id", "name")
              .or([
                {
                  $and: [
                    { linguistic_group: req.params.groupID },
                    { "linguistics.answer": { $exists: true } },
                  ],
                },
              ]),
          ]);

          for (let sentence of sentence_emotion) {
            if (sentence.linguistics && linguistics.length == 0) {
              for (let linguistic of sentence.linguistics) {
                linguistics.push(linguistic.user_id);
              }
            }

            if (sentence.annotators && annotators.length == 0) {
              for (let annotator of sentence.annotators) {
                annotators.push({
                  _id: annotator.user_id._id,
                  name: annotator.user_id.name,
                });
              }
            }

            let annotators_data = [];
            for (let annotator of sentence.annotators) {
              annotator.user_id = annotator.user_id._id;
              annotators_data.push(annotator);
            }

            labelled_data.push({
              text: sentence.text,
              annotators: annotators_data,
              linguistics: sentence.linguistics,
            });
          }

          res.json({
            status: "success",
            data: labelled_data,
            users: annotators,
            linguistics: linguistics,
          });
        } else if (req.params.dataType == "paragraph-emotion") {
          const [paragraph_emotion] = await Promise.all([
            ParagraphEmotion.find()
              .populate("linguistics.user_id", "name")
              .populate("annotators.user_id", "name")
              .or([
                {
                  $and: [
                    { linguistic_group: req.params.groupID },
                    { "linguistics.answer": { $exists: true } },
                  ],
                },
              ]),
          ]);

          for (let sentence of paragraph_emotion) {
            if (sentence.linguistics && linguistics.length == 0) {
              for (let linguistic of sentence.linguistics) {
                linguistics.push(linguistic.user_id);
              }
            }

            if (sentence.annotators && annotators.length == 0) {
              for (let annotator of sentence.annotators) {
                annotators.push({
                  _id: annotator.user_id._id,
                  name: annotator.user_id.name,
                });
              }
            }

            let annotators_data = [];
            for (let annotator of sentence.annotators) {
              annotator.user_id = annotator.user_id._id;
              annotators_data.push(annotator);
            }

            labelled_data.push({
              text: sentence.text,
              annotators: annotators_data,
              linguistics: sentence.linguistics,
            });
          }

          res.json({
            status: "success",
            data: labelled_data,
            users: annotators,
            linguistics: linguistics,
          });
        }
      }
    } catch (error) {
      console.log(error);
      res
        .status(500)
        .json({ status: "error", message: "somethings went wrong." });
    }
  }

  /**
   * Group list with Users
   *
   * @param req
   * @param res
   */
  static list(req, res) {
    const builtQuery = { status: "active" };
    if ('groupType' in req.query) {
      builtQuery['group_type'] = req.query.groupType;
    }

    Group.find(builtQuery)
      .populate("users", "name email").exec((err, results) => {
        if (err) {
          res.status(500).json({ status: "error", message: err });
        } else {
          res.status(200).json({
            status: "success",
            groups: results,
          });
        }
      });
  }

  /**
   * Add User to the Group
   *
   * @param req
   * @param res
   */
  static retrive(req, res) {
    Group.findById({ _id: req.params.groupID })
      .populate("users", "name email")
      .exec((err, group) => {
        if (err) {
          res.status(500).json({ status: "error", message: err });
        } else {
          res.status(201).json({ status: "success", group: group });
        }
      });
  }

  /**
   * Add User to the Group
   *
   * @param req
   * @param res
   */
  static update(req, res) {
    Group.findById({ _id: req.params.groupID }, (err, group) => {
      if (err) {
        res.status(500).json({ status: "error", message: err });
      } else {
        group.name = req.body.name;
        group.description = req.body.description;

        group.save((err) => {
          if (err) {
            res.status(500).json({
              status: "error",
              message: "Whoops! Something went wrong",
            });
          } else {
            res.status(201).json({
              status: "success",
              message: "Group Updated successfully",
            });
          }
        });
      }
    });
  }

  /**
   * Soft Delete
   *
   * @param req
   * @param res
   */
  static delete(req, res) {
    Group.findById({ _id: req.params.groupID }, (err, group) => {
      if (err) {
        res.status(500).json({ status: "error", message: err });
      } else {
        group.status = "deleted";

        group.save((err) => {
          if (err) {
            res.status(500).json({
              status: "error",
              message: "Whoops! Something went wrong",
            });
          } else {
            res.status(201).json({
              status: "success",
              message: "Group updated successfully",
            });
          }
        });
      }
    });
  }

  /**
   * Assign Users To Group
   *
   * @param req
   * @param res
   */
  static assignUsersToGroup(req, res) {
    Group.findById({ _id: req.body.group_id }, (err, group) => {
      if (err) {
        res.status(500).json({ status: "error", message: err });
      } else {
        if (group.users.length == 0) {
          group.users = req.body.users;

          group.save((err) => {
            if (err) {
              res.status(500).json({
                status: "error",
                message: "Whoops! Something went wrong",
              });
            } else {
              res.status(201).json({
                status: "success",
                message: "Group updated successfully",
              });
            }
          });
        } else {
          res.status(500).json({
            status: "error",
            message: "Sorry, this group users aready assigned",
          });
        }
      }
    });
  }

  /**
   * Assign Task To Group
   *
   * @param req
   * @param res
   */
  static assignTasksToGroup(req, res) {
    const linguisticAssign = parseInt(req.body.no_of_instance, 10) * (req.body.data_validation_percentage / 100);
    
    if (req.body.instanceType == "sentence-sentiment") {
      SentenceSentiment.find({ group: undefined }, (err, sentences) => {
        if (err) {
          res.status(500).json({ status: "error", message: err });
        } else {
          sentences.forEach((sentence, key) => {
            sentence.group = req.body.group_id;
            sentence.linguistic_group = req.body.linguistic_group_id;
            sentence.annotators = req.body.annotators;

            if (key < linguisticAssign) {
              sentence.linguistics = req.body.linguistics;
            }

            sentence.save();
          });

          res.status(201).json({
            status: "success",
            message: "Total: " +sentences.length+ " Sentence Sentiment added to the Group.",
          });
        }
      }).limit(req.body.no_of_instance);
      
    } else if (req.body.instanceType == "paragraph-sentiment") {
      ParagraphSentiment.find({ group: undefined }, (err, paragraphs) => {
        if (err) {
          res.status(500).json({ status: "error", message: err });
        } else {
          paragraphs.forEach((paragraph, key) => {
            paragraph.group = req.body.group_id;
            paragraph.linguistic_group = req.body.linguistic_group_id;
            paragraph.annotators = req.body.annotators;

            if (key < linguisticAssign) {
              paragraph.linguistics = req.body.linguistics;
            }

            paragraph.save();
          });

          res.status(201).json({
            status: "success",
            message: "Total: " +paragraphs.length+ " Paragraph Sentiment added to the Group.",
          });
        }
      }).limit(req.body.no_of_instance);
    } else if (req.body.instanceType == "document-sentiment") {
      DocumentSentiment.find({ group: undefined }, (err, documents) => {
        if (err) {
          res.status(500).json({ status: "error", message: err });
        } else {
          documents.forEach((document, key) => {
            document.group = req.body.group_id;
            document.linguistic_group = req.body.linguistic_group_id;
            document.annotators = req.body.annotators;

            if (key < linguisticAssign) {
              document.linguistics = req.body.linguistics;
            }

            document.save();
          });

          res.status(201).json({
            status: "success",
            message: "Total: " +documents.length+ " Document Sentiment added to the Group.",
          });
        }
      }).limit(req.body.no_of_instance);
    } else if (req.body.instanceType == "sentence-emotion") {
      SentenceEmotion.find({ group: undefined }, (err, sentences) => {
        if (err) {
          res.status(500).json({ status: "error", message: err });
        } else {
          sentences.forEach((sentence, key) => {
            sentence.group = req.body.group_id;
            sentence.linguistic_group = req.body.linguistic_group_id;
            sentence.annotators = req.body.annotators;

            if (key < linguisticAssign) {
              sentence.linguistics = req.body.linguistics;
            }

            sentence.save();
          });

          res.status(201).json({
            status: "success",
            message: "Total: " +sentences.length+ " Sentence Emotion added to the Group.",
          });
        }
      }).limit(req.body.no_of_instance);
    } else if (req.body.instanceType == "paragraph-emotion") {
      ParagraphEmotion.find({ group: undefined }, (err, paragraphs) => {
        if (err) {
          res.status(500).json({ status: "error", message: err });
        } else {
          paragraphs.forEach((paragraph, key) => {
            paragraph.group = req.body.group_id;
            paragraph.linguistic_group = req.body.linguistic_group_id;
            paragraph.annotators = req.body.annotators;

            if (key < linguisticAssign) {
              paragraph.linguistics = req.body.linguistics;
            }

            paragraph.save();
          });

          res.status(201).json({
            status: "success",
            message: "Total: " +paragraphs.length+ " Paragraph Emotion added to the Group.",
          });
        }
      }).limit(req.body.no_of_instance);
    }
  }
}
