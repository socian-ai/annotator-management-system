import LinguisticData from "../models/LinguisticData.js";

export default class ReviewSentenceEmotionController {
  /**
   * ParagraphSentiment Submitted List
   *
   * @param req
   * @param res
   */
  static list(req, res) {
    LinguisticData.find(
      {
        // linguistics: {
        //   $elemMatch: {
        //     user_id: req.decodedToken._id,
        //     answer: { $exists: true, $not: { $size: 0 } },
        //   },
        // },
        data_type: "sentence-emotion",
      },
      (err, results) => {
        if (err) {
          res.status(500).json({ status: "error", message: err });
        } else {
          res.status(200).json({
            status: "success",
            submitted_list: results,
          });
        }
      }
    );
  }

  /**
   * Get a ParagraphSentiment by Group ID
   *
   * @param req
   * @param res
   *
   */
  static retrive(req, res) {
    LinguisticData.findOne(
      {
        // linguistics: {
        //   $elemMatch: { user_id: req.decodedToken._id, answer: undefined },
        // },
        data_type: "sentence-emotion",
      },
      (err, sentence_emotion) => {
        if (err) {
          res.status(500).json({ status: "error", message: err });
        } else {
          res.status(200).json({ status: "success", sentence_emotion: sentence_emotion });
        }
      }
    ).select("-linguistics -created_at -updated_at");
  }

  /**
   * Get a ParagraphSentiment by ID
   *
   * @param req
   * @param res
   *
   */
  static getSentenceByID(req, res) {
    LinguisticData.findById({ _id: req.params.id }, (err, document) => {
      if (err) {
        res.status(500).json({ status: "error", message: err });
      } else {
        if (document.linguistics.length > 0) {
          let is_user_exist = false;
          document.linguistics.forEach((user) => {
            if (user.user_id == req.decodedToken._id) {
              is_user_exist = true;
            }
          });

          if (is_user_exist) {
            res.status(200).json({ status: "success", document: document });
          } else {
            res.status(200).json({
              status: "failed",
              message: "Sorry you can't edit this document sentiment",
            });
          }
        } else {
          res.status(200).json({
            status: "failed",
            message: "Sorry you can't edit this document sentiment",
          });
        }
      }
    }).select("-created_at -updated_at");
  }

  /**
   * Update ParagraphSentiment Sentiment
   *
   * @param req
   * @param res
   */
  static update(req, res) {
    LinguisticData.findById({ _id: req.body.id }, (err, sentence) => {
      if (err) {
        res.status(500).json({ status: "error", message: err });
      } else {
        sentence.linguistics.forEach((linguistic) => {
          if (linguistic.user_id == req.decodedToken._id) {
            linguistic.answer = req.body.answer;
          }
        });

        sentence.save((err) => {
          if (err) {
            res.status(500).json({
              status: "error",
              message: "Whoops! Something went wrong",
            });
          } else {
            if (req.body.update) {
              res.status(200).json({
                status: "success",
                message: "Sentence Emotion Updated Success",
              });
            } else {
              ReviewSentenceEmotionController.retrive(req, res);
            }
          }
        });
      }
    });
  }
}
