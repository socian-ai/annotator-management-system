import SentenceSentiment from "../models/SentenceSentiment.js";
import ParagraphSentiment from "../models/ParagraphSentiment.js";
import DocumentSentiment from "../models/DocumentSentiment.js";
import SentenceEmotion from "../models/SentenceEmotion.js";
import ParagraphEmotion from "../models/ParagraphEmotion.js";
import UploadHistory from "../models/UploadHistory.js";
import FinalData from "../models/FinalData.js";

export default class AdminUploadController {

  /**
   * Data Upload
   *
   * @param req
   * @param res
   */
  static dataUpload(req, res) {
    const instanceType = req.body.instanceType;
    const upload_data = req.body.data;

    let upload_history = new UploadHistory({
      "data_type": instanceType,
      "uploaded_data": upload_data.length,
      "similar_data": upload_data.length,
      "stored_data": upload_data.length
    });

    upload_history.save((err, upload_history) => {
      if (err) {
        res.status(500).json({ "status": "error", "message": err });
      }
    });

    if (instanceType === "sentence-sentiment") {
      Promise.all([
        upload_data.forEach(dat => {
          if (dat['Text'] == undefined) return

          let sentence = new SentenceSentiment({
            "unannotated_id": dat['Unannotated Id'],
            "domain": dat['Domain'],
            "text": dat['Text'],
            "root_url": dat['Root Url'],
            "category": dat['Category'],
            "published_date": dat['Published Date'],
            "parse_time": dat['Parse Time']
          });

          sentence.save((err, sentence) => {
            if (err) {
              res.status(500).json({ "status": "error", "message": err });
            }
          });
        })
      ]).then((results) => {
        res.status(200).json({
          "status": "success",
          "data": upload_data,
          "message": "Uploaded Success"
        });
      })
        .catch((err) => {
          throw err; // res.sendStatus(500) might be better here.
        });
    } else if (instanceType === "paragraph-sentiment") {
      Promise.all([
        upload_data.forEach(dat => {
          if (dat['Text'] == undefined) return

          let paragraph = new ParagraphSentiment({
            "unannotated_id": dat['Unannotated Id'],
            "domain": dat['Domain'],
            "text": dat['Text'],
            "root_url": dat['Root Url'],
            "category": dat['Category'],
            "published_date": dat['Published Date'],
            "parse_time": dat['Parse Time']
          });

          paragraph.save((err, paragraph) => {
            if (err) {
              res.status(500).json({ "status": "error", "message": err });
            }
          });
        })
      ]).then((results) => {
        res.status(200).json({
          "status": "success",
          "data": upload_data,
          "message": "Uploaded Success"
        });
      })
        .catch((err) => {
          throw err; // res.sendStatus(500) might be better here.
        });
    } else if (instanceType === "document-sentiment") {
      Promise.all([
        upload_data.forEach(dat => {
          if (dat['Text'] == undefined) return

          let document = new DocumentSentiment({
            "unannotated_id": dat['Unannotated Id'],
            "domain": dat['Domain'],
            "text": dat['Text'],
            "root_url": dat['Root Url'],
            "category": dat['Category'],
            "published_date": dat['Published Date'],
            "parse_time": dat['Parse Time']
          });

          document.save((err, document) => {
            if (err) {
              res.status(500).json({ "status": "error", "message": err });
            }
          });
        })
      ]).then((results) => {
        res.status(200).json({
          "status": "success",
          "data": upload_data,
          "message": "Uploaded Success"
        });
      })
        .catch((err) => {
          throw err; // res.sendStatus(500) might be better here.
        });
    } else if (instanceType === "sentence-emotion") {
      Promise.all([
        upload_data.forEach(dat => {
          if (dat['Text'] == undefined) return

          let sentenceEmotion = new SentenceEmotion({
            "unannotated_id": dat['Unannotated Id'],
            "domain": dat['Domain'],
            "text": dat['Text'],
            "root_url": dat['Root Url'],
            "category": dat['Category'],
            "published_date": dat['Published Date'],
            "parse_time": dat['Parse Time']
          });

          sentenceEmotion.save((err, sentene) => {
            if (err) {
              res.status(500).json({ "status": "error", "message": err });
            }
          });
        })
      ]).then((results) => {
        res.status(200).json({
          "status": "success",
          "data": upload_data,
          "message": "Uploaded Success"
        });
      })
        .catch((err) => {
          throw err; // res.sendStatus(500) might be better here.
        });
    } else if (instanceType === "paragraph-emotion") {
      Promise.all([
        upload_data.forEach(dat => {
          if (dat['Text'] == undefined) return

          let paragraphEmotion = new ParagraphEmotion({
            "unannotated_id": dat['Unannotated Id'],
            "domain": dat['Domain'],
            "text": dat['Text'],
            "root_url": dat['Root Url'],
            "category": dat['Category'],
            "published_date": dat['Published Date'],
            "parse_time": dat['Parse Time']
          });

          paragraphEmotion.save((err, paragraphEmotion) => {
            if (err) {
              res.status(500).json({ "status": "error", "message": err });
            }
          });
        })
      ]).then((results) => {
        res.status(200).json({
          "status": "success",
          "data": upload_data,
          "message": "Uploaded Success"
        });
      })
        .catch((err) => {
          throw err; // res.sendStatus(500) might be better here.
        });
    }
  }

  /**
   * Data Upload History
   * @param req
   * @param res
   */

   static uploadHistory(req, res) {
    UploadHistory.find({}, null, {sort: '-created_at'}, (err, results) => {
      if (err) {
        res.status(500).json({ "status": "error", "message": err });
      } else {
        res.status(200).json({
          "status": "success",
          "upload_histories": results
        })
      }
    });
   }

  /**
   * Data Download
   * @param req
   * @param res
   */

  static download(req, res) {
    const dataType = req.params.dataType;
    
    FinalData.find({
      "data_type": dataType
    }, (err, results) => {
      if (err) {
        res.status(500).json({ "status": "error", "message": err });
      } else {
        let data = [];

        for(let result of results) {
          data.push({
            UnannotatedID: result.unannotated_id,
            Text: result.text,
            Answer: JSON.stringify(result.answer),
            Domain: result.domain,
            Domain: result.domain,
            Validation: result.validation,
            Created_at: new Date(parseInt(result.created_at)).toLocaleDateString()
          })
        }

        // if(dataType == 'sentence-sentiment'){
        // } else if(dataType == 'paragraph-sentiment'){
        // }

        res.status(200).json({
          "status": "success",
          "data": data
        })
      }
    });
   }
}