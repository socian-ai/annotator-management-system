import ParagraphEmotion from "../models/ParagraphEmotion.js";

export default class LinguisticParagraphEmotionController {
    /**
     * ParagraphEmotion Submitted List
     *
     * @param req
     * @param res
     */
    static list(req, res) {
        ParagraphEmotion.find({
                linguistics: {
                    $elemMatch: {
                        user_id: req.decodedToken._id,
                        answer: { $exists: true, $not: { $size: 0 } },
                    },
                },
            },
            null, { sort: { created_at: -1 } },
            (err, results) => {
                if (err) {
                    res.status(500).json({ status: "error", message: err });
                } else {
                    res.status(200).json({
                        status: "success",
                        submitted_list: results,
                    });
                }
            }
        );
    }

    /**
     * Get a ParagraphEmotion by Group ID
     *
     * @param req
     * @param res
     *
     */
    static retrive(req, res) {
        ParagraphEmotion.findOne({
                linguistics: {
                    $elemMatch: { user_id: req.decodedToken._id, answer: undefined },
                },
            },
            (err, paragraphEmotion) => {
                if (err) {
                    res.status(500).json({ status: "error", message: err });
                } else {
                    res
                        .status(200)
                        .json({ status: "success", paragraph_emotion: paragraphEmotion });
                }
            }
        ).select("-linguistics -created_at -updated_at");
    }

    /**
     * Get a paragraphEmotion by ID
     *
     * @param req
     * @param res
     *
     */
    static getParagraphByID(req, res) {
        ParagraphEmotion.findById({ _id: req.params.id },
            (err, paragraphEmotion) => {
                if (err) {
                    res.status(500).json({ status: "error", message: err });
                } else {
                    if (paragraphEmotion.linguistics.length > 0) {
                        let is_user_exist = false;
                        paragraphEmotion.linguistics.forEach((user) => {
                            if (user.user_id == req.decodedToken._id) {
                                is_user_exist = true;
                            }
                        });

                        if (is_user_exist) {
                            res.status(200).json({
                                status: "success",
                                paragraph_emotion: paragraphEmotion,
                            });
                        } else {
                            res.status(200).json({
                                status: "failed",
                                message: "Sorry you can't edit this paragraph emotion",
                            });
                        }
                    } else {
                        res.status(200).json({
                            status: "failed",
                            message: "Sorry you can't edit this paragraph emotion",
                        });
                    }
                }
            }
        ).select("-created_at -updated_at");
    }

    /**
     * Update ParagraphEmotion
     *
     * @param req
     * @param res
     */
    static update(req, res) {
        ParagraphEmotion.findById({ _id: req.body.id }, (err, paragraphEmotion) => {
            if (err) {
                res.status(500).json({ status: "error", message: err });
            } else {
                paragraphEmotion.linguistics.forEach((linguistic) => {
                    if (linguistic.user_id == req.decodedToken._id) {
                        linguistic.answer = req.body.answer;
                    }
                });

                paragraphEmotion.save((err) => {
                    if (err) {
                        res.status(500).json({
                            status: "error",
                            message: "Whoops! Something went wrong",
                        });
                    } else {
                        if (req.body.update) {
                            res.status(200).json({
                                status: "success",
                                message: "Paragraph Emotion Updated Success",
                            });
                        } else {
                            LinguisticParagraphEmotionController.retrive(req, res);
                        }
                    }
                });
            }
        });
    }
}