import User from "../models/User.js";
import helper from "../helpers/index.js";
import template from "../email_template/index.js";
import AuthController from "./AuthController.js";

export default class RegistrationController {

    /**
     * Validation for Registration.
     *
     * @param req
     */

    /**
     * User Registration.
     *
     * @param req
     * @param res
     */
    static register(req, res) {
        AuthController.userValidation(req);

        req.getValidationResult().then(errors => {
            if (!errors.isEmpty()) {
                res.status(422).json({ "status": "validationError", "errors": errors.array() });
            } else {
                AuthController.verifyRecaptcha(req, (isVerified) => {
                    if (isVerified) {
                        let token = (Math.random().toString(36) + (new Date()).valueOf().toString()).substr(2, 35);
                        // Save the user
                        let user = new User({
                            "name": req.body.name,
                            "email": req.body.email,
                            "phone": req.body.phone,
                            "password": req.body.password,
                            "age": req.body.age,
                            "verify_token": token,
                            "gender": req.body.gender,
                            "profession": req.body.profession,
                            "type": req.body.type
                        });

                        if (req.body.type != "admin") {
                            user.save(err => {
                                if (err) {
                                    res.status(500).json({ "status": "error", "message": err });
                                } else {
                                    helper.mail(req.body.email, "Email Verification", template.account_verify(token));

                                    res.status(200).json({
                                        "status": "success",
                                        "message": "Registration Success"
                                    })
                                }
                            });
                        } else {
                            res.status(200).json({
                                "status": "failed",
                                "message": "You have no permission"
                            })
                        }
                    } else {
                        return res.status(401).json({ "status": "error", "message": "Invalid request" });
                    }
                });
            }
        });
    }

    /**
     * Verify Account
     *
     * @requires "verify_token"
     *
     * @param req
     * @param res
     */
    static verifyAccount(req, res) {

        if (req.params.token == null || req.params.token == "") {
            return res.status(404).json({
                "status": "error",
                "message": "Invalid Request"
            });
        } else {
            User.findOne({ verify_token: req.params.token }, (err, user) => {

                if (err || !user) {
                    return res.status(404).json({
                        "status": "error",
                        "message": "Invalid Request"
                    });
                }

                if (user.status == 'unverified') {
                    user.status = 'verified'

                    user.save((err, user) => {
                        if (err) {
                            res.status(500).json({ "status": "error", "message": err });
                        } else {
                            res.status(200).json({
                                "status": "success",
                                "message": "Your Email Adderess is successfully verified! Please login to access you account!"
                            });
                        }
                    });
                } else {
                    res.status(200).json({
                        "status": "success",
                        "message": "Your E-mail is already verified."
                    });
                }
            });
        }
    }
}