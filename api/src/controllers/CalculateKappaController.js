import SentenceSentiment from "../models/SentenceSentiment.js";
import ParagraphSentiment from "../models/ParagraphSentiment.js";
import DocumentSentiment from "../models/DocumentSentiment.js";
import SentenceEmotion from "../models/SentenceEmotion.js";
import ParagraphEmotion from "../models/ParagraphEmotion.js";
import LinguisticData from "../models/LinguisticData.js";
import FinalData from "../models/FinalData.js";
import request from 'request';

export default class CalculateKappaController {

    /**
     * Calculate Kappa
     * 
     * @param {*} req 
     * @param {*} res 
     */

    static async calculateKappa(req, res) {
        await request({
            uri: "http://flask-api:8000/apply-fleiss-kappa",
            // uri: "http://localhost:8000/apply-fleiss-kappa",
            method: "POST",
            json: {
                dataType: req.params.dataType,
                group_id: req.params.groupID
            }
        }, (error, response, body) => {
            console.log(body);
            res.json(body);
        });
    }
}