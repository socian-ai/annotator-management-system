import SentenceSentiment from "../models/SentenceSentiment.js";
import ParagraphSentiment from "../models/ParagraphSentiment.js";
import DocumentSentiment from "../models/DocumentSentiment.js";
import SentenceEmotion from "../models/SentenceEmotion.js";
import ParagraphEmotion from "../models/ParagraphEmotion.js";
import User from "../models/User.js";
import Group from "../models/Group.js";
import AuthController from "./AuthController.js";
import CalculatePerformanceController from "./CalculatePerformanceController.js";

export default class UserController {
    /**
     * Create User
     *
     * @param req
     * @param res
     */
    static create(req, res) {
        AuthController.userValidation(req);

        req.getValidationResult().then((errors) => {
            if (!errors.isEmpty()) {
                res
                    .status(422)
                    .json({ status: "validationError", errors: errors.array() });
            } else {
                // Create User
                let user = new User();

                user.name = req.body.name;
                user.email = req.body.email;
                user.phone = req.body.phone;

                if (req.body.password && req.body.password.length == 6) {
                    user.password = req.body.password;
                }

                user.age = req.body.age;
                user.gender = req.body.gender;
                user.profession = req.body.profession;
                user.type = req.body.type;
                user.status = "verified";

                user.save((err) => {
                    if (err) {
                        res.status(500).json({ status: "error", message: err });
                    } else {
                        res.status(200).json({
                            status: "success",
                            message: "User has been created",
                        });
                    }
                });
            }
        });
    }

    /**
     * User list
     *
     * @param req
     * @param res
     */
    static async list(req, res) {
        if (req.params.userType == 'all-users') {
            User.find({ $or: [{ status: "active" }, { status: "verified" }] },
                (err, results) => {
                    if (err) {
                        res.status(500).json({ status: "error", message: err });
                    } else {
                        res.status(200).json({
                            status: "success",
                            users: results,
                        });
                    }
                }
            ).select("-password");
        } else if (req.params.userType == 'all-annotators') {
            User.find({ $or: [{ status: "active" }, { status: "verified" }], type: "annotator" },
                (err, results) => {
                    if (err) {
                        res.status(500).json({ status: "error", message: err });
                    } else {
                        res.status(200).json({
                            status: "success",
                            users: results,
                        });
                    }
                }
            ).select("-password");
        } else if (req.params.userType == 'all-linguistics') {
            User.find({ $or: [{ status: "active" }, { status: "verified" }], type: "linguistic" },
                (err, results) => {
                    if (err) {
                        res.status(500).json({ status: "error", message: err });
                    } else {
                        res.status(200).json({
                            status: "success",
                            users: results,
                        });
                    }
                }
            ).select("-password");
        } else if (req.params.userType == 'assigned-annotators') {
            const results = await User.find({ $or: [{ status: "active" }, { status: "verified" }], type: "annotator" },).select("-password");

            let users = [];
            for (let user of results) {
                const group = await Group.find({
                    users: user._id,
                    group_type: "Annotator",
                    status: "active"
                });

                if (group.length) {
                    users.push(user);
                }
            }

            res.status(200).json({
                status: "success",
                users: users
            });
        } else if (req.params.userType == 'free-annotators') {

            const results = await User.find({ $or: [{ status: "active" }, { status: "verified" }], type: "annotator" },).select("-password");

            let users = [];
            for (let user of results) {
                const groups = await Group.find({
                    users: user._id,
                    group_type: "Annotator",
                    status: "active"
                });

                if (groups.length) {
                    let instance_count = 0;
                    for (let group of groups) {
                        instance_count += await SentenceSentiment.count({
                            group: group._id
                        }).exec();

                        instance_count += await ParagraphSentiment.count({
                            group: group._id
                        }).exec();

                        instance_count += await DocumentSentiment.count({
                            group: group._id
                        }).exec();

                        instance_count += await SentenceEmotion.count({
                            group: group._id
                        }).exec();

                        instance_count += await ParagraphEmotion.count({
                            group: group._id
                        }).exec();
                    }

                    if (instance_count == 0) {
                        users.push(user);
                    }
                } else {
                    users.push(user);
                }
            }

            res.status(200).json({
                status: "success",
                users: users
            });
        } else if (req.params.userType == 'assigned-linguistics') {
            const results = await User.find({ $or: [{ status: "active" }, { status: "verified" }], type: "linguistic" },).select("-password");

            let users = [];
            for (let user of results) {
                const group = await Group.find({
                    users: user._id,
                    group_type: "Linguistic",
                    status: "active"
                });

                if (group.length) {
                    users.push(user);
                }
            }

            res.status(200).json({
                status: "success",
                users: users
            });
        } else if (req.params.userType == 'free-linguistics') {
            const results = await User.find({ $or: [{ status: "active" }, { status: "verified" }], type: "linguistic" },).select("-password");

            let users = [];
            for (let user of results) {
                const groups = await Group.find({
                    users: user._id,
                    group_type: "Linguistic",
                    status: "active"
                });

                if (groups.length) {
                    let instance_count = 0;
                    for (let group of groups) {
                        instance_count += await SentenceSentiment.count({
                            linguistic_group: group._id
                        }).exec();

                        instance_count += await ParagraphSentiment.count({
                            linguistic_group: group._id
                        }).exec();

                        instance_count += await DocumentSentiment.count({
                            linguistic_group: group._id
                        }).exec();

                        instance_count += await SentenceEmotion.count({
                            linguistic_group: group._id
                        }).exec();

                        instance_count += await ParagraphEmotion.count({
                            linguistic_group: group._id
                        }).exec();
                    }

                    if (instance_count == 0) {
                        users.push(user);
                    }
                } else {
                    users.push(user);
                }
            }

            res.status(200).json({
                status: "success",
                users: users
            });
        } else {
            res.status(500).json({ status: "error", message: "Invalid Request" });
        }
    }

    /**
     * Get User By ID
     *
     * @param req
     * @param res
     */
    static retrive(req, res) {
        User.findById({ _id: req.params.userID }, (err, user) => {
            if (err) {
                res.status(500).json({ status: "error", message: err });
            } else {
                res.status(200).json({ status: "success", user: user });
            }
        }).select("-password -created_at -updated_at -is_active");
    }

    /**
     * Get User Details By ID
     *
     * @param req
     * @param res
     */
    static details(req, res) {
        const userID = req.params.userID;
        const dataType = req.query.dataType;
        try {
            if (dataType == 'sentence-sentiment') {
                Promise.all([
                    User.findById(userID).select(
                        "-password -created_at -updated_at -is_active -reset_token -verify_token"
                    ),
                    // Total Working group
                    Group.count({ users: userID }).exec(),
                    // Total skipped data
                    SentenceSentiment.count({
                        annotators: {
                            $elemMatch: {
                                user_id: userID,
                                answer: { skip: true },
                            },
                        },
                    }).exec(),
                    // Assigned data
                    SentenceSentiment.count({
                        "annotators.user_id": userID,
                    }).exec(),
                    // Annotated data
                    SentenceSentiment.count({
                        annotators: {
                            $elemMatch: {
                                user_id: userID,
                                answer: { $exists: true },
                            },
                        },
                    }).exec(),
                    // Linguistic 5 % data
                    SentenceSentiment.find().and([
                        {
                            "linguistics": {
                                $elemMatch: {
                                    answer: { $exists: true },
                                }
                            }
                        },
                        {
                            annotators: {
                                $elemMatch: {
                                    user_id: userID,
                                    answer: { $exists: true }
                                }
                            }
                        }
                    ]).exec(),
                ]).then(
                    ([
                        user_info,
                        group_info,
                        skiped_data,
                        assigned_data,
                        annotated_data,
                        linguistic_data
                    ]) => {
                        res.json({
                            status: "success",
                            details: {
                                user: user_info,
                                total_group: group_info,
                                skipped_data: skiped_data,
                                assigned_data: assigned_data,
                                annotated_data: annotated_data,
                                accuracy: CalculatePerformanceController.senteneSentimentPerformance(
                                    linguistic_data,
                                    userID
                                )
                            },
                        });
                    }
                ).catch((err) => {
                    throw err; // res.sendStatus(500) might be better here.
                });
            } else if (dataType == 'paragraph-sentiment') {
                Promise.all([
                    User.findById(userID).select(
                        "-password -created_at -updated_at -is_active -reset_token -verify_token"
                    ),
                    // Total Working group
                    Group.count({ users: userID }).exec(),
                    // Total skipped data
                    ParagraphSentiment.count({
                        annotators: {
                            $elemMatch: {
                                user_id: userID,
                                answer: { skip: true },
                            },
                        },
                    }).exec(),
                    // Assigned data
                    ParagraphSentiment.count({
                        "annotators.user_id": userID,
                    }).exec(),
                    // Annotated data
                    ParagraphSentiment.count({
                        annotators: {
                            $elemMatch: {
                                user_id: userID,
                                answer: { $exists: true },
                            },
                        },
                    }).exec(),
                    // Linguistic 5 % data
                    ParagraphSentiment.find().and([
                        {
                            "linguistics": {
                                $elemMatch: {
                                    answer: { $exists: true },
                                }
                            }
                        },
                        {
                            annotators: {
                                $elemMatch: {
                                    user_id: userID,
                                    answer: { $exists: true }
                                }
                            }
                        }
                    ]).exec(),
                ]).then(
                    ([
                        user_info,
                        group_info,
                        skiped_data,
                        assigned_data,
                        annotated_data,
                        linguistic_data
                    ]) => {
                        res.json({
                            status: "success",
                            details: {
                                user: user_info,
                                total_group: group_info,
                                skipped_data: skiped_data,
                                assigned_data: assigned_data,
                                annotated_data: annotated_data,
                                accuracy: CalculatePerformanceController.paragraphSentimentPerformance(
                                    linguistic_data,
                                    userID
                                )
                            },
                        });
                    }
                ).catch((err) => {
                    throw err; // res.sendStatus(500) might be better here.
                });
            } else if (dataType == 'document-sentiment') {
                Promise.all([
                    User.findById(userID).select(
                        "-password -created_at -updated_at -is_active -reset_token -verify_token"
                    ),
                    // Total Working group
                    Group.count({ users: userID }).exec(),
                    // Total skipped data
                    DocumentSentiment.count({
                        annotators: {
                            $elemMatch: {
                                user_id: userID,
                                answer: { skip: true },
                            },
                        },
                    }).exec(),
                    // Assigned data
                    DocumentSentiment.count({
                        "annotators.user_id": userID,
                    }).exec(),
                    // Annotated data
                    DocumentSentiment.count({
                        annotators: {
                            $elemMatch: {
                                user_id: userID,
                                answer: { $exists: true },
                            },
                        },
                    }).exec(),
                    // Linguistic 5 % data
                    DocumentSentiment.find().and([
                        {
                            "linguistics": {
                                $elemMatch: {
                                    answer: { $exists: true },
                                }
                            }
                        },
                        {
                            annotators: {
                                $elemMatch: {
                                    user_id: userID,
                                    answer: { $exists: true }
                                }
                            }
                        }
                    ]).exec(),
                ]).then(
                    ([
                        user_info,
                        group_info,
                        skiped_data,
                        assigned_data,
                        annotated_data,
                        linguistic_data
                    ]) => {
                        res.json({
                            status: "success",
                            details: {
                                user: user_info,
                                total_group: group_info,
                                skipped_data: skiped_data,
                                assigned_data: assigned_data,
                                annotated_data: annotated_data,
                                accuracy: CalculatePerformanceController.documentSentimentPerformance(
                                    linguistic_data,
                                    userID
                                )
                            },
                        });
                    }
                ).catch((err) => {
                    throw err; // res.sendStatus(500) might be better here.
                });
            } else if (dataType == 'sentence-emotion') {
                Promise.all([
                    User.findById(userID).select(
                        "-password -created_at -updated_at -is_active -reset_token -verify_token"
                    ),
                    // Total Working group
                    Group.count({ users: userID }).exec(),
                    // Total skipped data
                    SentenceEmotion.count({
                        annotators: {
                            $elemMatch: {
                                user_id: userID,
                                answer: { skip: true },
                            },
                        },
                    }).exec(),
                    // Assigned data
                    SentenceEmotion.count({
                        "annotators.user_id": userID,
                    }).exec(),
                    // Annotated data
                    SentenceEmotion.count({
                        annotators: {
                            $elemMatch: {
                                user_id: userID,
                                answer: { $exists: true },
                            },
                        },
                    }).exec(),
                    // Linguistic 5 % data
                    SentenceEmotion.find().and([
                        {
                            "linguistics": {
                                $elemMatch: {
                                    answer: { $exists: true },
                                }
                            }
                        },
                        {
                            annotators: {
                                $elemMatch: {
                                    user_id: userID,
                                    answer: { $exists: true }
                                }
                            }
                        }
                    ]).exec(),
                ]).then(
                    ([
                        user_info,
                        group_info,
                        skiped_data,
                        assigned_data,
                        annotated_data,
                        linguistic_data
                    ]) => {
                        res.json({
                            status: "success",
                            details: {
                                user: user_info,
                                total_group: group_info,
                                skipped_data: skiped_data,
                                assigned_data: assigned_data,
                                annotated_data: annotated_data,
                                accuracy: CalculatePerformanceController.sentenceEmotionPerformance(
                                    linguistic_data,
                                    userID
                                )
                            },
                        });
                    }
                ).catch((err) => {
                    throw err; // res.sendStatus(500) might be better here.
                });
            } else if (dataType == 'paragraph-emotion') {
                Promise.all([
                    User.findById(userID).select(
                        "-password -created_at -updated_at -is_active -reset_token -verify_token"
                    ),
                    // Total Working group
                    Group.count({ users: userID }).exec(),
                    // Total skipped data
                    ParagraphEmotion.count({
                        annotators: {
                            $elemMatch: {
                                user_id: userID,
                                answer: { skip: true },
                            },
                        },
                    }).exec(),
                    // Assigned data
                    ParagraphEmotion.count({
                        "annotators.user_id": userID,
                    }).exec(),
                    // Annotated data
                    ParagraphEmotion.count({
                        annotators: {
                            $elemMatch: {
                                user_id: userID,
                                answer: { $exists: true },
                            },
                        },
                    }).exec(),
                    // Linguistic 5 % data
                    ParagraphEmotion.find().and([
                        {
                            "linguistics": {
                                $elemMatch: {
                                    answer: { $exists: true },
                                }
                            }
                        },
                        {
                            annotators: {
                                $elemMatch: {
                                    user_id: userID,
                                    answer: { $exists: true }
                                }
                            }
                        }
                    ]).exec(),
                ]).then(
                    ([
                        user_info,
                        group_info,
                        skiped_data,
                        assigned_data,
                        annotated_data,
                        linguistic_data
                    ]) => {
                        res.json({
                            status: "success",
                            details: {
                                user: user_info,
                                total_group: group_info,
                                skipped_data: skiped_data,
                                assigned_data: assigned_data,
                                annotated_data: annotated_data,
                                accuracy: CalculatePerformanceController.paragraphEmotionPerformance(
                                    linguistic_data,
                                    userID
                                )
                            },
                        });
                    }
                ).catch((err) => {
                    throw err; // res.sendStatus(500) might be better here.
                });
            }
        } catch (error) {
            console.log(error);
            res.status(500)
                .json({ status: "error", message: "somethings went wrong." });
        }
    }

    /**
     * Get all group type Users Without the Group Users
     * 
     * @param {groupID} req 
     * @param {*} res 
     */
    static async getUserList(req, res) {
        const group = await Group.findById(req.params.groupID);
        const users = await User.find({
            status: "active",
            type: group.group_type.toLowerCase()
        });

        var users_list = [];
        for (let user of users) {
            if (!group.users.includes(user._id)) {
                users_list.push(user);
            }
        }

        res.json({
            status: "success",
            user: users_list,
        });
    }

    /**
     * Deactive User
     *
     * @param req
     * @param res
     */
    static async removeUseToGroup(req, res) {
        try {
            for (let user_id of req.body.users) {
                const group = await Group.findById(req.body.group_id);
                group.users = group.users.filter(user => user != user_id);
                await group.save();

                const sentence_sentiment = await SentenceSentiment.find({ group: req.body.group_id });
                if (sentence_sentiment.length) {
                    for (let sentence of sentence_sentiment) {
                        sentence.annotators = sentence.annotators.filter(annotator => annotator.user_id != user_id);
                        await sentence.save();
                    }
                }

                const paragraph_sentiment = await ParagraphSentiment.find({ group: req.body.group_id });
                if (paragraph_sentiment.length) {
                    for (let paragraph of paragraph_sentiment) {
                        paragraph.annotators = paragraph.annotators.filter(annotator => annotator.user_id != user_id);
                        await paragraph.save();
                    }
                }

                const sentence_emotion = await SentenceEmotion.find({ group: req.body.group_id });
                if (sentence_emotion.length) {
                    for (let sentence of sentence_emotion) {
                        sentence.annotators = sentence.annotators.filter(annotator => annotator.user_id != user_id);
                        await sentence.save();
                    }
                }

                const paragraph_emotion = await ParagraphEmotion.find({ group: req.body.group_id });
                if (paragraph_emotion.length) {
                    for (let paragraph of paragraph_emotion) {
                        paragraph.annotators = paragraph.annotators.filter(annotator => annotator.user_id != user_id);
                        await paragraph.save();
                    }
                }

                const document_sentiment = await DocumentSentiment.find({ group: req.body.group_id });
                if (document_sentiment.length) {
                    for (let document of document_sentiment) {
                        document.annotators = document.annotators.filter(annotator => annotator.user_id != user_id);
                        await document.save();
                    }
                }
            }

            res.status(200).json({
                status: "success",
                group: "User deactived successfully",
            });
        } catch (ex) {
            console.log(ex);
            res.status(500).json({
                status: "error",
                message: "Whoops! Something went wrong",
            });
        }
    }

    static async addUserToGroup(req, res) {
        try {
            for (let user_id of req.body.users) {
                const group = await Group.findById(req.body.group_id);
                if (group) {
                    group.users.push(user_id);
                    await group.save();
                }

                const sentence_sentiment = await SentenceSentiment.find({ group: req.body.group_id });
                if (sentence_sentiment.length) {
                    for (let sentence of sentence_sentiment) {
                        sentence.annotators.push({ user_id: user_id });
                        await sentence.save();
                    }
                }

                const paragraph_sentiment = await ParagraphSentiment.find({ group: req.body.group_id });
                if (paragraph_sentiment.length) {
                    for (let paragraph of paragraph_sentiment) {
                        paragraph.annotators.push({ user_id: user_id });
                        await paragraph.save();
                    }
                }

                const sentence_emotion = await SentenceEmotion.find({ group: req.body.group_id });
                if (sentence_emotion.length) {
                    for (let sentence of sentence_emotion) {
                        sentence.annotators.push({ user_id: user_id });
                        await sentence.save();
                    }
                }

                const paragraph_emotion = await ParagraphEmotion.find({ group: req.body.group_id });
                if (paragraph_emotion.length) {
                    for (let paragraph of paragraph_emotion) {
                        paragraph.annotators.push({ user_id: user_id });
                        await paragraph.save();
                    }
                }

                const document_sentiment = await DocumentSentiment.find({ group: req.body.group_id });
                if (document_sentiment.length) {
                    for (let document of document_sentiment) {
                        document.annotators.push({ user_id: user_id });
                        await document.save();
                    }
                }
            }

            res.status(201).json({
                status: "success",
                message: "User added successfully.",
            });
        } catch (ex) {
            console.log(ex);
            res.status(500).json({
                status: "error",
                message: "Whoops! Something went wrong",
            });
        }
    }

    /**
     * Update User
     *
     * @param req
     * @param res
     */
    static update(req, res) {
        User.findById({ _id: req.params.userID }, (err, user) => {
            if (err) {
                res.status(500).json({ status: "error", message: err });
            } else {
                if (req.body.hasOwnProperty("status")) {
                    user.status = "active";
                } else {
                    user.name = req.body.name;
                    user.phone = req.body.phone;

                    user.age = req.body.age;
                    user.gender = req.body.gender;
                    user.profession = req.body.profession;
                    user.type = req.body.type;
                }

                user.save((err) => {
                    if (err) {
                        res.status(500).json({
                            status: "error",
                            message: "Whoops! Something went wrong",
                        });
                    } else {
                        res.status(201).json({
                            status: "success",
                            message: "User updated successfully",
                        });
                    }
                });
            }
        });
    }

    /**
     * Soft Delete
     *
     * @param req
     * @param res
     */
    static delete(req, res) {
        User.findById({ _id: req.params.userID }, (err, user) => {
            if (err) {
                res.status(500).json({ status: "error", message: err });
            } else {
                user.status = "deleted";

                user.save((err) => {
                    if (err) {
                        res.status(500).json({
                            status: "error",
                            message: "Whoops! Something went wrong",
                        });
                    } else {
                        res.status(201).json({
                            status: "success",
                            message: "User updated successfully",
                        });
                    }
                });
            }
        });
    }
}