import jwt from "jsonwebtoken";
import config from "../config/index.js";

export default (req, res, next) => {
    let token;
    if (req.headers["authorization"] && req.headers["authorization"].split(" ")[0] === "Bearer") {
        token = req.headers["authorization"].split(" ")[1];
    } else {
        token = req.body.token || req.query.token;
    }

    if (token) {
        jwt.verify(token, config.jwtSecret, (err, decoded) => {
            if (err) {
                return res.status(500).json({"status": "error", "message": "Failed to authenticate token"});
            } else {
                let date = Math.floor(Date.now() / 1000);

                if (decoded.exp < date) {
                    return res.status(403).json({"status": "error", "message": "Token expired"});
                }  else {
                    req.decodedToken = decoded;
                    next();
                }
            }
        });
    } else {
        return res.status(403).json({"status": "error", "message": "Token is missing"});
    }
}