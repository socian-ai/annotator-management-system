export default (req, res, next) => {
    if (req.decodedToken.type === 'admin') {
        next();
    } else {
        res.status(403).json({"status": "error", "message": "Access forbidden"});
    }
}