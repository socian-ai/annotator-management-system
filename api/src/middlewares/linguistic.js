import User from "../models/User.js";

export default (req, res, next) => {
  User.findOne(
    { _id: req.decodedToken._id, type: "linguistic", status: "active" },
    (err, linguistic) => {
      if (err) {
        return res.status(500).json({
          status: "error",
          message: "Whoops! Something went wrong while processing your request",
        });
      }

      if (!linguistic) {
        return res.status(404).json({
          status: "error",
          message: "The requested linguistic could not be found",
        });
      }

      return next();
    }
  );
};
