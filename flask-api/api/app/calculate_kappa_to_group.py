from bson.objectid import ObjectId
from more_itertools import chunked
from majority_voting import (
    run_majority_voting_for_sentence_sentiment,
    run_majority_voting_for_sentiment_paragraph_and_document,
    run_majority_voting_for_sentence_emotion,
    run_majority_voting_for_emotion_paragraphs
)
from pymongo import MongoClient
from multilabel_kappa import main___apply_kappa
import os
from datetime import datetime


class KappaAndMajority(object):

    def __init__(self):
        try:
            self.client = MongoClient('mongodb://{}:{}@{}:{}/{}?authSource=admin'.format(
                os.environ['MONGO_USERNAME'],
                os.environ['MONGO_PASSWORD'],
                os.environ['MONGO_HOSTNAME'],
                os.environ['MONGO_PORT'],
                os.environ['MONGO_DB']
            ))

            # self.client = MongoClient(
            #     'mongodb://socian:A7B5C8M1B7P@localhost:27017/ams?authSource=admin',
            #     connect=True,
            #     serverSelectionTimeoutMS=50000
            # )

            self.db = self.client.ams
            self.kappa_threshold_for_sentiment_score = .8
            self.kappa_threshold_for_emotion_score = .7
            print("Mongo Connected......flask")
        except:
            print("Mongo not connected")

    def get_all_annotated_data_by_group_id(self, group_id, dataType):

        if dataType == 'sentence-sentiment':
            majority_voting_result = self.process_sentence_sentiment_data(
                group_id, dataType
            )

            if majority_voting_result:
                accepted_list = majority_voting_result['accepted_list']
                rejected_list = majority_voting_result['rejected_list']

                self.add_data_to_final_table(
                    'sentence-sentiment', accepted_list
                )
                self.add_data_to_linguistic_table(
                    'sentence-sentiment', set(rejected_list)
                )

        elif dataType == 'paragraph-sentiment':
            majority_voting_result = self.process_paragraph_sentiment_data(
                group_id, dataType
            )
            if majority_voting_result:
                accepted_list = majority_voting_result['accepted_list']
                rejected_list = majority_voting_result['rejected_list']

                self.add_data_to_final_table(
                    'paragraph-sentiment', accepted_list
                )
                self.add_data_to_linguistic_table(
                    'paragraph-sentiment', set(rejected_list)
                )

        elif dataType == 'document-sentiment':
            majority_voting_result = self.process_document_sentiment_data(
                group_id, dataType
            )
            if majority_voting_result:
                accepted_list = majority_voting_result['accepted_list']
                rejected_list = majority_voting_result['rejected_list']

                self.add_data_to_final_table(
                    'document-sentiment', accepted_list
                )
                self.add_data_to_linguistic_table(
                    'document-sentiment', set(rejected_list)
                )

        elif dataType == 'sentence-emotion':
            majority_voting_result = self.process_sentence_emotion_data(
                group_id, dataType
            )
            if majority_voting_result:
                accepted_list = majority_voting_result['accepted_list']
                rejected_list = majority_voting_result['rejected_list']

                self.add_data_to_final_table(
                    'sentence-emotion', accepted_list
                )
                self.add_data_to_linguistic_table(
                    'sentence-emotion', set(rejected_list)
                )

        elif dataType == 'paragraph-emotion':
            majority_voting_result = self.process_paragraph_emotion_data(
                group_id, dataType
            )
            accepted_list = majority_voting_result['accepted_list']
            rejected_list = majority_voting_result['rejected_list']

            self.add_data_to_final_table(
                'paragraph-emotion', accepted_list
            )
            self.add_data_to_linguistic_table(
                'paragraph-emotion', set(rejected_list)
            )

        return True

    def process_sentence_sentiment_data(self, group_id, annotation_type):
        print('Process Sentence Sentiment for Group ID:', group_id)
        majority_voting_result = None
        sentence_sentiments = self.db.sentence_sentiments.find({
            "group": ObjectId(group_id)
        })

        sentence_sentiments = list(sentence_sentiments)
        sentence_sentiment_length = len(sentence_sentiments)
        print('Applying Kappa on number of Sentence Sentiment: ',
              sentence_sentiment_length)

        if sentence_sentiment_length > 0:
            kappa_score = main___apply_kappa(
                'sentence-sentiment', sentence_sentiments
            )

            data_from = self.get_kappa_score(annotation_type, group_id)
            data_to = data_from + sentence_sentiment_length
            print("Kappa Score: {} || Data From: {} || Data To: {}".format(
                kappa_score, data_from, data_to)
            )
            self.add_kappa_score(
                annotation_type, group_id,
                kappa_score, data_from, data_to
            )

            if kappa_score >= self.kappa_threshold_for_sentiment_score:
                majority_voting_result = run_majority_voting_for_sentence_sentiment(
                    sentence_sentiments, 'hight'
                )
            else:
                majority_voting_result = run_majority_voting_for_sentence_sentiment(
                    sentence_sentiments, 'low'
                )

        return majority_voting_result

    def process_paragraph_sentiment_data(self, group_id, annotation_type):
        print('Process Paragraph Sentiment for Group ID:', group_id)
        majority_voting_result = None
        paragraph_sentiments = self.db.paragraph_sentiments.find({
            "group": ObjectId(group_id)
        })

        paragraph_sentiments = list(paragraph_sentiments)
        paragraph_sentiments_length = len(paragraph_sentiments)
        print('Applying Kappa on number of Paragraph Sentiment: ',
              paragraph_sentiments_length)

        if paragraph_sentiments_length > 0:
            kappa_score = main___apply_kappa(
                'paragraph-sentiment', paragraph_sentiments
            )

            data_from = self.get_kappa_score(annotation_type, group_id)
            data_to = data_from + paragraph_sentiments_length
            print("Kappa Score: {} || Data From: {} || Data To: {}".format(
                kappa_score, data_from, data_to)
            )
            self.add_kappa_score(
                annotation_type, group_id,
                kappa_score, data_from, data_to
            )

            if kappa_score >= self.kappa_threshold_for_sentiment_score:
                majority_voting_result = run_majority_voting_for_sentiment_paragraph_and_document(
                    paragraph_sentiments, 'high'
                )
            else:
                majority_voting_result = run_majority_voting_for_sentiment_paragraph_and_document(
                    paragraph_sentiments, 'low'
                )

        return majority_voting_result

    def process_document_sentiment_data(self, group_id, annotation_type):
        print('Process Document Sentiment for Group ID:', group_id)
        majority_voting_result = None
        document_sentiments = self.db.document_sentiments.find({
            "group": ObjectId(group_id)
        })

        document_sentiments = list(document_sentiments)
        document_sentiments_length = len(document_sentiments)
        print('Applying Kappa on number of Document Sentiment: ',
              document_sentiments_length)

        if document_sentiments_length > 0:
            kappa_score = main___apply_kappa(
                'document-sentiment', document_sentiments
            )

            data_from = self.get_kappa_score(annotation_type, group_id)
            data_to = data_from + document_sentiments_length
            print("Kappa Score: {} || Data From: {} || Data To: {}".format(
                kappa_score, data_from, data_to)
            )
            self.add_kappa_score(
                annotation_type, group_id,
                kappa_score, data_from, data_to
            )

            if kappa_score >= self.kappa_threshold_for_sentiment_score:
                majority_voting_result = run_majority_voting_for_sentiment_paragraph_and_document(
                    document_sentiments, 'high'
                )
            else:
                majority_voting_result = run_majority_voting_for_sentiment_paragraph_and_document(
                    document_sentiments, 'low'
                )

        return majority_voting_result

    def process_sentence_emotion_data(self, group_id, annotation_type):
        print('Process Sentence Emotion for Group ID:', group_id)
        majority_voting_result = None
        sentence_emotions = self.db.sentence_emotions.find({
            "group": ObjectId(group_id)
        })

        sentence_emotions = list(sentence_emotions)
        sentence_emotions_length = len(sentence_emotions)
        print('Applying Kappa on number of Sentence Emotion: ',
              sentence_emotions_length)

        if sentence_emotions_length > 0:
            kappa_score = main___apply_kappa(
                'sentence-emotion', sentence_emotions
            )

            data_from = self.get_kappa_score(annotation_type, group_id)
            data_to = data_from + sentence_emotions_length
            print("Kappa Score: {} || Data From: {} || Data To: {}".format(
                kappa_score, data_from, data_to)
            )
            self.add_kappa_score(
                annotation_type, group_id,
                kappa_score, data_from, data_to
            )

            if kappa_score >= self.kappa_threshold_for_emotion_score:
                majority_voting_result = run_majority_voting_for_sentence_emotion(
                    sentence_emotions, 'high'
                )
            else:
                majority_voting_result = run_majority_voting_for_sentence_emotion(
                    sentence_emotions, 'low'
                )

        return majority_voting_result

    def process_paragraph_emotion_data(self, group_id, annotation_type):
        print('Process Paragraph Emotion for Group ID:', group_id)
        majority_voting_result = None
        paragraph_emotions = self.db.paragraph_emotions.find({
            "group": ObjectId(group_id)
        })

        paragraph_emotions = list(paragraph_emotions)
        paragraph_emotions_length = len(paragraph_emotions)
        print('Applying Kappa on number of Paragraph Emotion: ',
              paragraph_emotions_length)

        if paragraph_emotions_length > 0:
            kappa_score = main___apply_kappa(
                'paragraph-emotion', paragraph_emotions
            )

            data_from = self.get_kappa_score(annotation_type, group_id)
            data_to = data_from + paragraph_emotions_length
            print("Kappa Score: {} || Data From: {} || Data To: {}".format(
                kappa_score, data_from, data_to)
            )
            self.add_kappa_score(
                annotation_type, group_id,
                kappa_score, data_from, data_to
            )

            if kappa_score >= self.kappa_threshold_for_emotion_score:
                majority_voting_result = run_majority_voting_for_emotion_paragraphs(
                    paragraph_emotions, 'high'
                )
            else:
                majority_voting_result = run_majority_voting_for_emotion_paragraphs(
                    paragraph_emotions, 'low'
                )

        return majority_voting_result

    def add_kappa_score(self, annotation_type, group_id, score, data_from, data_to):
        self.db.kappa_scores.insert_one({
            "group": ObjectId(group_id),
            "data_type": annotation_type,
            "data_from": data_from,
            "data_to": data_to,
            "score": str(score),
            "created_at": datetime.now(),
            "updated_at": datetime.now()
        })

        return True

    def get_kappa_score(self, annotation_type, group_id):
        data_from = None
        cursor = self.db.kappa_scores.find({
            "group": ObjectId(group_id),
            "data_type": annotation_type
        }).sort([('created_at', -1)]).limit(1)

        results = list(cursor)
        if len(results) == 0:
            data_from = 0
        else:
            data_from = results[0]['data_to']
        return data_from

    def add_data_to_final_table(self, annotation_type, accepted_data_list):
        for accepted_data in accepted_data_list:
            accepted_obj = self.get_data_by_id(
                annotation_type, accepted_data["_id"])
            id = accepted_obj['_id']
            del accepted_obj['_id']
            accepted_obj['answer'] = accepted_data["answer"]
            accepted_obj['data_type'] = annotation_type

            self.db.final_datas.insert_one(accepted_obj)
            self.remove_original_data(annotation_type, id)

        return True

    def add_data_to_linguistic_table(self, annotation_type, rejected_ids_array):
        # print(rejected_ids_array)
        linguistics = []
        rejected_obj = self.get_data_by_id(annotation_type, next(iter(rejected_ids_array)))
        if len(rejected_obj['linguistics']) == 0:
            linguistic_users = self.get_group_users_by_group_id(
                rejected_obj['linguistic_group']
            )
            for linguistic_user in linguistic_users:
                linguistics.append(linguistic_user['user_id'])

        counter = 0
        for rejected_id in rejected_ids_array:
            rejected_obj = self.get_data_by_id(annotation_type, rejected_id)
            # print(rejected_obj)
            id = rejected_obj['_id']
            try:
                if counter == (len(linguistics) - 1):
                    rejected_obj['linguistic_validator'] = linguistics[counter]
                    counter = 0
                else:
                    rejected_obj['linguistic_validator'] = linguistics[counter]
                    counter = counter + 1
            except:
                pass

            del rejected_obj['_id']
            rejected_obj['data_type'] = annotation_type
            
            self.db.linguistic_datas.insert_one(rejected_obj)
            self.remove_original_data(annotation_type, id)

        return True

    def remove_original_data(self, annotation_type, instance_id):
        # print("Data Type: {} and Deleted ID: {}".format(annotation_type, instance_id))

        if annotation_type == 'sentence-sentiment':
            self.db.sentence_sentiments.delete_one(
                {"_id": ObjectId(instance_id)})
        elif annotation_type == 'paragraph-sentiment':
            self.db.paragraph_sentiments.delete_one(
                {"_id": ObjectId(instance_id)})
        elif annotation_type == 'document-sentiment':
            self.db.document_sentiments.delete_one(
                {"_id": ObjectId(instance_id)})
        elif annotation_type == 'sentence-emotion':
            self.db.sentence_emotions.delete_one(
                {"_id": ObjectId(instance_id)})
        elif annotation_type == 'paragraph-emotion':
            self.db.paragraph_emotions.delete_one(
                {"_id": ObjectId(instance_id)})

        return True

    def get_data_by_id(self, annotation_type, instance_id):
        data_obj = None
        if annotation_type == 'sentence-sentiment':
            data_obj = self.db.sentence_sentiments.find_one(
                {"_id": ObjectId(instance_id)})
        elif annotation_type == 'paragraph-sentiment':
            data_obj = self.db.paragraph_sentiments.find_one(
                {"_id": ObjectId(instance_id)})
        elif annotation_type == 'document-sentiment':
            data_obj = self.db.document_sentiments.find_one(
                {"_id": ObjectId(instance_id)})
        elif annotation_type == 'sentence-emotion':
            data_obj = self.db.sentence_emotions.find_one(
                {"_id": ObjectId(instance_id)})
        elif annotation_type == 'paragraph-emotion':
            data_obj = self.db.paragraph_emotions.find_one(
                {"_id": ObjectId(instance_id)})

        return data_obj

    def get_group_users_by_group_id(self, group_id):
        group = self.db.groups.find_one({"_id": ObjectId(group_id)})

        users = []
        for user in group['users']:
            users.append({
                "_id": ObjectId(),
                "user_id": user
            })

        return users
