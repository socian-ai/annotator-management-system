import nltk
from nltk.metrics import agreement
from nltk.metrics.agreement import AnnotationTask
from nltk.metrics import masi_distance
import json
from tqdm import tqdm
import traceback
from pprint import pprint
from collections import Counter
import math


# #  Sentence sentiment

def extract_sentence_sentiment_json(json_obj):
    results = []
    for item in json_obj:
        sentence_id = item['_id']
        try:
            # if linguist label exists, ignore annotator label
            if 'linguistics' in item and len(item['linguistics']) > 0:
                annotations = item['linguistics']
            else:
                annotations = item['annotators']

            for labeling_instance in annotations:
                annotator_id = labeling_instance['user_id']
                answer = labeling_instance['answer']
                if 'subjectivity' not in answer:
                    continue
                subjectivity = answer['subjectivity']
                if subjectivity == 'subjective':
                    label = [answer['polarity']]
                    
                else:
                    label = ['objective']
                results.append((annotator_id, sentence_id, frozenset(label)))
        except:
            traceback.print_exc()

    return results



# with open('sentence_sentiment.json', 'r') as f:
#     obj=json.loads(f.read())
#     r = extract_sentence_sentiment_json(obj)
#     pprint(r)
#     task = AnnotationTask(distance = masi_distance)
#     task.load_array(r)
#     try:
#         score = task.multi_kappa()
#     except:
#         score=task.alpha()
#     print(score)


# # Paragraph sentiment


def extract_paragraph_sentiment_json(json_obj):
    results = []
    for item in json_obj:
        try:
            paragraph_id = item['_id']
            if 'linguistics' in item and len(item['linguistics']) > 0:
                annotations = item['linguistics']
            else:
                annotations = item['annotators']

            for labeling_instance in annotations:
                annotator_id = labeling_instance['user_id']
                answer = labeling_instance['answer']
                if 'sentiment_overall' not in answer:
                    continue
                label = [answer['sentiment_overall']]
                
                results.append((annotator_id, paragraph_id, frozenset(label)))
        except:
            traceback.print_exc()

    return results




# with open('paragraph_sentiment.json', 'r') as f:
#     obj=json.loads(f.read())
#     r = extract_paragraph_sentiment_json(obj)
#     pprint(r)
#     task = AnnotationTask(distance = masi_distance)
#     task.load_array(r)
#     #score = task.multi_kappa()
#     try:
#         score = task.multi_kappa()
#     except:
#         score=task.alpha()
#     print(score)


# # Sentence emotion


def extract_sentence_emotion_json(json_obj):
    results = []
    for item in json_obj:
        sentence_id = item['_id']
        try:
            if 'linguistics' in item and len(item['linguistics']) > 0:
                annotations = item['linguistics']
            else:
                annotations = item['annotators']

            for labeling_instance in annotations:
                annotator_id = labeling_instance['user_id']
                answer = labeling_instance['answer']
                
                if 'emotions' not in answer:
                    continue
                emotion_labels = answer['emotions']

                results.append((annotator_id, sentence_id, frozenset(emotion_labels)))
        except:
            traceback.print_exc()

    return results


# with open('sentence_emotion.json', 'r') as f:
#     obj=json.loads(f.read())
#     r = extract_sentence_emotion_json(obj)
#     pprint(r)
#     task = AnnotationTask(distance = masi_distance)
#     task.load_array(r)
#     try:
#         score = task.multi_kappa()
#     except:
#         score=task.alpha()
#     print(score)


# # Paragraph emotion


def extract_paragraph_emotion_json(json_obj):
    results = []
    for item in json_obj:
        sentence_id = item['_id']
        try:
            if 'linguistics' in item and len(item['linguistics']) > 0:
                annotations = item['linguistics']
            else:
                annotations = item['annotators']

            for labeling_instance in annotations:
                annotator_id = labeling_instance['user_id']
                answer = labeling_instance['answer']
                if 'sentiment_overall' not in answer:
                    continue
                emotion_labels = answer['sentiment_overall']

                results.append((annotator_id, sentence_id, frozenset(emotion_labels)))
        except:
            traceback.print_exc()

    return results

# with open('paragraph_emotion.json', 'r') as f:
#     obj=json.loads(f.read())
#     r = extract_paragraph_emotion_json(obj)
#     pprint(r)
#     task = AnnotationTask(distance = masi_distance)
#     task.load_array(r)
#     try:
#         score = task.multi_kappa()
#     except:
#         score=task.alpha()
#     print(score)

def main___apply_kappa(annotation_type, json_obj):
    if annotation_type == 'sentence-sentiment':
        data = extract_sentence_sentiment_json(json_obj)
    elif annotation_type == 'paragraph-sentiment':
        data = extract_paragraph_sentiment_json(json_obj)
    elif annotation_type == 'document-sentiment':
        data = extract_paragraph_sentiment_json(json_obj)
    elif annotation_type == 'sentence-emotion':
        data = extract_sentence_emotion_json(json_obj)
    elif annotation_type == 'paragraph-emotion':
        data = extract_paragraph_emotion_json(json_obj)
    else:
        raise ValueError('Please pass correct annotation type')
        
    task = AnnotationTask(distance = masi_distance)
    task.load_array(data)
    try:
        print('Running multi kappa')
        score = task.multi_kappa()
    except:
        print('Running Krippendorf alpha.')
        score = task.alpha()
    return score


