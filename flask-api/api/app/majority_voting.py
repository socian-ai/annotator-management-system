import nltk
from nltk.metrics import agreement
from nltk.metrics.agreement import AnnotationTask
from nltk.metrics import masi_distance
import json
from tqdm import tqdm
import traceback
from pprint import pprint
from collections import Counter
import math


# Sentence Sentiment

def extract_sentence_sentiment_json(json_obj):
    results = []
    for item in json_obj:
        sentence_id = item['_id']
        try:
            # if linguist label exists, ignore annotator label
            if 'linguistics' in item and len(item['linguistics']) > 0:
                annotations = item['linguistics']
            else:
                annotations = item['annotators']

            for labeling_instance in annotations:
                answer = labeling_instance['answer']
                if 'subjectivity' not in answer:
                    continue
                subjectivity = answer['subjectivity']
                if subjectivity == 'subjective':
                    label = [answer['polarity']]

                else:
                    label = ['objective']
                results.append(
                    (sentence_id, len(annotations), frozenset(label)))
        except:
            traceback.print_exc()
    
    return results


def extract_paragraph_sentiment_json(json_obj):
    results = []

    for item in json_obj:
        try:
            paragraph_id = item['_id']
            if 'linguistics' in item and len(item['linguistics']) > 0:
                annotations = item['linguistics']
            else:
                annotations = item['annotators']

            for labeling_instance in annotations:
                answer = labeling_instance['answer']
                if 'sentiment_overall' not in answer:
                    continue
                label = [answer['sentiment_overall']]

                results.append(
                    (paragraph_id, len(annotations), frozenset(label)))
        except:
            traceback.print_exc()

    return results


def run_majority_voting_for_sentence_sentiment(json_obj, kappa_status):
    data = extract_sentence_sentiment_json(json_obj)

    sentence_id_map = {}
    votes_needed_map = {}

    for item in data:
        sentence_id = item[0]
        votes_needed_map[sentence_id] = item[1]
        label = item[2]

        if sentence_id in sentence_id_map:
            sentence_id_map[sentence_id].append(list(label)[0])
        else:
            sentence_id_map[sentence_id] = [list(label)[0]]

    accepted = []
    rejected = []
    # print(sentence_id_map)
    for key in sentence_id_map:
        counter = Counter(sentence_id_map[key])
        # pprint(counter.most_common())
        votes_needed = votes_needed_map[key]
        if kappa_status == 'high':
            votes_needed = math.ceil(votes_needed/2)

        # print(votes_needed)
        for label, count in counter.most_common():
            if count < votes_needed:
                # rejected
                rejected.append(str(key))
            else:
                if label == 'objective':
                    answer = {
                        "subjectivity": "objective"
                    }
                else:
                    answer = {
                        "subjectivity": "subjective",
                        "polarity": label
                    }

                accepted.append({'_id': str(key), 'answer': answer})

    return {
        "accepted_list": accepted,
        "rejected_list": rejected,
    }

# Sentiment Common


def do_majority_voting_on_overall_field_for_sentiment(list_of_votes, votes_needed):
    vote_counter = Counter(list_of_votes)
    label, vote = vote_counter.most_common(1)[0]
    if vote < votes_needed:
        return {
            'passed': False,
            'reason': 'not enough votes cast'
        }
    if vote >= votes_needed:
        return {
            'passed': True,
            'result': label,
        }


# # Paragraph and Document Sentiment


def do_majority_voting_on_topics_for_sentiment(topic_map, votes_needed):
    optional_topics = []
    results = []
    disaggreed_topics = []
    verdict_passed = True

    for topic, responses in topic_map.items():
        vote_counter = Counter(responses)
        voted_label, voted_count = vote_counter.most_common(1)[0]

        if len(responses) < votes_needed:
            # not enough annotators to count vote
            optional_topics.append({"name": topic, "polarity": voted_label})
        else:
            if voted_count >= votes_needed:
                results.append({"name": topic, "polarity": voted_label})
            else:
                disaggreed_topics.append(
                    {"name": topic, "polarity": voted_label}
                )
                verdict_passed = False
    return {
        'passed': verdict_passed,
        'results': results,
        'disaggreed_topics': disaggreed_topics,
        'optional_topics': optional_topics,
    }


def process_single_sentiment_paragraph_and_document(item, kappa_status):
    annotator_map = {}
    sentiment_overalls = []

    # if linguist response exists, ignore annotator
    if 'linguistics' in item and len(item['linguistics']):
        annotations = item['linguistics']
    else:
        annotations = item['annotators']

    votes_needed = len(annotations)
    if kappa_status == 'high':
        votes_needed = math.ceil(votes_needed/2)
    # print(votes_needed)
    topic_map = {}

    for labeling_instance in annotations:
        annotator_id = labeling_instance['user_id']
        answer = labeling_instance['answer']

        annotator_map[annotator_id] = True

        if 'sentiment_overall' in answer:
            sentiment_overalls.append(answer['sentiment_overall'])

        if 'topics' not in answer:
            continue

        for topic in answer['topics']:
            if topic['name'] not in topic_map:
                topic_map[topic["name"]] = [topic['polarity']]
            else:
                topic_map[topic["name"]].append(topic['polarity'])

    overall_result = do_majority_voting_on_overall_field_for_sentiment(
        sentiment_overalls, votes_needed
    )
    if overall_result['passed']:
        topic_results = do_majority_voting_on_topics_for_sentiment(
            topic_map, votes_needed
        )

        if topic_results['passed']:
            return {
                'passed': True,
                'topic_results': topic_results,
                'overall_result': overall_result['result']
            }
        else:
            return {
                'passed': False,
                'overall_result': overall_result['result'],
                'failed_for': 'topic'
            }
    else:
        return {
            'passed': False,
            'failed_for': 'overall'
        }


def run_majority_voting_for_sentiment_paragraph_and_document(obj_list, kappa_status):
    accepted_list = []
    rejected_list = []

    for item in obj_list:
        item_id = item['_id']
        result = process_single_sentiment_paragraph_and_document(
            item, kappa_status
        )

        if result['passed']:
            accepted_list.append({
                '_id': str(item_id),
                'answer': {
                    "sentiment_overall": result['overall_result'],
                    "topics": result['topic_results']['results'],
                    "optional_topics": result['topic_results']['optional_topics']
                }
            })
        else:
            rejected_list.append(str(item_id))

    return {
        'accepted_list': accepted_list,
        'rejected_list': rejected_list
    }


# # Sentence Emotion


def extract_sentence_emotion_json(json_obj):
    results = []
    for item in json_obj:
        sentence_id = item['_id']
        try:
            if 'linguistics' in item and len(item['linguistics']) > 0:
                annotations = item['linguistics']
            else:
                annotations = item['annotators']

            for labeling_instance in annotations:
                answer = labeling_instance['answer']

                if 'emotions' not in answer:
                    continue
                emotion_labels = answer['emotions']

                results.append((sentence_id, len(annotations),
                                frozenset(emotion_labels)))
        except:
            traceback.print_exc()

    return results


def run_majority_voting_for_sentence_emotion(json_obj, kappa_status):
    data = extract_sentence_emotion_json(json_obj)
    # pprint(data)
    sentence_id_map = {}
    votes_needed_map = {}

    for item in data:
        sentence_id = item[0]
        votes_needed_map[sentence_id] = item[1]
        label = item[2]

        if sentence_id not in sentence_id_map:
            sentence_id_map[sentence_id] = [list(label)]
        else:
            sentence_id_map[sentence_id].append(list(label))

    accepted = []
    rejected = []
    for key in sentence_id_map:
        labels = sentence_id_map[key]
        flat_labels = [item for sublist in labels for item in sublist]

        labels_counter = Counter(flat_labels)

        votes_needed = votes_needed_map[key]
        if kappa_status == 'high':
            votes_needed = math.ceil(votes_needed/2)
        # print(votes_needed)
        majority_votes = []
        for label, count in labels_counter.most_common():
            if count >= votes_needed:
                majority_votes.append(label)
        if len(majority_votes) > 0:
            accepted.append({
                "_id": str(key),
                "answer": {
                    "emotions": majority_votes
                }
            })
        else:
            rejected.append(str(key))

    return {
        'accepted_list': accepted,
        'rejected_list': rejected
    }

# Emotion Common


def do_majority_voting_on_overall_field_for_emotion(list_of_votes, votes_needed):
    # votes_needed = math.ceil(annotator_count/2)
    # print('votes needed ', votes_needed)
    list_of_votes = [item for sublist in list_of_votes for item in sublist]
    vote_counter = Counter(list_of_votes)

    majority_voted = []
    for label, vote in vote_counter.most_common():
        if vote >= votes_needed:
            majority_voted.append(label)

    if len(majority_voted) > 0:
        return {
            'passed': True,
            'results': majority_voted,
        }
    else:
        return {
            'passed': False,
            'reason': 'not enough common votes found.'
        }


# # Paragraph Emotion


def do_majority_voting_on_topics_for_emotion(topic_map, votes_needed):
    optional_topics = []
    results = []
    disaggreed_topics = []
    verdict_passed = True
    # votes_needed = math.ceil(annotator_count/2)

    for topic, responses in topic_map.items():
        # print('topic ', topic)
        # print('responses ', responses)

        if len(responses) < votes_needed:
            optional_topics.append({"name": topic, "emotions": responses[0]})
            continue

        responses_flattened = [
            item for sublist in responses for item in sublist]
        majority_votes_this_topic = []
        vote_counter = Counter(responses_flattened)
        # print(vote_counter)
        for vote_label, vote_count in vote_counter.most_common():
            if vote_count >= votes_needed:
                majority_votes_this_topic.append(vote_label)
        # print('topic ', topic)
        # print(majority_votes_this_topic)

        if len(majority_votes_this_topic) > 0:
            results.append(
                {"name": topic, "emotions": majority_votes_this_topic})
        else:
            disaggreed_topics.append(
                {"name": topic, "emotions": majority_votes_this_topic})
            verdict_passed = False

    return {
        'passed': verdict_passed,
        'results': results,
        'disaggreed_topics': disaggreed_topics,
        'optional_topics': optional_topics,
    }


def process_single_emotion_paragraph(item, kappa_status):
    annotator_map = {}
    sentiment_overalls = []

    # if linguist response exists, ignore annotator
    if 'linguistics' in item and len(item['linguistics']):
        annotations = item['linguistics']
    else:
        annotations = item['annotators']

    votes_needed = len(annotations)
    if kappa_status == 'high':
        votes_needed = math.ceil(votes_needed/2)
    # print(votes_needed)

    topic_map = {}
    for labeling_instance in annotations:
        annotator_id = labeling_instance['user_id']
        answer = labeling_instance['answer']

        annotator_map[annotator_id] = True

        if 'sentiment_overall' in answer:
            sentiment_overalls.append(answer['sentiment_overall'])

        if 'topics' not in answer:
            continue

        for topic in answer['topics']:
            if topic['name'] not in topic_map:
                topic_map[topic["name"]] = [topic['emotions']]
            else:
                topic_map[topic["name"]].append(topic['emotions'])

    # pprint(topic_map)
    overall_result = do_majority_voting_on_overall_field_for_emotion(
        sentiment_overalls, votes_needed
    )
    if overall_result['passed']:
        topic_results = do_majority_voting_on_topics_for_emotion(
            topic_map, votes_needed
        )
        if topic_results['passed']:
            return {
                'passed': True,
                'overall_result': overall_result['results'],
                'topic_results': topic_results['results'],
                'optional_topics': topic_results['optional_topics']
            }
        else:
            return {
                'passed': False,
                'overall_result': overall_result['results'],
                'failed_for': 'topic',
                'optional_topics': topic_results['optional_topics']
            }
    else:
        return {
            'passed': False,
            'failed_for': 'overall'
        }


def run_majority_voting_for_emotion_paragraphs(obj_list, kappa_status):
    accepted_list = []
    rejected_list = []

    for item in obj_list:
        result = process_single_emotion_paragraph(item, kappa_status)
        if result['passed']:
            accepted_list.append({
                "_id": str(item['_id']),
                "answer": {
                    "topics": result['topic_results'],
                    "optional_topics": result['optional_topics'],
                    "sentiment_overall": result['overall_result']
                }
            })
        else:
            rejected_list.append(str(item['_id']))

    return {
        'accepted_list': accepted_list,
        'rejected_list': rejected_list
    }


# Get Object By Object ID

def get_object_by_id(json_obj, object_id):
    for item in json_obj:
        if item['_id'] == object_id:
            return item
    return None
