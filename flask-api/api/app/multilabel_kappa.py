import json
import numpy as np
import pandas as pd
from statsmodels.stats.inter_rater import fleiss_kappa
import traceback


def calculate_kappa(matrix):
    '''

    :param matrix: a matrix where rows= number of votes for label, and column = label
    :return: kappa value
    '''
    return round(fleiss_kappa(matrix), 3)

def list_to_string_for_annotator(label_list):
    '''
    convert ['Happy','Fear'] to ['Happy,Fear']
    :param label_list: list of string
    :return:
    '''
    name = ''
    for i, label in enumerate(label_list):
        if i == 0:
            name = name + str(label)
        else:
            name = name + "," + str(label)
    return name

def matrix_for_given_label(_matrix, label):
    '''
    for a given label count how many times it got vote and how many times it didn't get vote for each instances and make a matrix
    :param _matrix: a 2d list of all annotations
    :param label: label name (type string)
    :return: 2d list
    '''
    my_mat = []
    for row in _matrix:
        label_voted_count = 0
        label_not_voted_count = 0
        for column in row:
            column = column.split(',')
            if label in column:
                label_voted_count += 1
            else:
                label_not_voted_count += 1
        my_mat.append([label_voted_count, label_not_voted_count])
    return my_mat

def labelWise_FK_calculation(annotation_matrics, labels=["Happy","Fear","Anger/Disgust","Sadness","Surprise","Neutral"]):
    '''
    Caluculate Fleiss kappa for multi label multi class annotion
    :param annotation_matrics: a matrics of instances X [#of given, #of not_given]
    :param labels: a list of label names
    :return:  kappa score
    '''

    kappa_result = []
    #calculate kappa for each label
    for label in labels:
        my_mat = matrix_for_given_label(annotation_matrics, label)
        k = calculate_kappa(my_mat)
        if np.isnan(k):
            k = 1
        kappa_result.append(k)
    kappa_result = np.array(kappa_result)
    score = round(np.nanmean(kappa_result),3)
    return score

def two_d_list_to_df(lst):
    df = pd.DataFrame(lst)
    return df

def count_label_of_each_instances(df, label_names=['WN', 'SN', 'WP', 'SP', 'objective', 'NU']):
    '''
    this function count for each data which label get how many votes and append the info in a  dataframe and return  matrices
    :param df:
    :param label_names: a list of labels
    :return:2d list
    '''
    temp_df = pd.get_dummies(df, dummy_na=True)
    for label in label_names:
        df[label] = temp_df[temp_df.columns[temp_df.columns.str.endswith(label)]].sum(1)
    return df[label_names].values

# #  Sentence sentiment
def extract_sentence_sentiment_json(json_obj):
    _list = []
    for item in json_obj:
        try:
            annotations = item['annotators']
            _rows = []
            for labeling_instance in annotations:
                answer = labeling_instance['answer']
                if 'subjectivity' not in answer:
                    _rows.append('skip')
                    continue
                subjectivity = answer['subjectivity']
                if subjectivity == 'subjective':
                    label = answer['polarity']
                else:
                    label = 'objective'
                _rows.append(label)
            if 'skip' not in _rows:
                _list.append(_rows)
        except:
            traceback.print_exc()

    return _list

# #  paragraph and document sentiment
def extract_paragraph_sentiment_json(json_obj):
    _list = []
    for item in json_obj:
        try:
            annotations = item['annotators']
            _rows = []
            for labeling_instance in annotations:
                answer = labeling_instance['answer']
                if 'sentiment_overall' not in answer:
                    _rows.append('skip')
                    continue
                label = answer['sentiment_overall']
                _rows.append(label)
            if 'skip' not in _rows:
                _list.append(_rows)
        except:
            traceback.print_exc()

    return _list


# # emotion
def extract_emotion_json(json_obj,types):
    _list = []
    for item in json_obj:
        try:
            annotations = item['annotators']
            _rows = []
            for labeling_instance in annotations:
                answer = labeling_instance['answer']
                if types not in answer:
                    _rows.append('skip')
                    continue
                emotion_labels = answer[types]
                _rows.append(list_to_string_for_annotator(emotion_labels))

            if 'skip' not in _rows:
                _list.append(_rows)
        except:
            traceback.print_exc()
    return _list


def main___apply_kappa(annotation_type, json_obj):
    if annotation_type == 'sentence-sentiment':
        _list = extract_sentence_sentiment_json(json_obj)
        df = two_d_list_to_df(_list)
        matrix = count_label_of_each_instances(df, label_names=['WN', 'SN', 'WP', 'SP', 'objective', 'NU'])
        score = calculate_kappa(matrix)
        return score
    elif annotation_type == 'paragraph-sentiment':
        _list = extract_paragraph_sentiment_json(json_obj)
        print(_list)
        df = two_d_list_to_df(_list)
        matrix = count_label_of_each_instances(df, label_names=['WN', 'SN', 'WP', 'SP', 'objective', 'NU'])
        score = calculate_kappa(matrix)
        return score
    elif annotation_type == 'document-sentiment':
        _list = extract_paragraph_sentiment_json(json_obj)
        df = two_d_list_to_df(_list)
        matrix = count_label_of_each_instances(df, label_names=['WN', 'SN', 'WP', 'SP', 'objective', 'NU'])
        score = calculate_kappa(matrix)
        return score

    elif annotation_type == 'sentence-emotion':
        _list = extract_emotion_json(json_obj,types='emotions')
        if np.shape(_list)[1]>1:
            score = labelWise_FK_calculation(_list,labels=["Happy", "Fear", "Anger/Disgust", "Sadness", "Surprise", "Neutral"])
            return score

    elif annotation_type == 'paragraph-emotion':
        _list = extract_emotion_json(json_obj,types='sentiment_overall')
        if np.shape(_list)[1] > 1:
            score = labelWise_FK_calculation(_list,labels=["Happy", "Fear", "Anger/Disgust", "Sadness", "Surprise", "Neutral"])
            return score
    else:
        raise ValueError('Please pass correct annotation type')
        






#just for checking


#
# def main():
#     # Opening JSON file
#     f = open('document_sentiment.json', )
#
#     # returns JSON object as
#     # a dictionary
#     json_obj = json.load(f)
#     f.close()
#     print(json_obj)
#
#     kappa = main___apply_kappa(annotation_type='document-sentiment', json_obj=json_obj)
#     print("kappa: ", kappa)
#
#
# #for checking only
# if __name__ == '__main__':
#     main()
#