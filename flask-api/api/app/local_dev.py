import logging
import os
import sys
import uuid
import json
from io import BytesIO
from flask import jsonify
from celery import Celery
from celery.result import AsyncResult
from flask import Flask, request
from flask_cors import CORS
from minio import Minio
from calculate_kappa_to_group import KappaAndMajority
from minio.error import (ResponseError, BucketAlreadyOwnedByYou,
                         BucketAlreadyExists)


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
logger.addHandler(handler)

host = 'localhost'
port = 8000

app = Flask(__name__)
CORS(app)

CELERY_BROKER_URL = 'redis://localhost:6379/0'

CELERY_RESULT_BACKEND = 'redis://localhost:6379/1'
MINIO_HOST = '127.0.0.1:9000'
MINIO_ACCESS_KEY = 'ReTUArKhARtErIStehELEXpAToREn'
MINIO_SECRET_KEY = 'yLTs0auhZDB1BjjIitx7WZvY1k5hq9jUEUGr3u6q'
bucket_name = 'texts'

celery_app = Celery(broker=CELERY_BROKER_URL, backend=CELERY_RESULT_BACKEND)
minio_client = Minio(MINIO_HOST,
                     access_key=MINIO_ACCESS_KEY,
                     secret_key=MINIO_SECRET_KEY, secure=False)

# create minio bucket if not exists
try:
    # location param doesn't matter, as we are not using distributed deployment
    minio_client.make_bucket(bucket_name, location="ap-southeast-1")
except BucketAlreadyExists:
    logger.info("Bucket {} already exists. ".format(bucket_name))
except BucketAlreadyOwnedByYou:
    logger.info("Bucket {} already exists. ".format(bucket_name))


@app.route('/preprocess/similarity', methods=['POST'])
def similarity_sentence():
    print(request.json)

    file_name = f"{uuid.uuid1()}.json"
    json_string = json.dumps(request.json, ensure_ascii=False)
    byte_stream = BytesIO(json_string.encode('utf8'))

    # insert it to minio for passing it to Celery worker
    minio_client.put_object(bucket_name, file_name,
                            byte_stream, byte_stream.getbuffer().nbytes)
    result = celery_app.send_task('check_similarity_sentence', [file_name])
    return {"status": "Queued for deduplication.", "task_id": result.id}, 201


@app.route('/preprocess/sentences', methods=['POST'])
def preprocess_sentences():
    if 'sentence_list' not in request.json and \
            'existing_sentence_list_from_db' not in request.json:
        return {"error": "please send 'sentence_list' and 'existing_sentence_list_from_db'"}, 400

    file_name = f"{uuid.uuid1()}.json"
    json_string = json.dumps(request.json, ensure_ascii=False)
    byte_stream = BytesIO(json_string.encode('utf8'))

    # insert it to minio for passing it to Celery worker
    minio_client.put_object(bucket_name, file_name,
                            byte_stream, byte_stream.getbuffer().nbytes)

    result = celery_app.send_task('deduplicate_sentences', [file_name])
    return {"status": "Queued for deduplication.", "task_id": result.id}, 201


@app.route('/preprocess/result', methods=['GET'])
def get_result_by_task_id():
    if 'task_id' not in request.json:
        return {'error': "Please provide task_id"}, 400

    res = AsyncResult(request.json['task_id'], app=celery_app)
    return {"state": res.state, "result": res.get()}


@app.route('/apply-fleiss-kappa', methods=['POST'])
def apply_fleiss_kappa():
    print("Request Params:", request.json)
    kappa_majority = KappaAndMajority()
    kappa_majority.get_all_annotated_data_by_group_id(
        request.json['group_id'], request.json['dataType']
    )
    
    return jsonify({"status": "success", "message": "Kappa Applied Success"})


if __name__ == "__main__":
    app.run(debug=True, host=host, port=port)
