import logging
import os
import sys
import uuid
import json
from io import BytesIO

from celery import Celery
from celery.result import AsyncResult
from flask import Flask, request
from flask_cors import CORS
from minio import Minio
from minio.error import (
    ResponseError, BucketAlreadyOwnedByYou,
    BucketAlreadyExists
)

from calculate_kappa_to_group import KappaAndMajority

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
logger.addHandler(handler)

host = '0.0.0.0'
port = os.environ['HTTP_PORT']

app = Flask(__name__)
CORS(app)
CELERY_BROKER_URL = os.environ['CELERY_BROKER_URL']
CELERY_RESULT_BACKEND = os.environ['CELERY_RESULT_BACKEND']
MINIO_HOST = os.environ['MINIO_HOST']
MINIO_ACCESS_KEY = os.environ['MINIO_ACCESS_KEY']
MINIO_SECRET_KEY = os.environ['MINIO_SECRET_KEY']
bucket_name = os.environ['MINIO_BUCKET']

celery_app = Celery(broker=CELERY_BROKER_URL, backend=CELERY_RESULT_BACKEND)
minio_client = Minio(
    MINIO_HOST,
    access_key=MINIO_ACCESS_KEY,
    secret_key=MINIO_SECRET_KEY,
    secure=False
)

# create minio bucket if not exists
try:
    # location param doesn't matter, as we are not using distributed deployment
    minio_client.make_bucket(bucket_name, location="ap-southeast-1")
except BucketAlreadyExists:
    logger.info("Bucket {} already exists. ".format(bucket_name))
except BucketAlreadyOwnedByYou:
    logger.info("Bucket {} already exists. ".format(bucket_name))


@app.route('/preprocess/similarity', methods=['POST'])
def similarity_sentence():
    print(request.json)

    file_name = f"{uuid.uuid1()}.json"
    json_string = json.dumps(request.json, ensure_ascii=False)
    byte_stream = BytesIO(json_string.encode('utf8'))

    # insert it to minio for passing it to Celery worker
    minio_client.put_object(bucket_name, file_name,
                            byte_stream, byte_stream.getbuffer().nbytes)
    result = celery_app.send_task('check_similarity_sentence', [file_name])
    return {"status": "Queued for deduplication.", "task_id": result.id}, 201


@app.route('/preprocess/sentences', methods=['POST'])
def preprocess_sentences():
    if 'sentence_list' not in request.json and \
            'existing_sentence_list_from_db' not in request.json:
        return {"error": "please send 'sentence_list' and 'existing_sentence_list_from_db'"}, 400

    file_name = f"{uuid.uuid1()}.json"
    json_string = json.dumps(request.json, ensure_ascii=False)
    byte_stream = BytesIO(json_string.encode('utf8'))

    # insert it to minio for passing it to Celery worker
    minio_client.put_object(bucket_name, file_name,
                            byte_stream, byte_stream.getbuffer().nbytes)

    result = celery_app.send_task('deduplicate_sentences', [file_name])
    return {"status": "Queued for deduplication.", "task_id": result.id}, 201


@app.route('/preprocess/result', methods=['POST'])
def get_result_by_task_id():
    if 'task_id' not in request.json:
        return {'error': "Please provide task_id"}, 400

    res = AsyncResult(request.json['task_id'], app=celery_app)
    return {"state": res.state, "result": res.get()}


@app.route('/apply-fleiss-kappa', methods=['POST'])
def apply_fleiss_kappa():
    kappa_majority = KappaAndMajority()
    kappa_majority.get_all_annotated_data_by_group_id(
        request.json['group_id'], request.json['dataType']
    )

    return {"status": "success", "message": "Kappa Applied Success"}


if __name__ == "__main__":
    app.run(debug=True, host=host, port=port)
