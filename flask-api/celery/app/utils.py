import re

bangla_regex_pattern = re.compile(r"[^\u0980-\u09FF ।,?!.]+")
def is_all_bangla(text):
    if bool(bangla_regex_pattern.match(text)):  ## contains invalid characters
        return False
    return True
