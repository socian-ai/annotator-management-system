import logging
import sys
import os
import json
import uuid
from io import BytesIO

from tqdm import tqdm

import lemmatize
import jaccard_similarity
import word_mover_distance
from pymongo import MongoClient
from bson.objectid import ObjectId

# minio_client = Minio(os.environ['MINIO_HOST'],
#                      access_key=os.environ['MINIO_ACCESS_KEY'],
#                      secret_key=os.environ['MINIO_SECRET_KEY'], secure=False)
#
JACCARD_SIMILARITY_THRESHOLD = .80
WORD_MOVER_DISTANCE_THRESHOLD = .50

client = MongoClient('mongodb://socian:A7B5C8M1B7P@localhost:27017/ams?authSource=admin')
db = client.annotator_managemet_data_preprocessing_db


def deduplicate_sentences(incoming_json):
    # read the incoming json file from minio
    # response = minio_client.get_object(os.environ['MINIO_BUCKET'], minio_json_file_name)
    # incoming_json = json.loads(response.data.decode('utf-8'))
    print('incoming_json', incoming_json)
    sentence_text_list = [sent[0] for sent in incoming_json['sentence_list']]
    # sentence_text_list = incoming_json.get("sentence_list")
    sentence_domain_list = [sent[1] for sent in incoming_json['sentence_list']]
    # sentence_domain_list = incoming_json.get("sentence_list")

    print('sentence_text_list', sentence_text_list)

    existing_sentence_text_list_from_db = [sent[0] for sent in incoming_json['existing_sentences_from_db']]
    # existing_sentence_text_list_from_db = incoming_json.get("existing_sentence_list_from_db")

    print('existing_sentence_text_list_from_db', existing_sentence_text_list_from_db)

    # existing_sentence_domain_list_from_db = [sent[1] for sent in incoming_json['existing_sentence_list_from_db']]

    lemmatized_sentence_list = [lemmatize.lemmatize_sentence(sent) for sent in sentence_text_list]

    print('lemmatized_sentence_list', lemmatized_sentence_list)

    dissimilar_sentence_list = []
    similar_sentence_list = []
    for idx1, sentence1 in tqdm(enumerate(lemmatized_sentence_list), total=len(lemmatized_sentence_list)):
        is_similar = False

        for idx2, sentence2 in enumerate(lemmatized_sentence_list + existing_sentence_text_list_from_db):
            if idx1 == idx2:
                continue
            # check structural similarity
            jaccard_score = jaccard_similarity.compute_similarity(sentence1, sentence2)
            if jaccard_score > JACCARD_SIMILARITY_THRESHOLD:
                # check semantic similarity
                wm_distance = word_mover_distance.get_similarity(sentence1, sentence2)
                if wm_distance < WORD_MOVER_DISTANCE_THRESHOLD:
                    is_similar = True
        if is_similar:
            domain = sentence_domain_list[idx1]
            print(sentence1, domain)
            similar_sentence_list.append([sentence1, domain])
        else:
            domain = sentence_domain_list[idx1]
            print(sentence1, domain)
            dissimilar_sentence_list.append([sentence1, domain])


    print("Found {} sentences structurally dissimilar".format(len(dissimilar_sentence_list)))
    print('dissimilar_sentence_list', dissimilar_sentence_list)

    print("Found {} sentences structurally similar".format(len(similar_sentence_list)))
    print('similar_sentence_list', similar_sentence_list)

    #save minio json file for similar object
    similar_json_object_list = []
    if len(similar_sentence_list) > 0 :
        for object in similar_sentence_list:
            similar_json_object_list.append(object[1])
            print("Similar Object Added :",object[1])
    else:
        similar_json_object_list.append({'message': 'no similar data found'})
    result_json_str = json.dumps(similar_json_object_list, ensure_ascii=False)
    print('result_json_str', result_json_str)
    result_file_name = f"result-{uuid.uuid1()}.json"
    byte_stream = BytesIO(result_json_str.encode('utf8'))
    # minio_client.put_object(os.environ['MINIO_BUCKET'], result_file_name, byte_stream, byte_stream.getbuffer().nbytes)

    # save mongodb dissimilar object
    for dissimilar_sentence in dissimilar_sentence_list:
        text = dissimilar_sentence[0]
        print("Text", text)
        save_object = dissimilar_sentence[1]
        print("object", save_object)

        save_object["text"] = dissimilar_sentence[0]
        print("get_db_object_by_id result text", save_object)

        if save_object["data_type"] == "sentence-sentiment":
            sentence_sentiment = db["sentence_sentiment"]
            saved_node_id = sentence_sentiment.insert_one(save_object).inserted_id
            print("saved_node_id", saved_node_id)

        if save_object["data_type"] == "paragraph-sentiment":
            paragraph_sentiment = db["paragraph_sentiment"]
            saved_node_id = paragraph_sentiment.insert_one(save_object).inserted_id
            print("saved_node_id", saved_node_id)

        if save_object["data_type"] == "document-sentiment":
            document_sentiment = db["document_sentiment"]
            saved_node_id = document_sentiment.insert_one(save_object).inserted_id
            print("saved_node_id", saved_node_id)

        if save_object["data_type"] == "sentence-emotion":
            sentence_emotion = db["sentence_emotion"]
            saved_node_id = sentence_emotion.insert_one(save_object).inserted_id
            print("saved_node_id", saved_node_id)

        if save_object["data_type"] == "paragraph-emotion":
            paragraph_emotion = db["paragraph_emotion"]
            saved_node_id = paragraph_emotion.insert_one(save_object).inserted_id
            print("saved_node_id", saved_node_id)

    return result_file_name


def preprocess_sentences():

    dataJson = {'sentence_list': [('কেন্দ্রীয় ঔষধাগারের সাবেক পরিচালক শহীদুল্লাহ করোনায় মারা গেছেন', "01233"), ('চীনের গবেষণায় বাংলাদেশের অংশ নেওয়া উচিত: জাফরুল্লাহ চৌধুরী',"432435")],
                'existing_sentences_from_db': [('কেন্দ্রীয় ঔষধাগারের সাবেক পরিচালক শহীদুল্লাহ করোনায় মারা গেছেন', "4343434"),('চীনের গবেষণায় বাংলাদেশের অংশ নেওয়া উচিত: জাফরুল্লাহ চৌধুরী', "3232323"),('চীনের গবেষণায় বাংলাদেশের অংশ নেওয়া উচিত: জাফরুল্লাহ চৌধুরী', "323223")]}

    if 'sentence_list' not in dataJson and \
            'existing_sentence_list_from_db' not in dataJson:
        return {"error": "please send 'sentence_list' and 'existing_sentence_list_from_db'"}, 400

    # file_name = f"{uuid.uuid1()}.json"
    # json_string = json.dumps(dataJson, ensure_ascii=False)
    # byte_stream = BytesIO(json_string.encode('utf8'))

    # insert it to minio for passing it to Celery worker
    # minio_client.put_object(os.environ['MINIO_BUCKET'], file_name, byte_stream, byte_stream.getbuffer().nbytes)

    deduplicate_sentences(dataJson)


if __name__ == '__main__':
    preprocess_sentences()
