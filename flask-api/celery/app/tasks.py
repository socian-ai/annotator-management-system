import logging
import sys
import os
import json
import uuid
from io import BytesIO

from tqdm import tqdm
from celery.utils.log import get_task_logger
from celery import Celery
from minio import Minio
from minio.error import (ResponseError, BucketAlreadyOwnedByYou,
                         BucketAlreadyExists)

import lemmatize
import jaccard_similarity
import word_mover_distance
from pymongo import MongoClient
from bson.objectid import ObjectId
import numpy as np

logger = get_task_logger(__name__)

app = Celery()
app.config_from_object("celeryconfig")

try:
    client = MongoClient('mongodb://{}:{}@{}:{}/{}?authSource=admin'.format(
        os.environ['MONGO_USERNAME'],
        os.environ['MONGO_PASSWORD'],
        os.environ['MONGO_HOSTNAME'],
        os.environ['MONGO_PORT'],
        os.environ['MONGO_DB']
    ))
    db = client.ams
    print(db.users.find_one({}))
    print("Mongo Connected......INSIDE CELERY")
except:
    print("MOngo not connected")


MINIO_HOST = os.environ['MINIO_HOST']
MINIO_ACCESS_KEY = os.environ['MINIO_ACCESS_KEY']
MINIO_SECRET_KEY = os.environ['MINIO_SECRET_KEY']
MINIO_BUCKET = os.environ['MINIO_BUCKET']


minio_client = Minio(
    MINIO_HOST,
    access_key=MINIO_ACCESS_KEY,
    secret_key=MINIO_SECRET_KEY, 
    secure=False
)

# JACCARD_SIMILARITY_THRESHOLD = .80
# WORD_MOVER_DISTANCE_THRESHOLD = .50

JACCARD_SIMILARITY_THRESHOLD = .45
WORD_MOVER_DISTANCE_THRESHOLD = .40

@app.task(name='deduplicate_sentences')
def deduplicate_sentences(minio_json_file_name):
    # read the incoming json file from minio
    response = minio_client.get_object(MINIO_BUCKET, minio_json_file_name)
    incoming_json = json.loads(response.data.decode('utf-8'))

    sentence_text_list = [sent[0] for sent in incoming_json['sentence_list']]
    sentence_domain_list = [sent[1] for sent in incoming_json['sentence_list']]

    existing_sentence_text_list_from_db = [sent[0] for sent in incoming_json['existing_sentence_list_from_db']]
    # existing_sentence_domain_list_from_db = [sent[1] for sent in incoming_json['existing_sentence_list_from_db']]

    lemmatized_sentence_list = [lemmatize.lemmatize_sentence(sent) for sent in sentence_text_list]

    dissimilar_sentence_list = []
    for idx1, sentence1 in tqdm(enumerate(lemmatized_sentence_list), total=len(lemmatized_sentence_list)):
        is_similar = False

        for idx2, sentence2 in enumerate(lemmatized_sentence_list + existing_sentence_text_list_from_db):
            if idx1 == idx2:
                continue
            # check structural similarity
            jaccard_score = jaccard_similarity.compute_similarity(sentence1, sentence2)
            if jaccard_score > JACCARD_SIMILARITY_THRESHOLD:
                # check semantic similarity
                wm_distance = word_mover_distance.get_similarity(sentence1, sentence2)
                if wm_distance < WORD_MOVER_DISTANCE_THRESHOLD:
                    is_similar = True
        if not is_similar:
            domain = sentence_domain_list[idx1]
            dissimilar_sentence_list.append([sentence1, domain])

    logger.info("Found {} sentences structurally dissimilar".format(len(dissimilar_sentence_list)))

    result_json_str = json.dumps(dissimilar_sentence_list, ensure_ascii=False)
    result_file_name = f"result-{uuid.uuid1()}.json"
    byte_stream = BytesIO(result_json_str.encode('utf8'))
    minio_client.put_object(MINIO_BUCKET, result_file_name, byte_stream, byte_stream.getbuffer().nbytes)
    print(result_file_name)
    return result_file_name


@app.task(name='check_similarity_sentence')
def check_similarity_sentence(minio_json_file_name):
    response = minio_client.get_object(MINIO_BUCKET, minio_json_file_name)
    response_json = json.loads(response.data.decode('utf-8'))

    sentence_list = []
    existing_sentences_from_db = []
    try:
        for object in response_json:
            text = object["text"]
            text_and_node_tuple = (text, object)
            sentence_list.append(text_and_node_tuple)
    except:
        print("failed to parse date field")
        return {"status": "failed to parse date field"}, 303

    sentence_sentiment = get_existing_data(db.sentence_sentiment)
    paragraph_sentiment = get_existing_data(db.paragraph_sentiment)
    document_sentiment = get_existing_data(db.document_sentiment)
    sentence_emotion = get_existing_data(db.sentence_emotion)
    paragraph_emotion = get_existing_data(db.paragraph_emotion)

    existing_sentences_from_db.extend(sentence_sentiment)
    existing_sentences_from_db.extend(paragraph_sentiment)
    existing_sentences_from_db.extend(document_sentiment)
    existing_sentences_from_db.extend(sentence_emotion)
    existing_sentences_from_db.extend(paragraph_emotion)
    print("existing_sentences_from_db_lists", existing_sentences_from_db)

    incoming_json = {
        "sentence_list": sentence_list,
        "existing_sentences_from_db": existing_sentences_from_db
    }
    print('incoming_json', incoming_json)


    #start similarity process
    sentence_text_list = [sent[0] for sent in incoming_json['sentence_list']]
    sentence_domain_list = [sent[1] for sent in incoming_json['sentence_list']]
    print('sentence_text_list', sentence_text_list)

    existing_sentence_text_list_from_db = [sent[0] for sent in incoming_json['existing_sentences_from_db']]
    print('existing_sentence_text_list_from_db', existing_sentence_text_list_from_db)

    lemmatized_sentence_list = [lemmatize.lemmatize_sentence(sent) for sent in sentence_text_list]

    print('lemmatized_sentence_list', lemmatized_sentence_list)

    dissimilar_sentence_list = []
    similar_sentence_list = []
    for idx1, sentence1 in tqdm(enumerate(lemmatized_sentence_list), total=len(lemmatized_sentence_list)):
        is_similar = False

        for idx2, sentence2 in enumerate(lemmatized_sentence_list + existing_sentence_text_list_from_db):
            if idx1 == idx2:
                print("continue..id:",idx1)
                continue
            # check structural similarity
            jaccard_score = jaccard_similarity.compute_similarity(np.unique(sentence1.split()),np.unique(sentence2.split()))
            print("jaccard_score",jaccard_score)
            if jaccard_score > JACCARD_SIMILARITY_THRESHOLD:
                # check semantic similarity
                wm_distance = word_mover_distance.get_similarity(sentence1, sentence2)
                print("WM_DISTANCE: {}".format(wm_distance))
                if wm_distance < WORD_MOVER_DISTANCE_THRESHOLD:
                    is_similar = True
        if is_similar:
            domain = sentence_domain_list[idx1]
            print("similar",sentence1)
            similar_sentence_list.append([sentence1, domain])
        else:
            domain = sentence_domain_list[idx1]
            print("dissimilar",sentence1)
            dissimilar_sentence_list.append([sentence1, domain])

    print("Found {} sentences structurally dissimilar".format(len(dissimilar_sentence_list)))
    print('dissimilar_sentence_list', dissimilar_sentence_list)

    print("Found {} sentences structurally similar".format(len(similar_sentence_list)))
    print('similar_sentence_list', similar_sentence_list)
    #done similarity process

    #getting similar objects
    similar_json_object_list = []
    if len(similar_sentence_list) > 0:
        for object in similar_sentence_list:
            similar_json_object_list.append(object[1])
            print("Similar Object Added :", object[1])
    else:
        similar_json_object_list.append({'message': 'no similar data found'})

    # save mongodb dissimilar objects
    for dissimilar_sentence in dissimilar_sentence_list:
        text = dissimilar_sentence[0]
        print("Text", text)
        save_object = dissimilar_sentence[1]
        print("object", save_object)

        save_object["text"] = dissimilar_sentence[0]
        print("get_db_object_by_id result text", save_object)

        if save_object["data_type"] == "sentence-sentiment":
            sentence_sentiment = db["sentence_sentiment"]
            saved_node_id = sentence_sentiment.insert_one(save_object).inserted_id
            print("saved_node_id", saved_node_id)

        if save_object["data_type"] == "paragraph-sentiment":
            paragraph_sentiment = db["paragraph_sentiment"]
            saved_node_id = paragraph_sentiment.insert_one(save_object).inserted_id
            print("saved_node_id", saved_node_id)

        if save_object["data_type"] == "document-sentiment":
            document_sentiment = db["document_sentiment"]
            saved_node_id = document_sentiment.insert_one(save_object).inserted_id
            print("saved_node_id", saved_node_id)

        if save_object["data_type"] == "sentence-emotion":
            sentence_emotion = db["sentence_emotion"]
            saved_node_id = sentence_emotion.insert_one(save_object).inserted_id
            print("saved_node_id", saved_node_id)

        if save_object["data_type"] == "paragraph-emotion":
            paragraph_emotion = db["paragraph_emotion"]
            saved_node_id = paragraph_emotion.insert_one(save_object).inserted_id
            print("saved_node_id", saved_node_id)


    result_json_str = json.dumps(similar_json_object_list, ensure_ascii=False)
    print('result_json_str', result_json_str)
    result_file_name = f"result-{uuid.uuid1()}.json"
    byte_stream = BytesIO(result_json_str.encode('utf8'))
    minio_client.put_object(MINIO_BUCKET, result_file_name, byte_stream, byte_stream.getbuffer().nbytes)
    return result_json_str



def get_existing_data(temp_data):
    cursor = temp_data.find({})
    existing_sentences_list = []
    for document in cursor:
        text = document["text"]
        text_and_node_tuple = (text, document)
        existing_sentences_list.append(text_and_node_tuple)
    return existing_sentences_list