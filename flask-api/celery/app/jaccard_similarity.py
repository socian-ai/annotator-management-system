
def compute_similarity(word_list1, word_list2):
    intersection = len(list(set(word_list1).intersection(word_list2)))
    union = (len(word_list1) + len(word_list2)) - intersection
    return float(intersection) / union