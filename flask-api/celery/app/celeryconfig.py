import os

## Broker settings.
broker_url = os.environ['CELERY_BROKER_URL']
result_backend = os.environ['CELERY_RESULT_BACKEND']

# List of modules to import when the Celery worker starts.
include = ('tasks',)

## Using the database to store task state and results.
task_acks_late = True
worker_prefetch_multiplier = 1
worker_max_memory_per_child = 1000 * 1000 * 2 # GB

# todo change time limit
task_time_limit = 999000

timezone = 'UTC'
task_create_missing_queues = True