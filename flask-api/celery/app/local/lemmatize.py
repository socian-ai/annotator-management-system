from banglakit import lemmatizer


lemmatizer_instance = lemmatizer.BengaliLemmatizer()


def lemmatize_sentence(text):
    result = ""
    for word in text.split():
        lem_word = lemmatizer_instance.lemmatize(word, pos=lemmatizer.POS_NOUN)
        result += lem_word + " "
    return result
